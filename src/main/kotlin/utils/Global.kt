package utils

object Global {

    enum class Direction {
        DOWN, LEFT, RIGHT,UP
    }
    const val IS_DEBUG = false
    const val ATK_DEBUG = false

    // 視窗大小
    const val WINDOW_WIDTH = 1080
    const val WINDOW_HEIGHT = 720
    const val SCREEN_X = WINDOW_WIDTH - 8 - 8
    const val SCREEN_Y = WINDOW_HEIGHT - 31 - 8
    const val JUNGLE_MAP_WIDTH = 4800
    const val JUNGLE_MAP_HEIGHT = 4800

    // 資料刷新時間
    const val UPDATE_FREQ = 60 // 每秒更新60次遊戲邏輯

    // 畫面更新時間
    const val PAINT_FREQ = 60

    //人物大小
    const val UNIT_X = 32
    const val UNIT_Y = 32

    //Monster大小
    const val M_UNIT_X = 48
    const val M_UNIT_Y = 48

    //random
    fun random(min: Int, max: Int) = (Math.random() * (max - min + 1) + min).toInt()

    fun timeFormat(time: Long): String{
        var t: Long = time / 1000
        var m: Int = (t/60).toInt()
        var s: Int = (t%60).toInt()
        t = time % 1000/10
        return "$m : $s : $t"
    }

}