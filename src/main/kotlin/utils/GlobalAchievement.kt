package utils

object GlobalAchievement {
    enum class Achievement(val content: String, var finishedCount: Int, val goal: Int, var finish: Boolean) {  //？？？？？
        PRAY_TIME_UNDER_TWO_S("時間管理大師 -- 在遊戲時間剩下2秒內時購買時間", 0, 10, false),
        THREE_TOWER("大旅遊家 -- 造訪大陸中的三座巨塔", 0, 3, false),
        FOREST_FARMER("謎之隱居者 -- 發現隱居在森林旁的農戶", 0, 1, false),
        ALL_TREASURE("寶藏獵人 -- 找到大陸上所有寶箱", 0, 2, false),
        MORE_THAN_THIRTY_S("極速勇者 -- 在剩下時間大於30秒情況下通關", 0, 1, false),
        ALL_THREE_START("完美主義者 -- 全部關卡皆三星通關", 0, 4, false),
        ALL_STYLING("時尚達人 -- 購買全部時裝造型", 0, 13, false),
        TEN_THOUSAND("守財奴 -- 關卡遊戲內累積10000G金幣", 0, 10000, false),
        ALL_SPECIAL_EVENT("不務正業 -- 發現全大陸的特殊事件", 0, 8, false),
        DOWNGRADE_LESS_THREE("人生好難 -- 降等次數少於3次的情況下通關", 0, 1, false),
    }

    //PRAY_TIME_UNDER_TWO_S
    fun prayTimeUnderTwoSeconds(time: Double) {
        if (time <= 2 && !Achievement.PRAY_TIME_UNDER_TWO_S.finish) {
            Achievement.PRAY_TIME_UNDER_TWO_S.finishedCount++
            if (Achievement.PRAY_TIME_UNDER_TWO_S.finishedCount == Achievement.PRAY_TIME_UNDER_TWO_S.goal) {
                Achievement.PRAY_TIME_UNDER_TWO_S.finish = true
            }
        }
    }

    //THREE_TOWER
    enum class ThreeTower(var found: Boolean) {
        STAGE_ONE_TOWER(false),
        STAGE_TWO_TOWER(false),
        STAGE_FOUR_TOWER(false),
    }

    fun threeTower(index: Int) {
        if (!ThreeTower.values()[index].found) {
            ThreeTower.values()[index].found = true
            Achievement.THREE_TOWER.finishedCount++
            if (Achievement.THREE_TOWER.finishedCount == Achievement.THREE_TOWER.goal) {
                Achievement.THREE_TOWER.finish = true
            }
        }
    }

    //FOREST_FARMER
    fun forestFarmer() {
        if(!Achievement.FOREST_FARMER.finish) {
            Achievement.FOREST_FARMER.finishedCount = 1;
            Achievement.FOREST_FARMER.finish = true
        }
    }

    //ALL_TREASURE
    enum class AllTreasure(var found: Boolean) {
        STAGE_TWO_TOWER(false),
        STAGE_THREE_TOWER(false)
    }

    fun allTreasure(index: Int) {
        if (!AllTreasure.values()[index].found) {
            AllTreasure.values()[index].found = true
            Achievement.ALL_TREASURE.finishedCount++
            if (Achievement.ALL_TREASURE.finishedCount == Achievement.ALL_TREASURE.goal) {
                Achievement.ALL_TREASURE.finish = true
            }
        }
    }

    //MORE_THAN_THIRTY_S
    fun moreThanThirtySeconds(time: Double) {
        if (time > 30) {
            Achievement.MORE_THAN_THIRTY_S.finishedCount = Achievement.MORE_THAN_THIRTY_S.goal
            Achievement.MORE_THAN_THIRTY_S.finish = true
        }
    }

    //ALL_THREE_START
    fun allThreeStart() {
        if (!Achievement.ALL_THREE_START.finish) {
            var threeStartCount: Int = 0
            for (i in 0..3) {
                if (GlobalPlayer.GameRecord.values()[i].grade == 3) {
                    threeStartCount++
                }
            }
            Achievement.ALL_THREE_START.finishedCount = threeStartCount
            if (Achievement.ALL_THREE_START.finishedCount == Achievement.ALL_THREE_START.goal) {
                Achievement.ALL_THREE_START.finish = true
            }
        }
    }

    //ALL_STYLING
    fun allStyling() {
        if (!Achievement.ALL_STYLING.finish) {
            var allStylingCount: Int = 0
            for (i in 0..12) {
                if (GlobalPlayer.Styling.values()[i].isBought) {

                    allStylingCount++
                }
            }
            Achievement.ALL_STYLING.finishedCount = allStylingCount
            if (Achievement.ALL_STYLING.finishedCount >= Achievement.ALL_STYLING.goal) {
                Achievement.ALL_STYLING.finish = true
            }
        }
    }

    //TEN_THOUSAND
    fun tenThousand(money: Int) {
        if (money > Achievement.TEN_THOUSAND.finishedCount) {
            Achievement.TEN_THOUSAND.finishedCount = money
        }
        if (Achievement.TEN_THOUSAND.finishedCount >= Achievement.TEN_THOUSAND.goal && !Achievement.TEN_THOUSAND.finish) {
            Achievement.TEN_THOUSAND.finish = true
        }
    }

    //ALL_SPECIAL_EVENT
    enum class AllSpecialEvent(var found: Boolean) {
        STAGE_ONE_ONE(false),
        STAGE_ONE_TWO(false),
        STAGE_TWO_ONE(false),
        STAGE_TWO_TWO(false),
        STAGE_THREE_ONE(false),
        STAGE_THREE_TWO(false),
        STAGE_FOUR_ONE(false),
        STAGE_FOUR_TWO(false),
    }

    fun allSpecialEvent(index: Int){
        if (!Achievement.ALL_SPECIAL_EVENT.finish && !AllSpecialEvent.values()[index].found) {
            AllSpecialEvent.values()[index].found = true
            var count: Int = 0
            for (i in 0..7) {
                if (AllSpecialEvent.values()[i].found) {
                    count++
                }
            }
            Achievement.ALL_SPECIAL_EVENT.finishedCount = count
            if (Achievement.ALL_SPECIAL_EVENT.finishedCount == Achievement.ALL_SPECIAL_EVENT.goal) {
                Achievement.ALL_SPECIAL_EVENT.finish = true
            }
        }
    }

    //DOWNGRADE_LESS_THREE
    fun downgradeLessThanThree(count : Int){
        if(count <= 3 && !Achievement.DOWNGRADE_LESS_THREE.finish){
            Achievement.DOWNGRADE_LESS_THREE.finishedCount = Achievement.DOWNGRADE_LESS_THREE.goal
            Achievement.DOWNGRADE_LESS_THREE.finish = true
        }
    }
}