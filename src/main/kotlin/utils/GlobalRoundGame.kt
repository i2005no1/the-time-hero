package utils

object GlobalRoundGame {
    //Props
    var usingArms: GlobalPlayer.Arms = GlobalPlayer.Arms.WOOD_SWORD
    var usingArmors: GlobalPlayer.Armors = GlobalPlayer.Armors.LAVISH_MAIL
    var usingShoes: GlobalPlayer.Shoes = GlobalPlayer.Shoes.HIDE_SHOES
    var havingHerb: Boolean = true

    //Player information
    var level: Int = 1
    var exp: Int = 0
    var expLimit: Int = 100 + (level - 1) * 20
    var roundMoney: Int = 100;
    var hp: Int = 500 + level * 100
    var hpLimit = 500 + level * 100  //之後再算
    var attack: Int = 100+usingArms.addAttack + level * 18
    var defence: Int = usingArmors.addDefence + level * 18
    var speed: Int = usingShoes.addSpeed

    //textFrame
    var newText: MutableList<String>? = mutableListOf()
    var oldText: String? = null

    //Village
    var prayPrice: Int = 100
    var recoveryFoodPrice: Int = 100

    //Record
    var startTime: Long = 0
    var endTime: Long = 0
    var monsterKilled: Int = 0
    var moneySpent: Int = 0

    fun reset() {
        //Props
        usingArms = GlobalPlayer.Arms.WOOD_SWORD
        usingArmors = GlobalPlayer.Armors.LAVISH_MAIL
        usingShoes = GlobalPlayer.Shoes.HIDE_SHOES
        havingHerb = true

        //Player information
        level = 1
        exp = 0
        expLimit = 100 + (level - 1) * 20
        if(Global.ATK_DEBUG){
            atkDebug()
        }else {
            roundMoney = 100
        }
        hp = 500 + level * 100
        hpLimit = 500+ level * 100  //之後再算
        attack = 100+usingArms.addAttack + level * 18
        defence = usingArmors.addDefence + level * 18
        speed = usingShoes.addSpeed


        //textFrame
        newText = mutableListOf()
        oldText = null

        //Village
        prayPrice = 100
        recoveryFoodPrice = Global.random(10, 20) * 10  //??

        //Record
        startTime = 0
        endTime = 0
        monsterKilled = 0
        moneySpent = 0
    }

    fun setHerb() {
        havingHerb = !havingHerb
    } //藥草持有狀態更改


    fun updatePlayerInformation() {
        expLimit = 100 + (level - 1) * 20
        hpLimit = 500 + level * 100
        attack = 100+usingArms.addAttack + level * 18
        defence = usingArmors.addDefence + level * 18
        speed = usingShoes.addSpeed
    }

    var playerName = GlobalPlayer.playerName


    fun addNewText(s: String) {
        newText?.add(s)
    }

    const val FLEE_PROBABILITY: Int = 70

    fun atkDebug(){
        roundMoney = 10000
        level = 45
        usingArmors=GlobalPlayer.Armors.TIME_ARMOR
        usingArms=GlobalPlayer.Arms.TIME_SPEAR
        usingShoes=GlobalPlayer.Shoes.TIME_BOOTS

        GlobalAchievement.Achievement.TEN_THOUSAND.finish = true
        GlobalAchievement.Achievement.ALL_SPECIAL_EVENT.finish = true
        GlobalAchievement.Achievement.PRAY_TIME_UNDER_TWO_S.finish = true
        GlobalAchievement.Achievement.MORE_THAN_THIRTY_S.finish = true
        updatePlayerInformation()
        hp=hpLimit
    }

    //multiplayer
    var attacked : Boolean = false
    var ateRecoveryFood: Boolean = false
    val whoSendBossInformation: MutableList<Int> = mutableListOf(0,0,0)
}