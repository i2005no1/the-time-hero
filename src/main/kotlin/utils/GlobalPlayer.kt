package utils


import core.controllers.FileController
import javafx.scene.layout.Background
import java.io.BufferedReader
import java.io.IOException


object GlobalPlayer {
    enum class Gender {
        MALE, FEMALE
    }

    enum class Npc(val npcName: String) {
        TIME_GODDESS("TimeGoddess"),
        RECOVERY_FOOD("RecoveryFood"),
        WEAPONS_DEALER("WeaponsDealer")
    }

    enum class Styling(val stylingName: String, val gender: Gender, val price: Int, var isBought: Boolean) {
        G_KYLIE("G_Kylie", Gender.FEMALE, 0, true),
        G_OLIVIA("G_Olivia", Gender.FEMALE, 1000, false),
        G_ISLA("G_Isla", Gender.FEMALE, 1000, false),
        G_SOPHIA("G_Sophia", Gender.FEMALE, 1000, false),
        G_MIA("G_Mia", Gender.FEMALE, 1000, false),
        G_AMELIA("G_Amelia", Gender.FEMALE, 1000, false),
        B_BARD("B_Bard", Gender.MALE, 1000, false),
        B_HARRY("B_Harry", Gender.MALE, 1000, false),
        B_GEORGE("B_George", Gender.MALE, 1000, false),
        B_CHARLIE("B_Charlie", Gender.MALE, 1000, false),
        B_OSCAR("B_Oscar", Gender.MALE, 1000, false),
        B_NOAH("B_Noah", Gender.MALE, 1000, false),
        B_JON("B_Jon", Gender.MALE, 1000, false)
    }

    enum class Monsters(
        val monsterName: String, val hpLimit: Int, var attack: Int, val defence: Int, val exp: Int, val money: Int
        , val size: Int,val background: String
    ) {

        AREA1_MONSTER1("Slime", 650, 10, 10, 100, 80, 128,"Grassland"),
        AREA1_MONSTER2("Skeleton", 800, 100, 50, 120, 100, 128,"Combined"),
        AREA1_MONSTER3("Ziz", 1200, 200, 100, 150, 120, 128,"Parallax Desert "),
        AREA1_MONSTER4("Senior Fox", 1500, 300, 150, 200, 150, 128,"Forest"),

        AREA2_MONSTER1("Muk", 1600, 200, 50, 180, 160, 128,"Port"),
        AREA2_MONSTER2("DarkWizard", 1800, 400, 150, 200, 180, 128,"Wasteland"),
        AREA2_MONSTER3("Gargoyle", 2000, 600, 250, 220, 200, 128,"Snowfield"),
        AREA2_MONSTER4("Golem", 2250, 800, 350, 240, 220, 128,"IceMaze"),

        AREA3_MONSTER1("Goblin", 2500, 400, 200, 260, 240, 128,"Ruins"),
        AREA3_MONSTER2("Werewolf", 2700, 800, 300, 280, 260, 128,"Meadow"),
        AREA3_MONSTER3("Buraq", 3000, 1150, 400, 300, 280, 128,"Lava1"),
        AREA3_MONSTER4("Lich", 3500, 1500, 550, 320, 300, 128,"LavaCave"),

        STAGE1_BOSS("Ammut", 18000, 500, 360, 0, 0, 256,"DemonCastle3"),
        STAGE2_BOSS("Ars Goetia", 23000, 850, 630, 0, 0, 256,"DemonCastle2"),
        STAGE3_BOSS("Kraken", 40500, 1458, 730, 0, 0, 256,"DemonCastle4"),
        STAGE4_BOSS("Hydra", 45000, 1620, 810, 0, 0, 256,"DemonCastle4"),

        //多人模式
        MULTI_MONSTER1("Slime", 650, 10, 10, 100, 80, 128,"Grassland"),
        MULTI_MONSTER2("Skeleton", 800, 100, 50, 120, 100, 128,"Combined"),
        MULTI_MONSTER3("Ziz", 1200, 200, 100, 140, 120, 128,"Parallax Desert "),
        MULTI_MONSTER4("Senior Fox", 1400, 300, 150, 160, 140, 128,"Forest"),

        MULTI_MONSTER5("Muk", 1600, 450, 200, 180, 160, 128,"Port"),
        MULTI_MONSTER6("DarkWizard", 1800, 600, 250, 200, 180, 128,"Wasteland"),
        MULTI_MONSTER7("Gargoyle", 2000, 750, 300, 220, 200, 128,"Snowfield"),
        MULTI_MONSTER8("Golem", 2250, 900, 350, 240, 220, 128,"IceMaze"),

        MULTI_MONSTER9("Goblin", 2500, 1100, 400, 260, 240, 128,"Ruins"),
        MULTI_MONSTER10("Werewolf", 2750, 1300, 450, 280, 260, 128,"Meadow"),
        MULTI_MONSTER11("Buraq", 3000, 1500, 500, 300, 280, 128,"Lava1"),
        MULTI_MONSTER12("Lich", 3500, 1700, 550, 320, 300, 128,"LavaCave"),

        MULTI_BOSS1("Ammut", 20000, 550, 400, 1000, 500, 256,"DemonCastle3"),
        MULTI_BOSS2("Ars Goetia", 35000, 1000, 700, 2000, 800, 256,"DemonCastle2"),
        MULTI_BOSS3("Hydra", 50000, 1800, 900, 0, 0, 256,"DemonCastle4"),
        CUSTOMIZE("",0,0,0,0,0,0,"")

    }

    enum class Herbs(val herbsName: String, val price: Int, val explanation: String) {
        HERBS("Herbs", 150, "Herbs -- can be full blood at once time")
    }

    enum class Arms(val armsName: String, val addAttack: Int, val price: Int, val explanation: String) { //price???
        WOOD_SWORD("Wood Sword", 10, 200, "Level 0"),
        BRONZE_LANCE("Bronze Lance", 40, 300, "Level 1"),
        WAR_HAMMER("War Hammer", 80, 400, "Level 2"),
        SLIVER_SWORD("Sliver Sword", 120, 600, "Level 3"),
        EVIL_BREAKER("Evil Breaker", 160, 800, "Level 4"),
        SAGES_STAFF("Sage's Staff", 240, 1500, "Level 5"),
        SEA_SPEAR("Sea Spear", 320, 2000, "Level 6"),
        BALMUNG("Balmung", 400, 3000, "Level 7"),
        HERO_SWORD("Hero Sword", 560, 4000, "Level 8"),
        BRAVE_BLADE("Brave Blade", 720, 5000, "Level 9"),
        TIME_SPEAR("Time Spear", 880, 6000, "Level Max")
    }

    enum class Armors(val armorsName: String, val addDefence: Int, val price: Int, val explanation: String) {
        LAVISH_MAIL("Lavish Mail", 10, 200, "Level 0"),
        FUR_COAT("Fur Coat", 40, 300, "Level 1"),
        HIDE_ARMOR("Hide Armor", 80, 400, "Level 2"),
        RAINCOAT_MAIL("Raincoat Mail", 120, 600, "Level 3"),
        SCALE_MAIL("Scale Mail", 160, 800, "Level 4"),
        ANIMA_PLATE("Animal Plate", 240, 1500, "Level 5"),
        TURTLE_MAIL("Turtle Mail", 320, 2000, "Level 6"),
        LIGHT_ARMOR("Light Armor", 400, 3000, "Level 7"),
        SLIVER_MAIL("Sliver Mail", 560, 4000, "Level 8"),
        LORD_MAIL("Lord Mail", 720, 5000, "Level 9"),
        TIME_ARMOR("Time Armor", 880, 6000, "Level Max")
    }

    enum class Shoes(val shoesName: String, val addSpeed: Int, val price: Int, val explanation: String) {
        HIDE_SHOES("Hide Shoes", 3, 100, "Level 0"),
        COMFY_SANDALS("Comfy Sandals", 4, 125, "Level 1"),
        CLOGS("Clogs", 4, 125, "Level 1"),
        RUBBER_BOOTS("Rubber Boots", 4, 125, "Level 1"),
        BATTLE_BOOTS("Battle Boots", 5, 150, "Level 2"),
        SNOWALKERS("Snowalkers", 5, 150, "Level 2"),
        SILENT_BOOTS("Slient Boots", 6, 175, "Level 3"),
        AIR_SNEAKERS("Air Sneakers", 6, 175, "Level 3"),
        BRONZE_SHOES("Bronze Shoes", 7, 200, "Level 4"),
        SPEED_GEAR("Speed Gear", 7, 200, "Level 4"),
        BIGFOOR("Bigfoor", 8, 250, "Level 5"),
        DUEL_GREAVES("Duel Greaves", 9, 300, "Level 6"),
        TIME_BOOTS("Time Boots", 10, 350, "Level Max")
    }


    var playerMoney: Int = 10000
    var usingStyling: Styling = Styling.G_KYLIE
    var playerName: String = "LittleHebe"

    //Game record
    enum class GameRecord(
        var grade: Int,
        var time: Long,
        var moneySpent: Int,
        var playerLevel: Int,
        var monsterKilled: Int,
        var canPlay: Boolean
    ) {
        STAGE_ONE(0, 0, 0, 0, 0, true),
        STAGE_TWO(0, 0, 0, 0, 0,false),
        STAGE_THREE(0, 0, 0, 0, 0,false),
        STAGE_FOUR(0, 0, 0, 0, 0,false)
    }

    //Grade Level
    enum class GradeLevel(val twoStart: Long, val threeStart: Long) {
        STAGE_ONE(600000, 420000),
        STAGE_TWO(600000, 420000),
        STAGE_THREE(600000, 420000),
        STAGE_FOUR(900000, 600000),
        MULTIPLAYER(900000, 600000)
    }

    fun setRecord(stage: Int, grade: Int, playTime: Long) {
        if (GameRecord.values()[stage].grade == 0 || grade > GameRecord.values()[stage].grade) {
            GameRecord.values()[stage].grade = grade
            GameRecord.values()[stage].time = playTime
            GameRecord.values()[stage].moneySpent = GlobalRoundGame.moneySpent
            GameRecord.values()[stage].playerLevel = GlobalRoundGame.level
            GameRecord.values()[stage].monsterKilled = GlobalRoundGame.monsterKilled
        } else if (grade == GameRecord.values()[stage].grade && playTime < GameRecord.values()[stage].time) {
            GameRecord.values()[stage].grade = grade
            GameRecord.values()[stage].time = playTime
            GameRecord.values()[stage].moneySpent = GlobalRoundGame.moneySpent
            GameRecord.values()[stage].playerLevel = GlobalRoundGame.level
            GameRecord.values()[stage].monsterKilled = GlobalRoundGame.monsterKilled
        }
    }


    fun saveFile() {
        var dataString: MutableList<String> = mutableListOf()
        //player name
        dataString.add(playerName)
        //player usingStyling
        dataString.add(usingStyling.name)
        //player money
        dataString.add(playerMoney.toString())
        //styling
        for (i in 0..12) {
            dataString.add(Styling.values()[i].isBought.toString())
        }
        //stage record
        for (i in 0..3) {
            dataString.add(GameRecord.values()[i].grade.toString())
            dataString.add(GameRecord.values()[i].time.toString())
            dataString.add(GameRecord.values()[i].moneySpent.toString())
            dataString.add(GameRecord.values()[i].playerLevel.toString())
            dataString.add(GameRecord.values()[i].monsterKilled.toString())
            dataString.add(GameRecord.values()[i].canPlay.toString())
        }
        //achievement
        for (i in 0..9) {
            dataString.add(GlobalAchievement.Achievement.values()[i].finishedCount.toString())
            dataString.add(GlobalAchievement.Achievement.values()[i].finish.toString())
        }

        try {
            FileController().write("TheTimeHero", dataString)
        } catch (ex: IOException) {
        }
    }

    fun readFile(br: BufferedReader) {
        //player name
        playerName = br.readLine()
        //player usingStyling
        var styling = br.readLine()
        for (i in 0..12) {
            if (Styling.values()[i].stylingName == styling) {
                usingStyling = Styling.values()[i]
                break
            }
        }
        //player money
        playerMoney = br.readLine().toInt()
        //styling
        for (i in 0..12) {
            Styling.values()[i].isBought = br.readLine() == "true"
        }
        //stage record
        for (i in 0..3) {
            GameRecord.values()[i].grade = br.readLine().toInt()
            GameRecord.values()[i].time = br.readLine().toLong()
            GameRecord.values()[i].moneySpent = br.readLine().toInt()
            GameRecord.values()[i].playerLevel = br.readLine().toInt()
            GameRecord.values()[i].monsterKilled = br.readLine().toInt()
            GameRecord.values()[i].canPlay = br.readLine() == "true"
        }
        //achievement
        for (i in 0..9) {
            GlobalAchievement.Achievement.values()[i].finishedCount = br.readLine().toInt()
            GlobalAchievement.Achievement.values()[i].finish = br.readLine() == "true"
        }

    }


}