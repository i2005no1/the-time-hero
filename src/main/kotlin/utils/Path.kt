package utils

object Path {
    override fun toString(): String = ""

    object Imgs {
        override fun toString(): String = "$Path/imgs"

        object Actors {
            override fun toString(): String = "$Imgs/actors"

            var now = GlobalPlayer.usingStyling.stylingName
            var styling = "$this/$now.png"

            fun addPng(name: String): String = "$this/$name.png"

        }

        object Monsters {
            override fun toString(): String = "$Imgs/monsters"

            fun addPng(name: String): String = "$this/$name.png"

        }

        object Village {
            override fun toString(): String = "$Imgs/village"

            fun addPng(name: String): String = "$this/$name.png"

        }

        object Objs {
            override fun toString(): String = "$Imgs/objs"

            val RECT = "$this/rent.png"
            val LEFT_ANG = "$this/left_ang.png"
            val RIGHT_ANG = "$this/right_ang.png"
            val CONTROLLERFRAME = "$this/ControllerFrame.png"
            val ENTER_HINT = "$this/enterHint.png"
            val OPEN_HINT = "$this/openHint.png"
            val ATTACK_DEFENCE_SPEED = "$this/attackDefenceSpeed.png"
            val LIGHT = "$this/light.png"
            val CANNOTMOVE="$this/canNotMove.png"
            val BATTLESTRIKE="$this/battleStrike.png"
            val BOSSCASTLE="$this/bossCastle.png"
            val START = "$this/start.png"
            val COMPLETED = "$this/completed.png"
            val CHEST="$this/Chest.png"
            val ESC="$this/esc.png"
            val COUNTDOWN_NUM="$this/countdownText.png"
            val LOADING="$this/Loading.png"

            object Arms {
                override fun toString(): String = "$Objs/arms"
                fun addPng(name: String): String = "$this/$name.png"
            }

            object Armors {
                override fun toString(): String = "$Objs/armors"
                fun addPng(name: String): String = "$this/$name.png"
            }

            object Shoes {
                override fun toString(): String = "$Objs/shoes"
                fun addPng(name: String): String = "$this/$name.png"
            }

            object Herbs {
                override fun toString(): String = "$Objs/herbs"
                fun addPng(name: String): String = "$this/$name.png"
            }

            object Information {
                override fun toString(): String = "$Objs/information"
                val BLOOD_RED = "$this/blood original new.png"
                val BLOOD_MULTI="$this/blood multi.png"
                val BLOOD_GRAY="$this/blood back layer new.png"
                val EXP="$this/exp.png"
                val HEAD_BACKGROUND="$this/head background.png"
                val NUMBER="$this/number.png"
                val NUMBER_RED="$this/number_red.png"
                val NUMBER_SHADOW="$this/number_Dark.png"
                val MONEY="$this/money.png"
                val HERB="$this/herb.png"
                val CROSS="$this/cross.png"
                val FLEE_KEY="$this/keyboard_key_c.png"
                val USEHERB_KEY="$this/keyboard-key-x.png"
                val LEVEL_UP = "$this/LEVELUP.png"
                val GAME_OVER_ONE ="$this/GameOver1.png"
                val GAME_OVER_TWO ="$this/GameOverTwo.png"
                val LARGE_LABEL_90 ="$this/largeLabel90.png"
                val MULTI_DEFEATEDBOSS1="$this/devil_one_defeated.png"
                val MULTI_DEFEATEDBOSS2="$this/devil_two_defeated.png"
                val UNLOCK_NEW_WEAPONS="$this/unlocked  new weapons.png"
            }

        }

        object Backgrounds {
            override fun toString(): String = "$Imgs/backgrounds"

            //home
            val THE_TIME_HERO = "$this/TheTimeHero.png"
            val NEW_GAME = "$this/NEW GAME.png"
            val LOAD_GAME  = "$this/LOAD GAME.png"
            //game plot
            val GAME_PLOT  = "$this/GamePlot.png"
            val GAME_PLOT_TEXT  = "$this/GamePlotText.png"
            //input name
            val INPUT_NAME  = "$this/TypeName.png"
            //selection
            val SELECTION_STORY_MODE  = "$this/SelectionStroyMode.png"
            val SELECTION_GODDESS_ROOM  = "$this/SelectionGoddessRoom.png"
            val SELECTION_MULTI  = "$this/SelectionMultiplayer.png"
            //Goddess Room
            val GODDESS_ROOM_STYLING_SHOP  = "$this/StylingShopChoice.png"
            val GODDESS_ROOM_MY_WARDROBE  = "$this/MyWardrobeChoice.png"
            val GODDESS_ROOM_ACHIEVEMENT  = "$this/AchievementChoice.png"
            //Story Mode
            val STORY_MODE_STAGE_ONE = "$this/StageOne.png"
            val STORY_MODE_STAGE_TWO = "$this/StageTwo.png"
            val STORY_MODE_STAGE_THREE = "$this/StageThree.png"
            val STORY_MODE_STAGE_FOUR = "$this/StageFour.png"
            val CANNOT_PLAY = "$this/cannotPlay.png"
            //Styling Shop
            val STYLING_SHOP = "$this/StylingShop.png"
            //My Wardrobe
            val MY_WARDROBE = "$this/MyWardrobe.png"
            //Achievement
            val ACHIEVEMENT = "$this/Achievement.png"
            val ACHIEVEMENT_ARROWS = "$this/AchievementChoiceArrows.png"
            //jungle
            val OBJ_BLUEBG="$this/bluebg.png"
            val OBJ_REDBG="$this/redbg.png"
            val OBJ_GREENBG="$this/greenbg.png"
            val MAP="$this/map.png"
            //village
            object Village{
                override fun toString(): String = "$Imgs/backgrounds/village"
                fun addPng(name: String): String = "$this/$name.png"
            }

            //battle
            object Battle{
                override fun toString(): String = "$Imgs/backgrounds/battle"
                fun addPng(name: String): String = "$this/$name.png"
            }
            //game over
            val GAME_OVER_SCENE = "$this/GameOverScene.png"
            val GAME_OVER_TEXT = "$this/GameOverText.png"
            //multiplayer
            val MULTI_INPUT_NORMAL = "$this/MultiplayerInputNormal.png"
            val MULTI_INPUT_IP = "$this/MultiplayerInputIP.png"
            val MULTI_INPUT_PORT = "$this/MultiplayerInputPort.png"
            val MULTI_WAITING_ROOM_NORMAL = "$this/MultiWaitingRoomNormal.png"
            val MULTI_WAITING_ROOM_MASTER = "$this/MultiWaitingRoomMaster.png"
            val MULTI_GAME_OVER_BG = "$this/MultiGameOverScene.png"
            val MULTI_STATISTIC_BG = "$this/MultiStatisticScene.png"

        }
    }

    object Sounds {
        override fun toString(): String = "sounds"

        val HOME = "$this/Home.wav"
        val SELECTION = "$this/SELECTION.wav"
        val BATTLE = "$this/battle.wav"
        val CANNOT_MOVE = "$this/cantmove.wav"
        val CONFIRM = "$this/confirm.wav"
        val CONFIRM_JAP = "$this/confirm2.wav"
        val DASH = "$this/dash.wav"
        val NUMBER = "$this/battle.wav"//？？？
        val GAME_OVER = "$this/gameOver.wav"
        val HERB = "$this/herb.wav"
        val SELECTION_MOVE = "$this/leftright.wav"
        val LEVEL_UP = "$this/levelup.wav"
        val OPEN_CHEST = "$this/openChest.wav"
        val PRAY_TIME = "$this/pray.wav"
        val BUY_PROP = "$this/prop.wav"
        val RECOVERY_FOOD = "$this/recoveryFood.wav"
        val STAGE_1 = "$this/stage1.wav"
        val STAGE_2 = "$this/stage2.wav"
        val STAGE_3 = "$this/stage3.wav"
        val STAGE_4 = "$this/stage4.wav"

    }
}