package obj

import core.GameKernel
import core.utils.Delay
import obj.utils.Animator
import obj.utils.GameObject

import utils.Global
import utils.GlobalPlayer
import utils.GlobalRoundGame
import java.awt.Graphics
import java.awt.event.KeyEvent

open class Player(x: Int, y: Int, width: Int, height: Int) : GameObject(x, y, width, height) {
    private enum class walkingDir {
        UP, DOWN, LEFT, RIGHT, STOP
    }

    protected var animator: Animator = Animator(0, Animator.State.STOP, GlobalPlayer.usingStyling.stylingName, true)
    private var dir: Global.Direction = Global.Direction.DOWN
    private var name: String = GlobalPlayer.playerName
    private var dx = 10.0
    private var dy = 0.0
    private var isInvincible: Boolean = false
    private var isShow: Boolean = true
    private val showDelay: Delay = Delay(10).apply { loop() }
    private var isVillage: Boolean = false
    private var walkDir: walkingDir = walkingDir.STOP

    //BattleScene
    private var inBattleScene: Boolean = false

    //multiplayer
    fun getWalkDirString():String = walkDir.name
    //multiplayer
    fun setWalkDir(dirs :String){
        for(i in 0..4){
            if(dirs == walkingDir.values()[i].name){
                walkDir = walkingDir.values()[i]
                for(y in 0..3){
                    if(walkingDir.values()[i].name == Global.Direction.values()[y].name){
                        dir = Global.Direction.values()[y]
                    }
                }
                break
            }
        }
        if(walkDir == walkingDir.STOP) {
            animator.setSta(Animator.State.STOP)
        }else{
            animator.setSta(Animator.State.WALK)
        }
    }

    fun getDir(): Global.Direction {
        return dir
    }

    fun setDir(dir: Global.Direction) {
        this.dir = dir
    }

    fun isVillage() {
        isVillage = true
    }

    fun invincible() {
        isInvincible = true

    }

    fun getInvincible(): Boolean {
        return this.isInvincible
    }

    fun noInvincible() {
        isInvincible = false
    }

    fun inBattleScene() {
        inBattleScene = true;
        dir = Global.Direction.RIGHT
    }

    fun isShow() {
        isShow = true
    }

    private fun isShowOrNot() {
        isShow = !isShow
    }

    fun setAnimatorState(state: Animator.State) {
        animator.setSta(state)
    }


    fun setdx(dx: Double) {
        this.dx = dx
    }

    fun setdy(dx: Double) {
        this.dy = dy
    }

    fun friction() {
        if (dx > 10.0 || dx < -10.0) {
            dx *= 0.9
        }
    }

    private fun move() {
        friction()
        if (inBattleScene) {
            overBattleBoder()
        } else {
            overMapBoder()
        }
        translate(x = dx.toInt())
        translate(y = dy.toInt())

        if (dx in -10.0..10.0) {
            dx = 10.0
        }

    }

    private fun overMapBoder() {
        if (touchTop) {
            setCenter(painter.centerX, painter.height / 2)
        }
        if (touchBottom) {
            setCenter(painter.left, Global.SCREEN_Y - painter.height / 2)
        }
        if (touchRight) {
            setCenter(Global.SCREEN_X - painter.width / 2, painter.centerY)
        }
        if (touchLeft) {
            setCenter(painter.width / 2, painter.centerY)
        }

    }

    private fun overBattleBoder() {
        if (touchTop) {
            setCenter(painter.centerX, painter.height / 2)
        }
        if (touchBottom) {
            setCenter(painter.left, Global.SCREEN_Y - painter.height / 2)
        }
        if (touchScreenRight) {
            setCenter(Global.SCREEN_X - painter.width / 2, painter.centerY)
        }
        if (touchLeft) {
            setCenter(painter.width / 2, painter.centerY)
        }

    }

    override fun paintComponent(g: Graphics) {
        if (isShow) {
            animator.paint(dir, painter.left, painter.top, painter.right, painter.bottom, g)
        }
    }

    override fun update(timePassed: Long) {
        if (inBattleScene) {
            move()
        }
        animator.update()
        if (isInvincible) {
            if (showDelay.count()) {
                isShowOrNot()
            }
        }
    }

    override fun toString(): String {
        return name
    }

    private fun moveVillage(keyCode: Int) {
        if (keyCode == KeyEvent.VK_LEFT && !touchLeft) {
            animator.setSta(Animator.State.WALK)
            dir = Global.Direction.LEFT
            translate(x = -3 - GlobalRoundGame.speed)
        }
        if (keyCode == KeyEvent.VK_RIGHT && !touchScreenRight) {
            animator.setSta(Animator.State.WALK)
            dir = Global.Direction.RIGHT
            translate(x = 3 + GlobalRoundGame.speed)
        }
    }

    private fun move(keyCode: Int) {

        if (keyCode == KeyEvent.VK_UP && !touchTop && (walkDir == walkingDir.STOP || walkDir == walkingDir.UP)) {
            walkDir = walkingDir.UP
            animator.setSta(Animator.State.WALK)
            dir = Global.Direction.UP
            translate(y = -3 - GlobalRoundGame.speed)
        } else if (keyCode == KeyEvent.VK_DOWN && !touchBottom && (walkDir == walkingDir.STOP || walkDir == walkingDir.DOWN)) {
            walkDir = walkingDir.DOWN
            animator.setSta(Animator.State.WALK)
            dir = Global.Direction.DOWN
            translate(y = 3 + GlobalRoundGame.speed)
        } else if (keyCode == KeyEvent.VK_LEFT && !touchLeft && (walkDir == walkingDir.STOP || walkDir == walkingDir.LEFT)) {
            walkDir = walkingDir.LEFT
            animator.setSta(Animator.State.WALK)
            dir = Global.Direction.LEFT
            translate(x = -3 - GlobalRoundGame.speed)
        } else if (keyCode == KeyEvent.VK_RIGHT && !touchRight && (walkDir == walkingDir.STOP || walkDir == walkingDir.RIGHT)) {
            walkDir = walkingDir.RIGHT
            animator.setSta(Animator.State.WALK)
            dir = Global.Direction.RIGHT
            translate(x = 3 + GlobalRoundGame.speed)
        }


    }

    private fun stop() {
        animator.setSta(Animator.State.STOP)
        walkDir = walkingDir.STOP
    }

    override val input: ((GameKernel.Input.Event) -> Unit)? = { e ->
        run {
            when (e) {
                is GameKernel.Input.Event.KeyKeepPressed ->
                    if (isVillage) {
                        moveVillage(e.data.keyCode)
                    } else {
                        move(e.data.keyCode)
                    }
                is core.GameKernel.Input.Event.KeyReleased -> stop()
            }
        }
    }
}
