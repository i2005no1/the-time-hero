package obj

import core.controllers.ResController
import obj.utils.GameObject
import utils.GlobalPlayer
import utils.GlobalRoundGame
import utils.Path
import java.awt.Graphics
import java.awt.Image

abstract class Props(val x: Int, val y: Int, val width: Int, val height: Int) : GameObject(x, y, width, height) {
    abstract val img: Image
    abstract val name: String
    abstract val price: Int
    abstract val explanation: String
    abstract val addAbilityValue: Int
    abstract val addAttackValue: Int
    abstract val addDefenceValue: Int
    abstract val addSpeedValue: Int

    var isFocus: Boolean = false
    var riseY: Int = 0


    fun paintRise() {
        if (riseY < 16) {
            translate(y = -2)
            riseY++
        }
    }

    fun paintReset() {
        if (riseY > 0) {
            translate(y = 2)
            riseY--
        }
    }

    fun buyAction(): Boolean {
        if(GlobalRoundGame.roundMoney - price >= 0){
            GlobalRoundGame.roundMoney = GlobalRoundGame.roundMoney - price
            GlobalRoundGame.moneySpent += price
            return true
        }
        return false
    }

    override fun paintComponent(g: Graphics) {
        g.drawImage(img, painter.left, painter.top, painter.width, painter.height, null)
    }

    override fun update(timePassed: Long) {
        if (isFocus) {
            paintRise()
        } else {
            paintReset()
        }
    }


    class PropsArm(x: Int, y: Int, width: Int, height: Int, val arm: GlobalPlayer.Arms) : Props(x, y, width, height) {
        override val img = ResController.instance.image(Path.Imgs.Objs.Arms.addPng(arm.armsName))
        //override val img = ResController.instance.image(Path.Imgs.Objs.Arms.addPng("Time Spear"))
        override val name: String = this.arm.armsName
        override val price: Int = this.arm.price
        override val explanation: String = this.arm.explanation
        override val addAbilityValue: Int = this.arm.addAttack
        override val addAttackValue: Int = addAbilityValue - GlobalRoundGame.usingArms.addAttack
        override val addDefenceValue: Int = 0
        override val addSpeedValue: Int = 0
    }


    class PropsArmor(x: Int, y: Int, width: Int, height: Int, val armor: GlobalPlayer.Armors) : Props(x, y, width, height) {
        override val img = ResController.instance.image(Path.Imgs.Objs.Armors.addPng(armor.armorsName))
        //override val img = ResController.instance.image(Path.Imgs.Objs.Armors.addPng("Wood Sword"))
        override val name: String = this.armor.armorsName
        override val price: Int = this.armor.price
        override val explanation: String = this.armor.explanation
        override val addAbilityValue: Int = this.armor.addDefence
        override val addAttackValue: Int = 0
        override val addDefenceValue: Int = addAbilityValue - GlobalRoundGame.usingArmors.addDefence
        override val addSpeedValue: Int = 0

    }

    class PropsShoes(x: Int, y: Int, width: Int, height: Int, val shoes: GlobalPlayer.Shoes) : Props(x, y, width, height) {
        override val img = ResController.instance.image(Path.Imgs.Objs.Shoes.addPng(shoes.shoesName))
        //override val img = ResController.instance.image(Path.Imgs.Objs.Shoes.addPng("Wood Sword"))
        override val name: String = this.shoes.shoesName
        override val price: Int = this.shoes.price
        override val explanation: String = this.shoes.explanation
        override val addAbilityValue: Int = this.shoes.addSpeed
        override val addAttackValue: Int = 0
        override val addDefenceValue: Int = 0
        override val addSpeedValue: Int = addAbilityValue - GlobalRoundGame.usingShoes.addSpeed

    }

    class PropsHerb(x: Int, y: Int, width: Int, height: Int, val herb: GlobalPlayer.Herbs) : Props(x, y, width, height) {
        override val img = ResController.instance.image(Path.Imgs.Objs.Herbs.addPng("Herbs"))
        override val name: String = this.herb.herbsName
        override val price: Int = this.herb.price
        override val explanation: String = this.herb.explanation
        override val addAbilityValue: Int = GlobalRoundGame.hpLimit
        override val addAttackValue: Int = 0
        override val addDefenceValue: Int = 0
        override val addSpeedValue: Int = 0


    }


}