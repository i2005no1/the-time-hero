package obj

import core.controllers.ResController
import obj.utils.GameObject
import utils.Path
import java.awt.Graphics

class BossCastle (x: Int,y: Int,width: Int,height: Int) : GameObject(x,y,width,height){
    private val img = ResController.instance.image(Path.Imgs.Objs.BOSSCASTLE)

    override fun paintComponent(g: Graphics) {
        g.drawImage(img, painter.left, painter.top, painter.width, painter.height, null)
    }

    override fun update(timePassed: Long) {
    }
}