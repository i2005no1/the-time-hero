package obj

import core.controllers.ResController
import core.utils.Delay
import obj.utils.Animator
import obj.utils.GameObject
import utils.Global
import utils.GlobalPlayer
import utils.Path
import java.awt.Graphics
import java.awt.Image

class Monster(x: Int, y: Int, width: Int, height: Int, val monsterEnum: GlobalPlayer.Monsters) : GameObject(x, y, width, height) {
    private var animator: Animator = Animator(0, Animator.State.WALK, monsterEnum.monsterName)
    private var dir: Global.Direction = Global.Direction.DOWN
    private var dx = -10.0
    private var dy = 0


    enum class State {
        ALIVE, DEAD
    }

    //monster information
    val name: String = monsterEnum.monsterName
    var hp: Int = monsterEnum.hpLimit
    val hpLimit: Int = monsterEnum.hpLimit
    val attack: Int = monsterEnum.attack
    val defence: Int = monsterEnum.defence
    var state: State = State.ALIVE
    val exp: Int = monsterEnum.exp
    val money: Int = monsterEnum.money

    //BattleScene
    private var inBattleScene: Boolean = false
    private var moveGoalX: Int = x
    private var moveGoalY: Int = y

    //jungleMove
    private val moveLimitDistanceX: Int = 50
    private val moveLimitDistanceY: Int = 50
    private val startX: Int = x
    private val startY: Int = y
    private val moveDelay: Delay = Delay(60).apply { loop() }
    private val reborn: Delay = Delay(300).apply { loop() }//復活cd
    private fun setMoveGoal() {
        moveGoalX = Global.random(startX - moveLimitDistanceX, startX + moveLimitDistanceX)
        moveGoalY = Global.random(startY - moveLimitDistanceY, startY + moveLimitDistanceY)
    }

    fun getDirString() :String{
        return dir.name
    }

    fun setDir(dirs: String){
        for(i in 0..3){
            if(dirs == Global.Direction.values()[i].name){
                dir = Global.Direction.values()[i]
                break
            }
        }
    }

    private fun jungleMove() {
        if (painter.left > moveGoalX) {
            translate(x = -1)
            dir = Global.Direction.LEFT
            animator.setSta(Animator.State.WALK)
        } else if (painter.left < moveGoalX) {
            translate(x = 1)
            dir = Global.Direction.RIGHT
            animator.setSta(Animator.State.WALK)
        } else if (painter.top < moveGoalY) {
            translate(y = 1)
            dir = Global.Direction.DOWN
            animator.setSta(Animator.State.WALK)
        } else if (painter.top > moveGoalY) {
            translate(y = -1)
            dir = Global.Direction.UP
            animator.setSta(Animator.State.WALK)
        } else {
            animator.setSta(Animator.State.STOP)
            if (moveDelay.count()) {
                setMoveGoal()
            }
        }
    }

    fun inBattleScene() {
        inBattleScene = true;
    }

    fun setdx(dx: Double) {
        this.dx = dx
    }

    private fun friction() {
        if (dx > 10.0 || dx < -10.0) {
            dx *= 0.9
        }
    }



    private fun battleMove() {
        friction()
        if(inBattleScene){
            overBattleBoder()
        }else{
            overMapBoder()
        }
        translate(x = dx.toInt());
        translate(y = -dy);
        if (dx >= -10.0 && dx <= 10.0) {
            dx = -10.0
        }
    }

    private fun overMapBoder() {
        if (touchTop) {
            setCenter(painter.centerX, painter.height / 2);
        }
        if (touchBottom) {
            setCenter(painter.left, Global.SCREEN_Y - painter.height / 2);
        }
        if (touchRight) {
            setCenter(Global.SCREEN_X - painter.width / 2, painter.centerY);
        }
        if (touchLeft) {
            setCenter(painter.width / 2, painter.centerY);
        }

    }

    private fun overBattleBoder(){
        if (touchTop) {
            setCenter(painter.centerX, painter.height / 2);
        }
        if (touchBottom) {
            setCenter(painter.left, Global.SCREEN_Y - painter.height / 2);
        }
        if (touchScreenRight) {
            setCenter(Global.SCREEN_X - painter.width / 2, painter.centerY);
        }
        if (touchLeft) {
            setCenter(painter.width / 2, painter.centerY);
        }

    }

    private fun reborn(){
        if(state==State.DEAD&&reborn.count()){
            state=State.ALIVE
        }
    }

    override fun paintComponent(g: Graphics) {
        if(state==State.ALIVE) {
            animator.paint(dir, painter.left, painter.top, painter.right, painter.bottom, g)
        }
    }

    fun animatorUpdate(){
        animator.update()
    }

    override fun update(timePassed: Long) {
        if(state==State.ALIVE) {
            if (inBattleScene) {
                dir = Global.Direction.LEFT
                battleMove()
            } else {
                jungleMove()
            }
            animator.update()
        }
        reborn()
    }

    override fun toString(): String {
        return name;
    }

}