package obj

import core.controllers.ResController
import obj.utils.GameObject
import utils.Path
import java.awt.Color
import java.awt.Graphics

class MapCollision(x: Int, y: Int, w: Int, h: Int) : GameObject(x, y, w, h) {
    private val img = ResController.instance.image(Path.Imgs.Objs.CANNOTMOVE)

    override fun paintComponent(g: Graphics) {

    }

    override fun update(timePassed: Long) {

    }
}