
package obj.utils

import core.controllers.ResController
import core.utils.Delay
import utils.Global
import utils.Path
import java.awt.Graphics

class Animator (private var type: Int,private var state: State){

    enum class State(val arr: kotlin.IntArray, val speed: Int){
        WALK(intArrayOf(0,1,2,3) , 6),
        SLOW(intArrayOf(1,3,1,3) , 15),
        STOP(intArrayOf(0,0,0,0) , 100)
    }

    constructor( type: Int, state: State, monsterName: String): this(type,state){
        img = ResController.instance.image(Path.Imgs.Monsters.addPng(monsterName))
    }

    constructor( type: Int, state: State, stylingName: String, isStyling: Boolean): this(type,state){
        img = ResController.instance.image(Path.Imgs.Actors.addPng(stylingName))
    }


    private var img = ResController.instance.image(Path.Imgs.Actors.styling)
    private val delay: Delay = Delay(state.speed).apply{loop()}
    private var count: Int = 0

    fun setSta(state: State){
        this.state = state
        delay.countLimit = state.speed
    }



    fun paint(dir: Global.Direction, left: Int, top: Int, right: Int, bottom: Int, g: Graphics){
            g.drawImage(img,
                    left, top,
                    right, bottom,
                    (type % 4) * Global.UNIT_X * 4 + Global.UNIT_X * state.arr[count],
                    (type / 4) * Global.UNIT_Y * 4 + Global.UNIT_Y * dir.ordinal,
                    (type % 4) * Global.UNIT_X * 4 + Global.UNIT_X + Global.UNIT_X * state.arr[count],
                    (type / 4) * Global.UNIT_Y * 4 + Global.UNIT_Y + Global.UNIT_Y * dir.ordinal, null);

    }

    fun update(){
        if(delay.count()){
            count = ++count % state.arr.size;
        }
    }

    fun play(){
        delay.play()
    }

    fun pause(){
        delay.pause()
        count = 0
    }


}

