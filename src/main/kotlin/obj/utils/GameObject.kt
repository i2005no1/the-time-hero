package obj.utils

import core.GameKernel
import core.GameKernel.GameInterface
import utils.Global
import java.awt.Color
import java.awt.Graphics


abstract class GameObject(val collider: Rect, val painter: Rect = Rect(collider)) : GameInterface {

    constructor(
            x: Int, y: Int, width: Int, height: Int,
            x2: Int = x, y2: Int = y, width2: Int = width, height2: Int = height
    ) : this(
            collider = Rect.genWithCenter(x, y, width, height),
            painter = Rect.genWithCenter(x2, y2, width2, height2)
    )

    constructor(
            x: Int, y: Int, width: Int, height: Int
    ) : this(
            collider = Rect.genWithCenter(x, y, width, height),
            painter = Rect.genWithCenter(x, y, width, height)
    )


    val outOfScreen: Boolean
        get() {
            if (painter.bottom <= 0) {
                return true
            }
            if (painter.right <= 0) {
                return true
            }
            return if (painter.left >= Global.SCREEN_X) {
                true
            } else painter.top >= Global.SCREEN_Y
        }

    val touchTop: Boolean
        get() = collider.top <= 0

    val touchLeft: Boolean
        get() = collider.left <= 0

    val touchScreenRight: Boolean
        get() = collider.right >= Global.SCREEN_X

    val touchScreenBottom: Boolean
        get() = collider.bottom >= Global.SCREEN_Y

    val touchRight: Boolean
        get() = collider.right >= Global.JUNGLE_MAP_WIDTH

    val touchBottom: Boolean
        get() = collider.bottom >= Global.JUNGLE_MAP_HEIGHT

    fun isCollision(obj: GameObject): Boolean {
        return collider.overlap(obj.collider)
    }

    fun <T : GameObject> isCollision(obj: List<T>): List<T> {
        return obj.filter { it.collider.overlap(collider) }
    }

    fun translate(x: Int = 0, y: Int = 0) {
        collider.translate(x, y)
        painter.translate(x, y)
    }

    fun setSize(width: Int, height: Int){
        collider.serSize(width,height)
        painter.serSize(width,height)
    }

    fun setCenter(x: Int=0, y: Int=0) {
        collider.setCenter(x, y)
        painter.setCenter(x, y)
    }

    override fun paint(g: Graphics) {
        paintComponent(g)
        if (Global.IS_DEBUG) {
            g.color = Color.RED
            g.drawRect(painter.left, painter.top, painter.width, painter.height)
            g.color = Color.BLUE
            g.drawRect(collider.left, collider.top, collider.width, collider.height)
            g.color = Color.BLACK
        }
    }

    override val input: ((GameKernel.Input.Event) -> Unit)? = null

    abstract fun paintComponent(g: Graphics)
}