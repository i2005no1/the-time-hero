package obj

import core.controllers.ResController
import obj.utils.GameObject
import utils.GlobalPlayer
import utils.Path
import java.awt.Graphics

class Npc(x: Int,y: Int,width: Int,height: Int,private val npcEnum: GlobalPlayer.Npc): GameObject(x,y,width,height) {

    private val img = ResController.instance.image(Path.Imgs.Actors.addPng(npcEnum.npcName))

    override fun paintComponent(g: Graphics) {
        g.drawImage(img,painter.left,painter.top,painter.width,painter.height,null)
    }

    override fun update(timePassed: Long) {
    }

}