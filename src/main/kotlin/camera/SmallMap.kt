package camera


import obj.utils.GameObject
import java.awt.Color
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.Image


class SmallMap(cam: Camera, smallMapZoomX: Double, smallMapZoomY: Double) : Camera(cam) {
    private var smallMapZoomX : Double// 小地圖的X縮放率
    private var smallMapZoomY : Double// 小地圖的Y縮放率

    init {
        if (cam.obj() == null) { // 第一次自動對焦
            setObj(this)
        }
        this.smallMapZoomX = smallMapZoomX
        this.smallMapZoomY = smallMapZoomY
        setChaseDivisorX(1.0)
        setChaseDivisorY(1.0)
    }

    /*
    以下paint 為提供常用的方法overloading目前除放入圖片外，皆是畫長方形，如果要改成其他形狀，
    請將fillRect 做調整（如不想填滿顏色，請用draw + 形狀）
    */
    //將物件轉換成方格畫出，自動將該類型物件生成相同顏色，大小依物件縮放
    fun paint(g: Graphics, obj: GameObject) {
        val c = getColor("" + obj.javaClass)
        g.color = c
        g.fillRect(obj.painter.left, obj.painter.top,
                obj.painter.width, obj.painter.height)
        g.color = Color.BLACK
    }

    //將物件轉換成方格畫出，自動將該類型物件生成相同顏色，大小自訂
    fun paint(g: Graphics, obj: GameObject, width: Int, height: Int) {
        val c = getColor("" + obj.javaClass)
        g.color = c
        g.fillRect(obj.painter.left, obj.painter.top,
                width, height)
        g.color = Color.BLACK
    }

    //將物件轉換成方格畫出，顏色自訂，大小依物件縮放
    fun paint(g: Graphics, obj: GameObject, c: Color?) {
        g.color = c
        g.fillRect(obj.painter.left, obj.painter.top,
                obj.painter.width, obj.painter.height)
        g.color = Color.BLACK
    }

    //將物件轉換成方格畫出，顏色自訂
    fun paint(g: Graphics, obj: GameObject, c: Color?, width: Int, height: Int) {
        g.color = c
        g.fillRect(obj.painter.left, obj.painter.top,
                width, height)
        g.color = Color.BLACK
    }

    // 小地圖的物件顯示圖片（可以相同也可以自己放），大小依照物件本身縮小
    fun paint(g: Graphics, obj: GameObject, img: Image?) {
        g.drawImage(img, obj.painter.left, obj.painter.top,
                obj.painter.width, obj.painter.height, null)
    }

    // 小地圖的物件顯示圖片（可以相同也可以自己放），大小自訂
    fun paint(g: Graphics, obj: GameObject, img: Image?, width: Int, height: Int) {
        g.drawImage(img, obj.painter.left, obj.painter.top, width, height, null)
    }

    //畫追蹤物件的鏡頭框大小，camera 請放追蹤物件的主鏡頭，將在小地圖上顯示目前主鏡頭可見的範圍。
    fun paint(g: Graphics, camera: Camera, color: Color?) {
        g.color = color
        var targetX: Int = camera.obj()!!.painter.centerX - camera.painter.width / 2
        var targetY: Int = camera.obj()!!.painter.centerY - camera.painter.height / 2
        if (targetX < painter.left) {
            targetX = painter.left
        }
        if (targetY < painter.top) {
            targetY = painter.top
        }
        if (targetX > painter.right - camera.painter.width) {
            targetX = painter.right - camera.painter.width
        }
        if (targetY > painter.bottom - camera.painter.height) {
            targetY = painter.bottom - camera.painter.height
        }
        g.drawRect(targetX, targetY,
                camera.painter.width, camera.painter.height)
        g.color = Color.BLACK
    }

    override fun start(g: Graphics) {
        val g2d = g as Graphics2D //Graphics2D 為Graphics的子類，先做向下轉型。
        tmpCanvas = g2d.transform // 暫存畫布
        g2d.scale(smallMapZoomX, smallMapZoomY) // 將畫布整體做縮放 ( * 縮放的係數)
        // 先將畫布初始移到(0,0) 然後再到自己想定位的地點(+ cameraWindowX. Y)，因為有被縮放的話要將為位移點調整-> (/ 縮放的係數)
        g2d.translate(-painter.left + cameraWindowX() / smallMapZoomX, -painter.top + cameraWindowY() / smallMapZoomY)
        // 將畫布依照鏡頭大小作裁切
        g.setClip(painter.left, painter.top, painter.width, painter.height)
    }

    fun getColor(str: String): Color {
        var colorCode = 0
        val arr = str.toCharArray()
        for (i in arr.indices) {
            colorCode += arr[i].toInt()
        }
        val colorCodeArr = IntArray(3)
        val temp = intArrayOf(colorCode % 255, colorCode % 175, colorCode % 90) // 裝可能性，將得到的數字分不同參數做mod運算，以獲得差異較大的色碼
        for (i in 0..1) {
            colorCodeArr[i] = temp[i]
        }
        return Color(colorCodeArr[0], colorCodeArr[1], colorCodeArr[2])
    }

    fun setSmallMapZoomX(num: Double) {
        smallMapZoomX = num
    }
    fun smallMapZoomX(): Double {
        return smallMapZoomX
    }
    fun setSmallMapZoomY(num: Double) {
        smallMapZoomY = num
    }
    fun smallMapZoomY(): Double {
        return smallMapZoomY
    }


}

