package menu

import javafx.scene.Scene
import java.awt.Graphics
import java.awt.Graphics2D

abstract class PopupWindow(var x: Int, var y: Int, var width: Int, var height: Int): core.Scene()  {
    var isShow: Boolean = false
    var isCancelable: Boolean = false


    fun isCancelable() {
        isCancelable = true
    }

    fun show(){
        isShow = true
    }


    fun hide() {
        isShow = false;
    }

    fun getIsShow(): Boolean = this.isShow


    abstract fun paintWindow(g: Graphics)


    final override fun paint(g: Graphics) {
        if(g is Graphics2D){
            var tf = g.getTransform()
            g.translate(x, y)
            paintWindow(g)
            g.setTransform(tf)
        }
    }


}