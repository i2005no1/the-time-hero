package menu

import core.GameKernel
import core.utils.Delay
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.Toolkit
import java.awt.event.KeyEvent

class EditText(private val x: Int, private val y: Int, private val width: Int, private val height: Int, private val defaultText: String?) : GameKernel.GameInterface {
    private val defaultTextColor: Color? = Color.GRAY
    private var isFocus = false

    //cursor
    private val cursorSpeed: Delay? = Delay(utils.Global.PAINT_FREQ / 2).apply { loop() }
    private val cursorColor: Color? = Color.WHITE
    private var isCursorLight = false
    private val cursorWidth = 3
    private var cursorHeight = 20

    //edit
    private val editLimit = 15
    private var editText: String = ""

    fun changeCursorIsLight() {
        isCursorLight = !isCursorLight
    }

    fun setEditText(text:String){
        editText=text
    }

    private fun isOverEditLimit(): Boolean {
        return editText!!.length >= editLimit
    }

    private fun isLegalRange(c: Int): Boolean {
        if (c in 46..57) {
            return true
        }
        if (c in 30..36) {
            return true
        }
        if (c in 65..90) {
            return true
        }
        if (c in 96..111) {
            return true
        }
        return false
    }

    fun isFocus() {
        isFocus = true
    }

    fun unFocus() {
        isFocus = false
    }

    fun getEditText(): String = this.editText

    fun getFocus(): Boolean = this.isFocus

    override fun paint(g: Graphics) {
        g.font = Font("", Font.BOLD, 30)
        g.color = if (editText!!.isEmpty()) defaultTextColor else Color.WHITE
        g.drawString(if (editText!!.isEmpty()) defaultText else editText, x, y + height / 2 + g.fontMetrics.descent) //文字位置
        if (isFocus) {
            if (cursorSpeed!!.count()) {
                changeCursorIsLight()
            }
            if (isCursorLight) {
                g.color = cursorColor
                val stringWidth = g.fontMetrics.stringWidth(editText)
                cursorHeight = g.fontMetrics.ascent //自動偵測目前字體高度並設置 但使用者這樣就無法自定義
                g.fillRect(x + stringWidth, y + height / 2 - (cursorHeight + g.fontMetrics.descent) / 2, cursorWidth, cursorHeight)
            }
        }
    }

    override fun update(timePassed: Long) {
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { e ->
            run {
                when (e) {
                    is GameKernel.Input.Event.KeyPressed -> {
                        if (isFocus) {
                            if (e.data.keyCode == KeyEvent.VK_BACK_SPACE) {
                                if (editText!!.isNotEmpty()) {
                                    editText = editText!!.substring(0, editText!!.length - 1)
                                }
                            } else if (!isOverEditLimit() && e.data.keyCode != KeyEvent.VK_SHIFT && isLegalRange(e.data.keyCode)) {
                                editText += e.data.keyChar
                            }
//                            } else if (isOverEditLimit() && isLegalRange(e.data.keyChar) && Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK)) {
//                                editText += e.data.keyChar
//                            } else if (isOverEditLimit() && e.data.keyChar.toInt() >= 65 && e.data.keyChar.toInt() <= 90) {
//                                editText += (e.data.keyChar.toInt() + 32).toChar()
//                            }
                        }
                    }
                }
            }
        }

}