import core.GameInterfaceImpl
import core.GameKernel
import scene.*
import utils.Global
import utils.GlobalRoundGame
import javax.swing.JFrame

fun main() {

    val gk = GameKernel(Global.UPDATE_FREQ, Global.PAINT_FREQ, GameInterfaceImpl(HomeScene1()))

    if(Global.ATK_DEBUG){
        GlobalRoundGame.atkDebug()
    }

    JFrame().apply {
        title = "The Time Hero"
        setSize(Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT+39)
        defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        add(gk)
        isVisible = true
        gk.play(Global.IS_DEBUG)
    }

}