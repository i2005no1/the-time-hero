package core.controllers

import utils.GlobalPlayer.readFile
import java.io.*

class FileController {
    @Throws(IOException::class)
    fun read(name: String) {
        val br = BufferedReader(FileReader("$name.txt"))
        readFile(br)
//        while(br.ready()){
//            System.out.println(br.readLine());
//        }
        br.close()
    }

    @Throws(IOException::class)
    fun write(name: String, data: List<String>) {
        val br = BufferedWriter(FileWriter("$name.txt"))
        for(str in data){
            br.write(str + "\n");
        }
        br.flush()
        br.close()
    }
}