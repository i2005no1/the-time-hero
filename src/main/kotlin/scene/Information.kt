package scene

import com.sun.prism.paint.Color
import core.GameKernel
import core.Scene
import core.controllers.ResController
import core.utils.Delay
import utils.Global
import utils.GlobalPlayer
import utils.GlobalRoundGame
import utils.Path
import java.awt.Font
import java.awt.Graphics
import java.awt.Image


class Information : Scene() {
    class TeamMemberInformation(
        x: Int,
        y: Int,
        width: Int,
        height: Int,
        var multiPlayers: MutableList<MultiRoundGame.Multiplayer>
    ) : menu.PopupWindow(x, y, width, height) {
        private val head: MutableList<Image> = mutableListOf()
        private val headBackground: Image = ResController.instance.image(Path.Imgs.Objs.Information.HEAD_BACKGROUND)
        private val bloodGray: Image = ResController.instance.image(Path.Imgs.Objs.Information.BLOOD_GRAY)
        private val multiBlood: Image = ResController.instance.image(Path.Imgs.Objs.Information.BLOOD_MULTI)

        fun addHead(stylingName: String) {
            head += ResController.instance.image(Path.Imgs.Actors.addPng(stylingName))
        }

        fun updateMultiplayer(a: MutableList<MultiRoundGame.Multiplayer>) {
            multiPlayers = a
        }

        override fun paintWindow(g: Graphics) {
            for (i in 0 until multiPlayers.size) {
                g.drawImage(headBackground, x + width / 2 - 145, y + 15 + i * 60, 70, 55, null)
                g.drawImage(
                    head[i],
                    x + width / 2 - 143,
                    y + 15 + 8 + i * 60,
                    x + width / 2 + Global.UNIT_X * 2 - 143,
                    y + 15 + 8 + 19 * 2 + i * 60,
                    0,
                    0,
                    Global.UNIT_X,
                    19,
                    null
                )
                g.drawImage(bloodGray, x + width / 2 - 70, y + 35 + i * 60, 225, 20, null)
                g.drawImage(
                    multiBlood, x + width / 2 - 70, y + 35 + i * 60,
                    (multiPlayers[i].hp.toDouble() / multiPlayers[i].hpLimit.toDouble() * 225).toInt(), 20, null
                )
                g.font = Font("", Font.BOLD, 15)
                g.color = java.awt.Color.WHITE
                g.drawString(
                    "" + multiPlayers[i].hp + " / " + multiPlayers[i].hpLimit,
                    x + width / 2 + 5 + 20,
                    y + 51 + i * 60
                )
                g.color = java.awt.Color.black
                g.font = Font("", Font.BOLD, 20)
                g.drawString("LEVEL:" + multiPlayers[i].level, x + width / 2 + 2, y + 32 + i * 60)
                g.color = java.awt.Color.GREEN
                g.font = Font("", Font.BOLD, 20)
                g.drawString("LEVEL:" + multiPlayers[i].level, x + width / 2, y + 30 + i * 60)
            }
        }

        override fun sceneBegin() {
        }

        override fun sceneEnd() {
        }

        override fun update(timePassed: Long) {
        }

        override val input: ((GameKernel.Input.Event) -> Unit)?
            get() = {}
    }

    var teamMemberInformation: TeamMemberInformation? = null

    fun addTeamMember(x: Int, y: Int, width: Int, height: Int, multiPlayers: MutableList<MultiRoundGame.Multiplayer>) {
        teamMemberInformation = TeamMemberInformation(x, y, width, height, multiPlayers)
    }

    enum class GameOverText(val string: String) {
        NAME("Time Goddess"),
        ONE_1("Hear, hear!"),
        ONE_2("Good work everyone~"),
        TWO_1("Time for the results, this is so exciting!"),
        TWO_2("Wonder if there's a true hero among you.")
    }

    class CompareTime(var x: Int, var y: Int) {
        var img = ResController.instance.image(Path.Imgs.Objs.COUNTDOWN_NUM)
        var Hint = 0
        var count = 1
        var isStart = false

        fun paint(g: Graphics) {
            if (Hint <= 5) {
                g.drawImage(img, x, y, x + 300, y + 200, 0 + Hint * 300, 0, 300 + Hint * 300, 200, null)
            }
        }

        fun update(timePassed: Long) {
            if (Hint <= 5) {
                if (count < 61) {
                    count++
                } else {
                    count = 1
                    Hint++
                    if (Hint > 5) {
                        isStart = true
                    }
                }
            }
        }
    } //開始倒數

    private var compareTime = CompareTime(375, 230)

    var newTextIndex: Int = 0
    var printText: String = ""
    private val head: Image =
        ResController.instance.image(Path.Imgs.Actors.addPng(GlobalPlayer.usingStyling.stylingName))
    private val bloodRed: Image = ResController.instance.image(Path.Imgs.Objs.Information.BLOOD_RED)
    private val bloodGray: Image = ResController.instance.image(Path.Imgs.Objs.Information.BLOOD_GRAY)
    private val exp: Image = ResController.instance.image(Path.Imgs.Objs.Information.EXP)
    private val headBackground: Image = ResController.instance.image(Path.Imgs.Objs.Information.HEAD_BACKGROUND)

    private val money: Image = ResController.instance.image(Path.Imgs.Objs.Information.MONEY)
    private val moneyBg: Image = ResController.instance.image(Path.Imgs.Backgrounds.OBJ_REDBG)
    private val headBg: Image = ResController.instance.image(Path.Imgs.Backgrounds.OBJ_GREENBG)
    private val herb: Image = ResController.instance.image(Path.Imgs.Objs.Information.HERB)
    private val herbHint: Image = ResController.instance.image(Path.Imgs.Objs.Information.USEHERB_KEY)
    private val cross: Image = ResController.instance.image(Path.Imgs.Objs.Information.CROSS)

    //level up
    private val levelUp: Image = ResController.instance.image(Path.Imgs.Objs.Information.LEVEL_UP)
    private val unlockNewWeapon:Image = ResController.instance.image(Path.Imgs.Objs.Information.UNLOCK_NEW_WEAPONS)
    private var levelUpX: Int = Global.WINDOW_WIDTH / 2
    var showLevelUp: Boolean = false
    private var levelUpTime: Int = 60

    //multiBossDefeated
    private val bossOnedefeated: Image = ResController.instance.image(Path.Imgs.Objs.Information.MULTI_DEFEATEDBOSS1)
    private val bossTwodefeated: Image = ResController.instance.image(Path.Imgs.Objs.Information.MULTI_DEFEATEDBOSS2)
    var showBossOneDefeated: Boolean = false
    var showBossTwoDefeated: Boolean = false
    private var bossDefeatedSize: Double = 1.0
    private var bossDefeatedTime: Int = 120


    //enterHint
    var showEnterImg: Boolean = false
    private val enterHint = ResController.instance.image(Path.Imgs.Objs.ENTER_HINT)
    var showOpenImg: Boolean = false
    private val openHint = ResController.instance.image(Path.Imgs.Objs.OPEN_HINT)

    //popupWindow
    val informationPopupWindow: InformationPopupWindow =
        InformationPopupWindow(Global.SCREEN_X - 200, 0, 100, 150)
    val escPopupWindow: EscPopupWindow =
        EscPopupWindow(430, 0, 100, 150)


    //time
    var time = 30.0
    private val timeNum = ResController.instance.image(Path.Imgs.Objs.Information.NUMBER)
    private val timeRed = ResController.instance.image(Path.Imgs.Objs.Information.NUMBER_RED)
    private val timeShadow = ResController.instance.image(Path.Imgs.Objs.Information.NUMBER_SHADOW)
    private val timeDelay: Delay = Delay(10).apply { loop() }
    private var showTime = true

    //game over and time goddess talk
    private val gameOverOne = ResController.instance.image(Path.Imgs.Objs.Information.GAME_OVER_ONE)
    private val largeLabel = ResController.instance.image(Path.Imgs.Objs.Information.LARGE_LABEL_90)
    private var gameOverOneY: Int = 90
    private var largeLabelY: Int = 50
    private var textDelay: Int = 240


    fun resetTime() {  //重置時間
        time = 30.00
    }

    private fun getNoMoreThanTwoDigits(number: Double): String { //小數格式化
        if (time < 10) {
            return "0" + String.format("%.2f", number).replace(".", "")
        }
        return String.format("%.2f", number).replace(".", "")
    }

    private fun paintLevelUp(g: Graphics) {
        if(GlobalRoundGame.level==15||GlobalRoundGame.level==30){
            g.drawImage(unlockNewWeapon, levelUpX+220, Global.WINDOW_HEIGHT / 12 * 5 - 75,678,215, null)
        }else {
            g.drawImage(levelUp, levelUpX, 0, null)
        }
        levelUpX -= 30
        if (levelUpX <= 0) {
            levelUpX = 0
            levelUpTime--
            if (levelUpTime <= 0) {
                showLevelUp = false
                levelUpX = Global.WINDOW_WIDTH / 2
                levelUpTime = 60
            }
        }
    }

    private fun paintBossDefeated(g: Graphics, bossNum: Int) {
        var width = (561 * (bossDefeatedSize / 60)).toInt()
        var height = (344 * (bossDefeatedSize / 60)).toInt()
        when (bossNum) {
            1 -> g.drawImage(
                bossOnedefeated,
                Global.WINDOW_WIDTH / 2 - width / 2,
                Global.WINDOW_HEIGHT / 12 * 5 - height / 2,
                width,
                height,
                null
            )
            2 -> g.drawImage(
                bossTwodefeated,
                Global.WINDOW_WIDTH / 2 - width / 2,
                Global.WINDOW_HEIGHT / 12 * 5 - height / 2,
                width,
                height,
                null
            )
        }

        if (bossDefeatedSize < 61) {
            bossDefeatedSize++
        } else if (bossDefeatedSize < 91) {
            bossDefeatedTime--
            if (bossDefeatedTime <= 0) {
                bossDefeatedSize = 1.0
                when (bossNum) {
                    1 -> showBossOneDefeated = false
                    2 -> showBossTwoDefeated = false
                }
                bossDefeatedTime = 90
            }
        }
    }


    override fun sceneBegin() {
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    override fun update(timePassed: Long) {
        compareTime.update(timePassed)
        if (compareTime.isStart) {
            if (time > 0) {
                time -= 1.0 / 60.0
            } else {
                time = 0.0
            }
        }
        if (timeDelay.count()) {
            showTime = !showTime
        }
    }

    override fun paint(g: Graphics) {
        //System Message
        g.color = java.awt.Color.BLACK
        g.fillRect(0, Global.WINDOW_HEIGHT / 6 * 5, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT / 6)
        g.color = java.awt.Color.white
        if (GlobalRoundGame.newText!!.size > 0) {
            printText += GlobalRoundGame.newText!![0][newTextIndex]
            g.font = Font("", java.awt.Font.BOLD, 20)
            g.drawString(printText, 50, Global.WINDOW_HEIGHT - Global.WINDOW_HEIGHT / 6 / 2 + 10)
            newTextIndex++
            if (printText.equals(GlobalRoundGame.newText!![0])) {
                GlobalRoundGame.oldText = GlobalRoundGame.newText!![0]
                GlobalRoundGame.newText!!.removeAt(0)
                newTextIndex = 0
                printText = ""
            }
        }
        if (GlobalRoundGame.oldText != null) {
            g.color = java.awt.Color.WHITE
            g.font = Font("", Font.BOLD, 20)
            g.drawString(GlobalRoundGame.oldText, 50, Global.WINDOW_HEIGHT / 6 * 5 + 35)
        }

        //Head
        g.drawImage(headBg, 0, 0, 270, 98, null)
        g.drawImage(headBackground, 28, 9, 70, 55, null)
        g.drawImage(
            head, 30, 17, 30 + Global.UNIT_X * 2, 17 + 19 * 2,
            0, 0, Global.UNIT_X, 19, null
        )


        //money
        g.drawImage(moneyBg, 0, 97, 270, 60, null)
        g.drawImage(money, 100, 95, 60, 60, null)
        g.color = java.awt.Color.BLACK
        g.font = Font("", Font.BOLD, 20)
        g.drawString("" + GlobalRoundGame.roundMoney + "G", 155, 130)

        //herb
        g.drawImage(herb, 45, 105, 87, 147, 1510, 500, 1934, 912, null)
        g.drawImage(herbHint, 83, 130, 20, 20, null)
        if (!GlobalRoundGame.havingHerb) {
            g.drawImage(cross, 52, 112, 30, 30, null)
        }

        //Blood
        g.drawImage(bloodGray, 10, 65, 225, 20, null)
        g.drawImage(
            bloodRed, 10, 65,
            ((GlobalRoundGame.hp.toDouble() / GlobalRoundGame.hpLimit.toDouble()) * 225).toInt() //按比例縮短血量條
            , 20, null
        )
        g.color = java.awt.Color.white
        g.font = Font("", Font.BOLD, 15)
        g.drawString("HP:" + GlobalRoundGame.hp + "/" + GlobalRoundGame.hpLimit, 90, 81)

        //Level
        g.color = java.awt.Color.black
        g.font = Font("", Font.BOLD, 20)
        g.drawString("LEVEL:" + GlobalRoundGame.level, 122, 32)
        g.color = java.awt.Color.orange
        g.font = Font("", Font.BOLD, 20)
        g.drawString("LEVEL:" + GlobalRoundGame.level, 120, 30)

        //Exp
        g.drawImage(bloodGray, 110, 35, 125, 20, null)
        g.drawImage(
            exp, 110, 35,
            ((GlobalRoundGame.exp.toDouble() / GlobalRoundGame.expLimit.toDouble()) * 125).toInt() //按比例增加經驗條
            , 20, null
        )
        g.color = java.awt.Color.white
        g.font = Font("", java.awt.Font.BOLD, 15)
        g.drawString("Exp:" + GlobalRoundGame.exp + "/" + GlobalRoundGame.expLimit, 135, 50)


        //countdown
        var countdown = getNoMoreThanTwoDigits(time)


        //圖片版本倒數計時
        for (i in 0..3) {
            when (countdown[i]) {
                '0'
                -> {
                    if (time > 10.0) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 60, 160, 530, 925, null)
                        g.drawImage(timeNum, 350 + i * 75, 10, 420 + i * 75, 120, 60, 160, 530, 925, null)
                    } else if (showTime) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 60, 160, 530, 925, null)
                        g.drawImage(timeRed, 350 + i * 75, 10, 420 + i * 75, 120, 60, 160, 530, 925, null)
                    }
                }
                '1'
                -> {
                    if (time > 10) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 665, 160, 1135, 925, null)
                        g.drawImage(timeNum, 350 + i * 75, 10, 420 + i * 75, 120, 665, 160, 1135, 925, null)
                    } else if (showTime) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 665, 160, 1135, 925, null)
                        g.drawImage(timeRed, 350 + i * 75, 10, 420 + i * 75, 120, 665, 160, 1135, 925, null)
                    }
                }
                '2'
                -> {
                    if (time > 10) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 1270, 160, 1740, 925, null)
                        g.drawImage(timeNum, 350 + i * 75, 10, 420 + i * 75, 120, 1270, 160, 1740, 925, null)
                    } else if (showTime) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 1270, 160, 1740, 925, null)
                        g.drawImage(timeRed, 350 + i * 75, 10, 420 + i * 75, 120, 1270, 160, 1740, 925, null)
                    }
                }
                '3'
                -> {
                    if (time > 10) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 1875, 160, 2345, 925, null)
                        g.drawImage(timeNum, 350 + i * 75, 10, 420 + i * 75, 120, 1875, 160, 2345, 925, null)
                    } else if (showTime) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 1875, 160, 2345, 925, null)
                        g.drawImage(timeRed, 350 + i * 75, 10, 420 + i * 75, 120, 1875, 160, 2345, 925, null)
                    }
                }
                '4'
                -> {
                    if (time > 10) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 2480, 160, 2950, 925, null)
                        g.drawImage(timeNum, 350 + i * 75, 10, 420 + i * 75, 120, 2480, 160, 2950, 925, null)
                    } else if (showTime) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 2480, 160, 2950, 925, null)
                        g.drawImage(timeRed, 350 + i * 75, 10, 420 + i * 75, 120, 2480, 160, 2950, 925, null)
                    }
                }
                '5'
                -> {
                    if (time > 10) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 60, 1100, 530, 1865, null)
                        g.drawImage(timeNum, 350 + i * 75, 10, 420 + i * 75, 120, 60, 1100, 530, 1865, null)
                    } else if (showTime) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 60, 1100, 530, 1865, null)
                        g.drawImage(timeRed, 350 + i * 75, 10, 420 + i * 75, 120, 60, 1100, 530, 1865, null)
                    }
                }
                '6'
                -> {
                    if (time > 10) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 665, 1100, 1135, 1865, null)
                        g.drawImage(timeNum, 350 + i * 75, 10, 420 + i * 75, 120, 665, 1100, 1135, 1865, null)
                    } else if (showTime) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 665, 1100, 1135, 1865, null)
                        g.drawImage(timeRed, 350 + i * 75, 10, 420 + i * 75, 120, 665, 1100, 1135, 1865, null)
                    }
                }
                '7'
                -> {
                    if (time > 10) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 1270, 1100, 1740, 1865, null)
                        g.drawImage(timeNum, 350 + i * 75, 10, 420 + i * 75, 120, 1270, 1100, 1740, 1865, null)
                    } else if (showTime) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 1270, 1100, 1740, 1865, null)
                        g.drawImage(timeRed, 350 + i * 75, 10, 420 + i * 75, 120, 1270, 1100, 1740, 1865, null)
                    }
                }
                '8'
                -> {
                    if (time > 10) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 1875, 1100, 2345, 1865, null)
                        g.drawImage(timeNum, 350 + i * 75, 10, 420 + i * 75, 120, 1875, 1100, 2345, 1865, null)
                    } else if (showTime) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 1875, 1100, 2345, 1865, null)
                        g.drawImage(timeRed, 350 + i * 75, 10, 420 + i * 75, 120, 1875, 1100, 2345, 1865, null)
                    }
                }
                '9'
                -> {
                    if (time > 10) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 2480, 1100, 2950, 1865, null)
                        g.drawImage(timeNum, 350 + i * 75, 10, 420 + i * 75, 120, 2480, 1100, 2950, 1865, null)
                    } else if (showTime) {
                        g.drawImage(timeShadow, 355 + i * 75, 15, 425 + i * 75, 125, 2480, 1100, 2950, 1865, null)
                        g.drawImage(timeRed, 350 + i * 75, 10, 420 + i * 75, 120, 2480, 1100, 2950, 1865, null)
                    }
                }
            }
        }

        //multiplayer
        if (teamMemberInformation != null) {
            teamMemberInformation?.paintWindow(g)
        }
        //能力值
        informationPopupWindow.paintWindow(g)

        //HintImg
        if (showEnterImg) {
            g.drawImage(enterHint, 360, 500, null)
        }
        if (showOpenImg) {
            g.drawImage(openHint, 360, 500, null)
        }

        //level up
        if (showLevelUp) {
            paintLevelUp(g)
        }
        if (showBossOneDefeated) {
            paintBossDefeated(g, 1)
        }
        if (showBossTwoDefeated) {
            paintBossDefeated(g, 2)
        }
        compareTime.paint(g)
        escPopupWindow.paint(g)


        //數字版倒數計時
//        g.color = java.awt.Color.BLACK
//        g.font = Font("", java.awt.Font.BOLD, 75)
//        g.drawString(
//            "" + getNoMoreThanTwoDigits(time).toDouble().toInt() + ":"
//                    + ((getNoMoreThanTwoDigits(time % 1) ).toDouble()*100).toInt(), Global.WINDOW_WIDTH / 2 - 100, 80
//        )
//        g.color = java.awt.Color.RED
//        g.drawString(
//            "" + getNoMoreThanTwoDigits(time).toDouble().toInt() + ":"
//                    + ((getNoMoreThanTwoDigits(time % 1) ).toDouble()*100).toInt(), Global.WINDOW_WIDTH / 2 - 105, 75
//        )

    }

    fun resetPrintText() {
        newTextIndex = 0
        printText = ""
    }


    fun gameOverTimeGoddessTalk(g: Graphics) {
        if (gameOverOneY > 0) {
            gameOverOneY--
        } else if (largeLabelY > 0) {
            largeLabelY--
        }
        g.drawImage(gameOverOne, 50, 0 - gameOverOneY, null)
        if (gameOverOneY <= 0) {
            g.drawImage(largeLabel, 50, Global.WINDOW_HEIGHT / 2 + largeLabelY, null)
        }
        if (largeLabelY <= 0) {
            g.font = Font("", java.awt.Font.BOLD, 20)
            if (textDelay >= 120) {
                g.drawString(GameOverText.NAME.string, 150, Global.WINDOW_HEIGHT / 2 + 60)
                g.drawString(GameOverText.ONE_1.string, 190, Global.WINDOW_HEIGHT / 2 + 100)
                g.drawString(GameOverText.ONE_2.string, 190, Global.WINDOW_HEIGHT / 2 + 140)
                textDelay--
            } else if (textDelay >= 0) {
                g.drawString(GameOverText.NAME.string, 150, Global.WINDOW_HEIGHT / 2 + 60)
                g.drawString(GameOverText.TWO_1.string, 190, Global.WINDOW_HEIGHT / 2 + 100)
                g.drawString(GameOverText.TWO_2.string, 190, Global.WINDOW_HEIGHT / 2 + 140)
                textDelay--
            }
        }
    }

    fun changeGameOverScene(): Boolean = textDelay <= 0

    fun isStart(): Boolean {   //倒數結束判定
        return compareTime.isStart
    }


    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { }


}