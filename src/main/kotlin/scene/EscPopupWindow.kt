package scene

import core.GameKernel
import core.controllers.ResController
import utils.Path
import java.awt.Graphics

class EscPopupWindow(x: Int, y: Int, width: Int, height: Int) : menu.PopupWindow(x, y, width, height) {
    private val bgImg = ResController.instance.image(Path.Imgs.Objs.ESC)

    override fun paintWindow(g: Graphics) {
        if (isShow) {
            g.drawImage(bgImg, x, y, null)
        }
    }


    override fun update(timePassed: Long) {

    }

    override fun sceneBegin() {
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { }
}
