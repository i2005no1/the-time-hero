package scene

import client.ClientClass
import client.CommandReceiver
import obj.utils.Animator
import utils.Global
import utils.GlobalPlayer
import utils.GlobalRoundGame
import utils.Path
import java.awt.Font
import java.awt.Graphics

class MultiBossBattleScene(monsterJungle: GlobalPlayer.Monsters, val playerNumber: Int,private val bossIndex:Int) : BattleScene(monsterJungle) {

    private var myPlayer: MultiRoundGame.Multiplayer = MultiRoundGame.Multiplayer(10, 420, 128, 128, ClientClass.getInstance().id, GlobalPlayer.playerName, GlobalPlayer.usingStyling.stylingName).apply { inBattleScene() }.apply { setAnimatorState(Animator.State.WALK) }
    private val battlePlayers: MutableList<MultiRoundGame.Multiplayer> = mutableListOf()
    private val monHpLimit=monsterJungle.hpLimit


    //internet
    private val sendClient: ArrayList<String> = arrayListOf()
    private val commandBoss: Command = Command()
    private val playerNumArr: MutableList<Int> = mutableListOf()
    private var removeBossScene: Boolean = false
    private var trueHero: MultiRoundGame.Multiplayer? = null


    class Command {
        val BOSS_ADD: Int = 51
        val BOSS_INFORMATION = 52
        val ATTACK_NUM = 53
        val REMOVE: Int = 54
        val WHO_SEND_BOSS_INFORMATION: Int = 55
    }

    fun whoIsTrueHero() = trueHero

    fun start() {
        damageDateLimit = 12
        battlePlayers += myPlayer
        playerNumArr += playerNumber
        //送自己位置給其他人
        sendClient += myPlayer.collider.centerX.toString()
        sendClient += myPlayer.collider.centerY.toString()
        sendClient += myPlayer.id.toString()
        sendClient += GlobalPlayer.playerName
        sendClient += myPlayer.stylingName
        sendClient += playerNumber.toString()
        ClientClass.getInstance().sent(commandBoss.BOSS_ADD, sendClient)
        sendClient.clear()
    }


    private fun multiAttackToPlayer() { //怪物打玩家
        attackToPlayerDamage = damageToPlayer()
        GlobalRoundGame.attacked = true
        if (GlobalRoundGame.hp - attackToPlayerDamage > 0) {
            GlobalRoundGame.hp = GlobalRoundGame.hp - attackToPlayerDamage
        } else {
            GlobalRoundGame.hp = 0
        }
        if (damageDate.size <= damageDateLimit) {
            damageDate += DamageData(myPlayer.painter.centerX - 60, myPlayer.painter.centerY, attackToPlayerDamage, true)
        }
        GlobalRoundGame.addNewText(GlobalRoundGame.playerName + " took $attackToPlayerDamage damage!") //!!!!!!
    }

    private fun whoSendBossInformation() {
        if (playerNumber == GlobalRoundGame.whoSendBossInformation[bossIndex]) {
            sendClient += monster.painter.centerX.toString()
            sendClient += monster.painter.centerY.toString()
            sendClient += monster.hp.toString()
            ClientClass.getInstance().sent(commandBoss.BOSS_INFORMATION, sendClient)
            sendClient.clear()
        }
    }


    private fun multiRebound(player: MultiRoundGame.Multiplayer) {

        var stronger = (playerStrength().toDouble()/monHpLimit) -
                (monStrength().toDouble()/GlobalRoundGame.hpLimit) //計算優勢(對怪物傷害占怪物最大血量百分比-對玩家傷害占玩家最大血量百分比)
//        println(stronger)
        if (stronger >= 0.1) {
            player.setdx(-22.0) //玩家有優勢
            monster.setdx(37.0)
        } else if (stronger <= -0.1) {
            player.setdx(-37.0) //怪物有優勢
            monster.setdx(22.0)
        } else {
            player.setdx(-22.0) //正常反彈距離
            monster.setdx(22.0)
        }

    }

    fun bossBattleReceive(serialNum: Int, commandCode: Int, strs: ArrayList<String>?) {

        when (commandCode) {
            commandBoss.BOSS_ADD -> {
                for (existPlayer in battlePlayers) {
                    if (existPlayer.id == serialNum) {
                        return
                    }
                }
                //接收新進來的人的信息
                battlePlayers += MultiRoundGame.Multiplayer(strs!![0].toInt(), strs!![1].toInt(), 128, 128, strs!![2].toInt(), strs!![3], strs!![4]).apply { inBattleScene() }.apply { setAnimatorState(Animator.State.WALK) }
                playerNumArr += strs!![5].toInt()

                //告訴新進來的人，我在這裡！
                sendClient += myPlayer.collider.centerX.toString()
                sendClient += myPlayer.collider.centerY.toString()
                sendClient += myPlayer.id.toString()
                sendClient += GlobalPlayer.playerName
                sendClient += myPlayer.stylingName
                sendClient += playerNumber.toString()
                ClientClass.getInstance().sent(commandBoss.BOSS_ADD, sendClient)
                sendClient.clear()
            }
            commandBoss.BOSS_INFORMATION -> {
                if (myPlayer.id == serialNum) {
                    return
                }
                monster.setCenter(strs!![0].toInt(), strs[1].toInt())
                monster.hp = strs[2].toInt()
            }
            commandBoss.REMOVE -> {
                if (myPlayer.id == serialNum) {
                    return
                }
                for (multiplayer in battlePlayers) {
                    if (multiplayer.id == serialNum) {
                        println("remove:" + multiplayer.playerName)
                        battlePlayers.remove(multiplayer)
                        println("battlePlayers size:" + battlePlayers.size)
                        return
                    }
                }
            }
            commandBoss.WHO_SEND_BOSS_INFORMATION -> {
                if (myPlayer.id == serialNum) {
                    return
                }
                playerNumArr.remove(strs!![0].toInt())
                if (strs[0].toInt() == GlobalRoundGame.whoSendBossInformation[bossIndex]) {
                    var foundNextSend: Boolean = false
                    while (!foundNextSend) {
                        GlobalRoundGame.whoSendBossInformation[bossIndex]++
                        for (num in playerNumArr) {
                            if (num == GlobalRoundGame.whoSendBossInformation[bossIndex]) {
                                foundNextSend = true
                            }
                        }
                    }
                }
            }
            commandBoss.ATTACK_NUM -> {
                if (myPlayer.id == serialNum) {
                    return
                }
                for (multiplayer in battlePlayers) {
                    if (multiplayer.id == serialNum) {
                        if (damageDate.size <= damageDateLimit) {
                            damageDate += DamageData(monster.painter.centerX + 60, monster.painter.centerY, strs!![0].toInt(), false)
                            damageDate += DamageData(multiplayer.painter.centerX - 60, multiplayer.painter.centerY, strs!![1].toInt(), true)
                        }
                        if (playerNumber == GlobalRoundGame.whoSendBossInformation[bossIndex]) {
                            monster.hp -= strs!![0].toInt()
                            if(monster.hp <= 0 ){
                                trueHero = multiplayer
                            }
                        }
                        return
                    }
                }
            }
        }
    }

    override fun update(timePassed: Long) {
        monster.update(timePassed)
        for (player in battlePlayers) {
            player.update(timePassed)
            if (player.isCollision(monster)) {
                AudioResourceController.getInstance().play(Path.Sounds.BATTLE)
                multiRebound(player)
                strikeLight += StrikeLight(player.painter.right, player.painter.centerY)
                if (player == myPlayer) {
                    attackToMons()
                    multiAttackToPlayer()
                    if(monster.hp <= 0 ){
                        trueHero = player
                    }
                    sendClient += attackToMonsDamage.toString()
                    sendClient += attackToPlayerDamage.toString()
                    ClientClass.getInstance().sent(commandBoss.ATTACK_NUM, sendClient)
                    sendClient.clear()
                }
            }
        }
        for (damage in damageDate) {
            damage.update(timePassed)
        }
        damageDate.removeIf { damage -> damage.remove }

        for (sl in strikeLight) {
            sl.update(timePassed)
        }
        strikeLight.removeIf { sl -> sl.remove }

        whoSendBossInformation()

    }


    override fun paint(g: Graphics) {
        g.drawImage(bgImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT / 6 * 5, null)

        //flee
        g.drawImage(fleeBgImg, 0, 155, 270, 55, null)
        g.drawImage(fleeKeyImg, 70, 159, 40, 40, null)
        g.color = java.awt.Color.BLACK
        g.font = Font("", Font.BOLD, 20)
        g.drawString("Flee!!", 122, 186)

        for (player in battlePlayers) {
            player.paint(g)
        }
        monster.paint(g)
        for (damage in damageDate) {
            damage.paint(g)
        }

        for (sl in strikeLight) {
            sl.paint(g)
        }

    }

    fun getRemoveBossScene(): Boolean = this.removeBossScene

    fun removeBossScene() {
        removeBossScene = true
    }

    fun theLastOneRemove(): Boolean = playerNumArr.size == 1 && battlePlayers.size == 1
}