package scene

import core.GameKernel
import core.Scene
import core.controllers.ResController
import core.controllers.SceneController
import utils.Global
import utils.GlobalRoundGame
import utils.Path
import java.awt.Graphics
import java.awt.event.KeyEvent


class GamePlotScene2 : Scene() {
    private val bgImg = ResController.instance.image(Path.Imgs.Backgrounds.GAME_PLOT)
    private val textImg = ResController.instance.image(Path.Imgs.Backgrounds.GAME_PLOT_TEXT)
    private val textImgX = 214
    private var textImgY = Global.WINDOW_HEIGHT
//    private var gamePlotText: MutableList<String> = mutableListOf()
//    private var printingText: String = ""
//    enum class TextPosition(val x:Int, val y:Int){
//        ONE(200,300),
//        TWO(300,300),
//        THREE(400,300),
//        FOUR(500,300),
//        FIVE(600,300)
//    }

    override fun sceneBegin() {
//        gamePlotText.add("Goddess Era 100... An epic battle has")
//        gamePlotText.add("begun. With the evil battle, the world will be")
//        gamePlotText.add("destroyed! What awaits the new young hero")
//        gamePlotText.add("on the dangerous road ahead?!")
//        gamePlotText.add("Save the world in 30 Sec!")
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    override fun update(timePassed: Long) {
        if(textImgY > 217) {
            textImgY -= 3
        }
    }


    override fun paint(g: Graphics) {
        g.drawImage(bgImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        g.drawImage(textImg, textImgX, textImgY ,null)

    }

    private fun changeScene(change: Scene) {
        SceneController.instance.change(change)
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { e ->
            run{
                when(e){
                    is GameKernel.Input.Event.KeyPressed ->{
                        if (e.data.keyCode == KeyEvent.VK_Z && textImgY <= 217) {
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)
                            changeScene(scene.InputNameScene3())
                        }else if(e.data.keyCode == KeyEvent.VK_Z && textImgY > 217){
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)
                            textImgY = 217
                        }
                    }
                }
            }
        }
}