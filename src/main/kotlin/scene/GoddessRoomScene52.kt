package scene

import core.GameKernel
import core.Scene
import core.controllers.ResController
import core.controllers.SceneController
import utils.Global
import utils.Path
import java.awt.Graphics
import java.awt.event.KeyEvent

class GoddessRoomScene52 : core.Scene() {
    enum class State{
        STYLING_SHOP,ACHIEVEMENT,MY_WARDROBE
    }
    private val stylingShopChoice = ResController.instance.image(Path.Imgs.Backgrounds.GODDESS_ROOM_STYLING_SHOP)
    private val myWardrobeChoice = ResController.instance.image(Path.Imgs.Backgrounds.GODDESS_ROOM_MY_WARDROBE)
    private val achievementChoice = ResController.instance.image(Path.Imgs.Backgrounds.GODDESS_ROOM_ACHIEVEMENT)
    private var state: State = State.STYLING_SHOP
    private var stateIndex: Int = 0

    override fun sceneBegin() {
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    override fun update(timePassed: Long) {
    }

    override fun paint(g: Graphics) {
        if(state == State.STYLING_SHOP){
            g.drawImage(stylingShopChoice, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        } else if (state == State.MY_WARDROBE) {
            g.drawImage(myWardrobeChoice, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        } else if (state == State.ACHIEVEMENT) {
            g.drawImage(achievementChoice, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        }
    }

    private fun changeScene(change: Scene) {
        SceneController.instance.change(change)
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { e ->
            run{
                when(e){
                    is GameKernel.Input.Event.KeyPressed ->{
                        if(e.data.keyCode == KeyEvent.VK_DOWN && state != State.MY_WARDROBE){
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            state = State.values()[++stateIndex]
                        }else if(e.data.keyCode == KeyEvent.VK_UP && state != State.STYLING_SHOP){
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            state = State.values()[--stateIndex]
                        }else if(e.data.keyCode == KeyEvent.VK_RIGHT && state != State.MY_WARDROBE){
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            state = State.values()[++stateIndex]
                        }else if(e.data.keyCode == KeyEvent.VK_LEFT && state != State.STYLING_SHOP){
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            state = State.values()[--stateIndex]
                        }else if(e.data.keyCode == KeyEvent.VK_Z){
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)
                            if(state == State.STYLING_SHOP){
                                changeScene(StylingShop521())
                            }else if(state == State.ACHIEVEMENT){
                                changeScene(Achievement523())
                            }else if(state == State.MY_WARDROBE){
                                changeScene(MyWardrobe522())
                            }
                        }else if(e.data.keyCode == KeyEvent.VK_X){
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                            changeScene(SelectionScene4())
                        }
                    }
                }
            }
        }
}