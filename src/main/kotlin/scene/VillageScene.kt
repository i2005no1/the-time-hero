package scene

import core.GameKernel
import core.controllers.ResController
import obj.Npc
import obj.Player
import obj.Props
import utils.*
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.event.KeyEvent

class VillageScene(background: String) : core.Scene() {

    class ControllerFrame {
        private var zUsed: Boolean = false
        private var zInformation: String? = null
        private var cUsed: Boolean = false
        private var cInformation: String? = null
        private var isProps: Boolean = false
        private var attackColor: Color = Color.WHITE
        private var attackInformation: String? = null
        private var defenceColor: Color = Color.WHITE
        private var defenceInformation: String? = null
        private var speedColor: Color = Color.WHITE
        private var speedInformation: String? = null
        private val bgImg = ResController.instance.image(Path.Imgs.Objs.CONTROLLERFRAME)

        fun paintComponent(g: Graphics) {
            g.drawImage(bgImg, 0, 365, null)
            g.color = Color.WHITE
            g.font = Font("", Font.BOLD, 20)
            if (zUsed) {
                g.drawString(zInformation, 220, 410)
            }
            if (cUsed) {
                g.drawString(cInformation, 220, 560)
            }

            if (isProps) {
                g.color = attackColor
                g.drawString("Attack: $attackInformation /", 530, 410)
                g.color = defenceColor
                g.drawString(" Defence: $defenceInformation /", 700, 410)
                g.color = speedColor
                g.drawString(" Speed: $speedInformation", 900, 410)
            }
        }

        fun isProps() {
            isProps = true
        }

        fun noProps() {
            isProps = false
        }

        fun cUsed(s: String) {
            cUsed = true
            cInformation = s
        }

        fun zUsed(s: String) {
            zUsed = true
            zInformation = s
        }

        fun cNoUsed() {
            cUsed = false
            cInformation = null
        }

        fun zNoUsed() {
            zUsed = false
            zInformation = null
        }

        fun attackInformation(i: Int) {
            if (i > 0) {
                attackColor = Color.GREEN
                attackInformation = "▲ $i"
            } else if (i == 0) {
                attackColor = Color.WHITE
                attackInformation = "$i"
            } else {
                attackColor = Color.RED
                var a = -i
                attackInformation = "▼ $a"
            }
        }

        fun defenceInformation(i: Int) {
            if (i > 0) {
                defenceColor = Color.GREEN
                defenceInformation = "▲ $i"
            } else if (i == 0) {
                defenceColor = Color.WHITE
                defenceInformation = "$i"
            } else {
                defenceColor = Color.RED
                defenceInformation = "▼ $i"
            }
        }

        fun speedInformation(i: Int) {
            if (i > 0) {
                speedColor = Color.GREEN
                speedInformation = "▲ $i"
            } else if (i == 0) {
                speedColor = Color.WHITE
                speedInformation = "$i"
            } else {
                speedColor = Color.RED
                speedInformation = "▼ $i"
            }
        }

    }

    private val controllerFrame: ControllerFrame = ControllerFrame()
    private val npcTimeGoddess: Npc = Npc(240, 255, 96, 96, GlobalPlayer.Npc.TIME_GODDESS)
    private val npcRecoveryFood: Npc = Npc(400, 260, 96, 96, GlobalPlayer.Npc.RECOVERY_FOOD)
    private val npcWeaponDealer: Npc = Npc(560, 260, 96, 96, GlobalPlayer.Npc.WEAPONS_DEALER)
    private val player: Player = Player(100, 290, 96, 96).apply { isVillage() }
    private var propsArm: Props.PropsArm? =
        Props.PropsArm(670, 260, 96, 96, GlobalPlayer.Arms.values()[Global.random(0, unlockShopProps())])
    private var propsArmor: Props.PropsArmor? =
        Props.PropsArmor(770, 260, 96, 96, GlobalPlayer.Armors.values()[Global.random(0, unlockShopProps())])
    private var propsShoes: Props.PropsShoes? =
        Props.PropsShoes(870, 260, 96, 96, GlobalPlayer.Shoes.values()[Global.random(0, 12)])
    private var propsHerb: Props.PropsHerb? = null
    private var bgImg = ResController.instance.image(Path.Imgs.Backgrounds.Village.addPng(background))
    private val foodPrice = GlobalRoundGame.recoveryFoodPrice

    private var isResetTime: Boolean = false

    override fun sceneBegin() {
        if (Global.random(0, 100) > 40) {
            propsHerb = Props.PropsHerb(970, 265, 64, 64, GlobalPlayer.Herbs.values()[Global.random(0, 0)])
        }
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    override fun update(timePassed: Long) {
        player.update(timePassed)
        if (player.touchLeft || player.touchScreenRight) {
            controllerFrame.noProps()
            controllerFrame.cUsed("Exit Village")
            controllerFrame.zNoUsed()
            propsAllPaintReset()
        } else if (player.isCollision(npcTimeGoddess)) {
            controllerFrame.zUsed("Pray           " + GlobalRoundGame.prayPrice + "G")
            controllerFrame.cNoUsed()
            controllerFrame.noProps()
            propsAllPaintReset()
        } else if (player.isCollision((npcRecoveryFood))) {
            controllerFrame.zUsed("Recovery Food           " + foodPrice + "G")
            controllerFrame.cUsed("Talk")
            controllerFrame.noProps()
            propsAllPaintReset()
        } else if (propsArm != null && player.isCollision((propsArm!!))) {
            controllerFrame.zUsed(propsArm!!.name + "           " + propsArm!!.price + "G")
            controllerFrame.cUsed("Explanation")
            controllerFrame.isProps()
            controllerFrame.attackInformation(propsArm!!.addAttackValue)
            controllerFrame.defenceInformation(propsArm!!.addDefenceValue)
            controllerFrame.speedInformation((propsArm!!.addSpeedValue))
            onePropsPaintRise(propsArm, propsArmor, propsShoes, propsHerb)
        } else if (propsArmor != null && player.isCollision((propsArmor!!))) {
            controllerFrame.zUsed(propsArmor!!.name + "           " + propsArmor!!.price + "G")
            controllerFrame.cUsed("Explanation")
            controllerFrame.isProps()
            controllerFrame.attackInformation(propsArmor!!.addAttackValue)
            controllerFrame.defenceInformation(propsArmor!!.addDefenceValue)
            controllerFrame.speedInformation((propsArmor!!.addSpeedValue))
            onePropsPaintRise(propsArmor, propsArm, propsShoes, propsHerb)
        } else if (propsShoes != null && player.isCollision((propsShoes!!))) {
            controllerFrame.zUsed(propsShoes!!.name + "           " + propsShoes!!.price + "G")
            controllerFrame.cUsed("Explanation")
            controllerFrame.isProps()
            controllerFrame.attackInformation(propsShoes!!.addAttackValue)
            controllerFrame.defenceInformation(propsShoes!!.addDefenceValue)
            controllerFrame.speedInformation((propsShoes!!.addSpeedValue))
            onePropsPaintRise(propsShoes, propsArmor, propsArm, propsHerb)
        } else if (propsHerb != null && player.isCollision((propsHerb!!))) {
            controllerFrame.zUsed(propsHerb!!.name + "           " + propsHerb!!.price + "G")
            controllerFrame.cUsed("Explanation")
            controllerFrame.noProps()
            onePropsPaintRise(propsHerb, propsArmor, propsArm, propsShoes)
        } else {
            controllerFrame.cNoUsed()
            controllerFrame.zNoUsed()
            controllerFrame.noProps()
            propsAllPaintReset()
        }

    }

    fun endVillage(): Boolean {
        return player.touchLeft || player.touchScreenRight
    }

    private fun propsAllPaintReset() {
        propsArm?.paintReset()
        propsArmor?.paintReset()
        propsHerb?.paintReset()
        propsShoes?.paintReset()
    }

    private fun onePropsPaintRise(rise: Props?, reOne: Props?, reTwo: Props?, reThree: Props?) {
        rise?.paintRise()
        reOne?.paintReset()
        reTwo?.paintReset()
        reThree?.paintReset()
    }

    private fun unlockShopProps() :Int{
        if (GlobalRoundGame.level < 15) {
            return  4
        } else if (GlobalRoundGame.level in 15..29) {
            return  7
        } else {
            return  10
        }

    }

    override fun paint(g: Graphics) {
        g.drawImage(bgImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        controllerFrame.paintComponent(g)
        npcTimeGoddess.paint(g)
        npcWeaponDealer.paint(g)
        npcRecoveryFood.paint(g)
        propsArm?.paint(g)
        propsArmor?.paint(g)
        propsShoes?.paint(g)
        propsHerb?.paint(g)
        player.paint(g)
    }

    fun getIsResetTime(): Boolean {
        return isResetTime
    }

    fun noResetTime() {
        isResetTime = false
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { e ->
            run {
                when (e) {
                    is GameKernel.Input.Event.KeyKeepPressed -> {
                        if (e.data.keyCode == KeyEvent.VK_LEFT || e.data.keyCode == KeyEvent.VK_RIGHT) {
                            player.input?.invoke(e)
                        } else {
                        }
                    }
                    is GameKernel.Input.Event.KeyPressed -> {
                        if (e.data.keyCode == KeyEvent.VK_Z) {
                            when {
                                player.isCollision(npcTimeGoddess) -> {
                                    if (GlobalRoundGame.roundMoney - GlobalRoundGame.prayPrice >= 0) {
                                        AudioResourceController.getInstance().play(Path.Sounds.PRAY_TIME)
                                        GlobalRoundGame.roundMoney =
                                            GlobalRoundGame.roundMoney - GlobalRoundGame.prayPrice
                                        isResetTime = true
                                        GlobalPlayer.playerMoney += GlobalRoundGame.prayPrice
                                        GlobalRoundGame.moneySpent += GlobalRoundGame.prayPrice
                                        GlobalRoundGame.prayPrice = GlobalRoundGame.prayPrice + 20
                                        ResController.instance.clear()
                                    } else {
                                        GlobalRoundGame.addNewText("Time Goddess: You don't have enough money.")
                                    }
                                }
                                player.isCollision(npcRecoveryFood) -> {
                                    if (GlobalRoundGame.roundMoney - foodPrice >= 0) {
                                        GlobalRoundGame.ateRecoveryFood = true
                                        AudioResourceController.getInstance().play(Path.Sounds.RECOVERY_FOOD)
                                        GlobalRoundGame.addNewText("Ate Recovery Food! Healthy Point Fully Recovered!")
                                        GlobalRoundGame.roundMoney = GlobalRoundGame.roundMoney - foodPrice
                                        GlobalRoundGame.hp = GlobalRoundGame.hpLimit
                                        GlobalRoundGame.moneySpent += foodPrice
                                    } else {
                                        GlobalRoundGame.addNewText("Waitress: You don't have enough money.")
                                    }
                                }
                                (propsArm != null && player.isCollision(propsArm!!)) -> {
                                    if (!propsArm!!.buyAction()) {
                                        GlobalRoundGame.addNewText("Weapons Dealer: You don't have enough money.")
                                    } else {
                                        AudioResourceController.getInstance().play(Path.Sounds.BUY_PROP)
                                        GlobalRoundGame.usingArms = propsArm!!.arm
                                        GlobalRoundGame.updatePlayerInformation()
                                        propsArm = null
                                    }
                                }
                                (propsArmor != null && player.isCollision(propsArmor!!)) -> {
                                    if (!propsArmor!!.buyAction()) {
                                        GlobalRoundGame.addNewText("Weapons Dealer: You don't have enough money.")
                                    } else {
                                        AudioResourceController.getInstance().play(Path.Sounds.BUY_PROP)
                                        GlobalRoundGame.usingArmors = propsArmor!!.armor
                                        GlobalRoundGame.updatePlayerInformation()
                                        propsArmor = null
                                    }
                                }
                                (propsShoes != null && player.isCollision(propsShoes!!)) -> {
                                    if (!propsShoes!!.buyAction()) {
                                        GlobalRoundGame.addNewText("Weapons Dealer: You don't have enough money.")
                                    } else {
                                        AudioResourceController.getInstance().play(Path.Sounds.BUY_PROP)
                                        GlobalRoundGame.usingShoes = propsShoes!!.shoes
                                        GlobalRoundGame.updatePlayerInformation()
                                        propsShoes = null
                                    }
                                }
                                (propsHerb != null && player.isCollision(propsHerb!!)) -> {
                                    if (!propsHerb!!.buyAction()) {
                                        GlobalRoundGame.addNewText("Weapons Dealer: You don't have enough money.")
                                    } else if (GlobalRoundGame.havingHerb) {
                                        GlobalRoundGame.addNewText("You already have a herb. ")
                                    } else {
                                        AudioResourceController.getInstance().play(Path.Sounds.BUY_PROP)
                                        GlobalRoundGame.setHerb()
                                        propsHerb = null
                                    }
                                }
                                else -> null
                            }
                        } else if (e.data.keyCode == KeyEvent.VK_C) {
                            when {
                                (propsArm != null && player.isCollision(propsArm!!)) -> {
                                    AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                                    GlobalRoundGame.addNewText(propsArm!!.explanation)
                                }
                                (propsArmor != null && player.isCollision(propsArmor!!)) -> {
                                    AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                                    GlobalRoundGame.addNewText(propsArmor!!.explanation)
                                }
                                (propsShoes != null && player.isCollision(propsShoes!!)) -> {
                                    AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                                    GlobalRoundGame.addNewText(propsShoes!!.explanation)
                                }
                                (propsHerb != null && player.isCollision(propsHerb!!)) -> {
                                    AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                                    GlobalRoundGame.addNewText(propsHerb!!.explanation)
                                }
                                else -> null
                            }
                        } else if (e.data.keyCode == KeyEvent.VK_X) {
                        } else {
                        }
                    }
                    else -> player.input?.invoke(e)
                }

            }
        }

}