package scene

import core.GameKernel
import core.Scene
import core.controllers.ResController
import core.controllers.SceneController
import menu.EditText
import obj.utils.Animator
import utils.Global
import utils.GlobalPlayer
import utils.Path
import java.awt.Graphics
import java.awt.event.KeyEvent

class InputNameScene3: Scene() {
    private val bgImg = ResController.instance.image(Path.Imgs.Backgrounds.INPUT_NAME)
    private val editText: EditText = EditText(425,177,200,80,"   Your Name   ")
    private var player: obj.Player = obj.Player(538,495,224,224)

    override fun sceneBegin() {
        player.setAnimatorState(Animator.State.SLOW)
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    override fun update(timePassed: Long) {
        player.update(timePassed)
    }

    override fun paint(g: Graphics) {
        g.drawImage(bgImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        editText.paint(g)
        player.paint(g)
    }

    private fun changeScene(change: Scene) {
        SceneController.instance.change(change)
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = {e ->
            run{
                when(e){
                    is GameKernel.Input.Event.KeyPressed -> {
                        if(e.data.keyCode == KeyEvent.VK_ENTER){
                            if(!editText.getFocus()){
                                editText.isFocus()
                                AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                            }else{
                                editText.unFocus()
                                AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                                GlobalPlayer.playerName = editText.getEditText()
                                changeScene(SelectionScene4())
                            }
                        }
                        if(editText.getFocus() && e.data.keyCode != KeyEvent.VK_ENTER){
                            editText.input!!.invoke(e)
                        }
                    }
                }

            }
        }
}