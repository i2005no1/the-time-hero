package scene

import client.ClientClass
import client.CommandReceiver
import core.GameKernel
import core.Scene
import core.controllers.ResController
import core.controllers.SceneController
import obj.utils.Animator
import obj.utils.GameObject
import utils.Global
import utils.Global.SCREEN_X
import utils.Global.SCREEN_Y
import utils.GlobalPlayer
import utils.Path
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.event.KeyEvent
import java.io.IOException


class MultiWaitingHome(private val ip: String, private val port: Int, private val name: String,private val usingStyling :GlobalPlayer.Styling) : Scene() {
    private val normalBgImg = ResController.instance.image(Path.Imgs.Backgrounds.MULTI_WAITING_ROOM_NORMAL)
    private val masterBgImg = ResController.instance.image(Path.Imgs.Backgrounds.MULTI_WAITING_ROOM_MASTER)
    private val loading=ResController.instance.image(Path.Imgs.Objs.LOADING)
    private var isLoading=false
    private var enterDelay=0

    class WaitingHomePlayer(x: Int, y: Int, width: Int, height: Int, val playerName: String, val id: Int,val usingStyling :String) : GameObject(x, y, width, height) {
        private var animator: Animator = Animator(0, Animator.State.STOP, usingStyling, true)
        var ok: Boolean = false

        override fun paintComponent(g: Graphics) {
            g.color = Color.WHITE
            g.font = Font("", Font.BOLD,20)
            var stringWidth= g.getFontMetrics().stringWidth(playerName)
            g.drawString(playerName,painter.centerX-stringWidth/2,painter.top-100)
            animator.paint(Global.Direction.DOWN, painter.left, painter.top, painter.right, painter.bottom, g)
            if(ok){
                g.color = Color.RED
                g.drawString("READY",painter.left+50,painter.top+260)
            }
        }

        override fun update(timePassed: Long) {
        }

    }

    class Command {
        val ADD: Int = 1
        val START_GAME = 2
        val OK = 3
    }

    private val waitingHomePlayer: MutableList<WaitingHomePlayer> = mutableListOf()
    private var myselfPlayer: WaitingHomePlayer? = null
    private val sendClient: ArrayList<String> = arrayListOf()
    private val command: Command = Command()
    //頭頂旗子

    override fun sceneBegin() {
        try {
            ClientClass.getInstance().connect(ip, port) // ("SERVER端IP", "SERVER端PORT")
            myselfPlayer = WaitingHomePlayer((ClientClass.getInstance().id % 100) * 265 + 140, 370, 160, 160, name, ClientClass.getInstance().id,usingStyling.stylingName)
            waitingHomePlayer += myselfPlayer!!
            sendClient += myselfPlayer!!.collider.centerX.toString()
            sendClient += myselfPlayer!!.collider.centerY.toString()
            sendClient += myselfPlayer!!.playerName
            sendClient += myselfPlayer!!.id.toString()
            sendClient += myselfPlayer!!.usingStyling
            ClientClass.getInstance().sent(1, sendClient);
        } catch (ex: IOException) {
        }
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    override fun update(timePassed: Long) {
        ClientClass.getInstance().consume(object : CommandReceiver {
            override fun receive(serialNum: Int, commandCode: Int, strs: ArrayList<String>?) {
                when (commandCode) {
                    command.ADD -> {
                        for(existPlayer in waitingHomePlayer){
                            if(existPlayer.id == serialNum){
                                return
                            }
                        }
                        //接收新進來的人的信息
                        waitingHomePlayer += WaitingHomePlayer(strs!![0].toInt(), strs!![1].toInt(), 160, 160, strs!![2], strs!![3].toInt(),strs!![4])
                        //告訴新進來的人，我在這裡！
                        sendClient += myselfPlayer!!.collider.centerX.toString()
                        sendClient += myselfPlayer!!.collider.centerY.toString()
                        sendClient += myselfPlayer!!.playerName
                        sendClient += myselfPlayer!!.id.toString()
                        sendClient += myselfPlayer!!.usingStyling
                        ClientClass.getInstance().sent(1, sendClient);
                    }
                    command.START_GAME -> {
                        changeScene(MultiRoundGame())
                    }
                    command.OK -> {
                        for(player in waitingHomePlayer){
                            if(player.id == serialNum && myselfPlayer!!.id != serialNum){
                                player.ok = !player.ok
                            }
                        }
                    }
                }
            }
        })
    }

    private fun changeScene(change: Scene) {
        SceneController.instance.change(change)
    }

    override fun paint(g: Graphics) {
        if(myselfPlayer!!.id == 100){
            g.drawImage(masterBgImg,0,0,null)
        }else{
            g.drawImage(normalBgImg,0,0,null)
        }
        for(player in waitingHomePlayer){
            player.paintComponent(g)
        }
        if(isLoading){
            g.drawImage(loading,SCREEN_X-210,SCREEN_Y-40,200,50,null)
            enter()
        }

    }

    private fun enter(){
        if(enterDelay<2){
            enterDelay++
        }else {
            enterDelay = 0
            isLoading = false
            for (player in waitingHomePlayer) {
                if (!player.ok) {
                    break
                }
                if (player == waitingHomePlayer[waitingHomePlayer.size - 1]) {
                    ClientClass.getInstance().sent(2, sendClient)
                }
            }
        }
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { e ->
            run{
                when(e){
                    is GameKernel.Input.Event.KeyPressed -> {
                        if(e.data.keyCode == KeyEvent.VK_Z){
                            myselfPlayer!!.ok = !myselfPlayer!!.ok
                            ClientClass.getInstance().sent(3,sendClient)
                        }
//                        if(e.data.keyCode == KeyEvent.VK_X){ //返回?
//                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
//                            changeScene(MultiConnectInput())
//                        }
                        if(e.data.keyCode == KeyEvent.VK_ENTER && myselfPlayer!!.id == 100){
                            isLoading=true

                        }
                    }
                }
            }
        }
}
