package scene

import camera.Camera
import camera.MapInformation
import core.GameKernel
import core.Scene
import core.controllers.ResController
import core.controllers.SceneController
import core.utils.Delay
import maploader.MapInfo
import maploader.MapLoader
import obj.*
import obj.utils.GameObject
import utils.*
import java.awt.Graphics
import java.awt.event.KeyEvent

class StageFourScene : Scene() {
    //Scene
    private var battleScene: BattleScene? = null
    private val information: Information = Information()
    private var isBattle: Boolean = false
    private var villageScene: VillageScene? = null
    private var isVillage: Boolean = false
    private var cantMoveHint: Boolean = false
    private var isBossBattle: Boolean = false
    private var isGameOver = false
    private var informationPopUpShow: Boolean = false
    private var escPopupShow: Boolean = false
    private var gameOverSound: Boolean = false

    //Achievement
    private var downgradeCount: Int = 0

    //GameObject
    private val startPositionX: Int = 4300
    private val startPositionY: Int = 3850
    private val player: Player = Player(startPositionX, startPositionY, 64, 64)
    private val monsters: MutableList<Monster> = mutableListOf()
    private val villages: MutableList<Village> = mutableListOf()
    private val invincibleDelay: Delay = Delay(110).apply { loop() }
    private val bossCastle = BossCastle(4100, 2655, 192, 192)


    //Image
    private val bgImg = ResController.instance.image(Path.Imgs.Backgrounds.MAP) //要改

    //camera
    private var camera: Camera = Camera.Builder(Global.SCREEN_X, Global.WINDOW_HEIGHT - Global.WINDOW_HEIGHT / 6)
        .setChaseObj(player, 10.0, 10.0)
        .setCameraWindowLocation(0, 0)
        .gen()

    //map
    private var mapCollision: ArrayList<MapInfo> = MapLoader("/maps/genMap.bmp", "/maps/genMap.txt").combineInfo()
    private var mapCollisionObj: ArrayList<GameObject> = ArrayList()
    private var mapLoader: MapLoader = MapLoader("/maps/genMap.bmp", "/maps/genMap.txt")

    //event
    private var eventOne = EventPoint(2260, 2040, 100, 64)
    private var eventTwo = EventPoint(864, 4608, 64, 128)


    override fun sceneBegin() {
        AudioResourceController.getInstance().stop(Path.Sounds.SELECTION)
        AudioResourceController.getInstance().loop(Path.Sounds.STAGE_4, 20)
        GlobalRoundGame.startTime = System.currentTimeMillis()
        setMapLoader()
        createMons()
        createVillages()
        MapInformation.setMapInfo(0, 0, 4800, 4800)

    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    private fun timeUpDowngrade() {
        downgradeCount++
        information.resetTime()
        player.setCenter(startPositionX, startPositionY)//玩家死亡重生點
        GlobalRoundGame.level -= 4

        if (GlobalRoundGame.level <= 0) {
            GlobalRoundGame.level = 1
        }

        if (isBattle) {
            isBossBattle = false
            endBattle()
        }
        if (isVillage) {
            endVillage()
        }
        GlobalRoundGame.updatePlayerInformation()
        if(GlobalRoundGame.hp>GlobalRoundGame.hpLimit){
            GlobalRoundGame.hp=GlobalRoundGame.hpLimit
            GlobalRoundGame.updatePlayerInformation()
        }
    }

    override fun update(timePassed: Long) {
        //倒數階段
        if (!information.isStart()) {
            player.setCenter(startPositionX, startPositionY)
        }
        //鐘頭移動
        camera.update(timePassed)

        //BattleScene update
        if (isBattle) {
            if (!isGameOver) {
                battleScene?.update(timePassed)
            }
            if (battleScene!!.monsterHp() == 0) {
                if (!isBossBattle) {  //boss戰鬥設定
                    GlobalRoundGame.newText = mutableListOf()
                    information.resetPrintText()
                    GlobalRoundGame.exp = GlobalRoundGame.exp + battleScene!!.monsterExp()
                    var addMoney = battleScene!!.monsterMoney()
                    GlobalRoundGame.roundMoney = GlobalRoundGame.roundMoney + addMoney
                    GlobalRoundGame.addNewText("Defeated " + battleScene!!.monsterName() + "! Gained " + battleScene!!.monsterExp() + " EXP and " + addMoney + " G!")
                    GlobalRoundGame.monsterKilled++
                    //Achievement
                    GlobalAchievement.tenThousand(GlobalRoundGame.roundMoney)
                    endBattle()
                } else {
                    GlobalRoundGame.newText = mutableListOf()
                    information.resetPrintText()
                    GlobalRoundGame.endTime = System.currentTimeMillis()
                    isGameOver = true
                    if (!gameOverSound) {
                        AudioResourceController.getInstance().stop(Path.Sounds.STAGE_4)
                        AudioResourceController.getInstance().loop(Path.Sounds.GAME_OVER, 5)
                        gameOverSound = true
                    }
                }
            }
            if (GlobalRoundGame.hp == 0) {
                GlobalRoundGame.newText = mutableListOf()
                information.resetPrintText()
                GlobalRoundGame.addNewText(GlobalPlayer.playerName + " was defeated...")
                endBattle()
                isBossBattle = false
                GlobalRoundGame.hp = 10
                player.setCenter(startPositionX, startPositionY) //玩家死亡重生點
            }
            if (information.time <= 0) {
                GlobalRoundGame.newText = mutableListOf()
                information.resetPrintText()
                timeUpDowngrade()
                endBattle()
            }


            // VillageScene update
        } else if (isVillage) {
            villageScene?.update(timePassed)
            if (villageScene!!.getIsResetTime()) {
                information.resetTime()
                villageScene!!.noResetTime()
            }
            if (information.time <= 0) {
                timeUpDowngrade()
                endVillage()
            }
        } else { //JungleScene update
            player.update(timePassed)
            if (!player.getInvincible()) {
                player.isShow()
            } else { //Player invincible
                if (invincibleDelay.count()) {
                    player.noInvincible()
                }
            }

            for (monster in monsters) {
                if (!player.getInvincible() && player.isCollision(monster) && monster.state == Monster.State.ALIVE) {
                    startBattle(monster.monsterEnum)
                    monster.state = Monster.State.DEAD
                    break
                } else {
                    monster.update(timePassed)
                }
            }
            if (information.time <= 0) {
                timeUpDowngrade()
            }
        }
        //informationScene update
        if (!isGameOver) {
            information.update(timePassed)
        }

        //level up
        if (GlobalRoundGame.exp >= GlobalRoundGame.expLimit) {
            GlobalRoundGame.exp = GlobalRoundGame.exp - GlobalRoundGame.expLimit
            GlobalRoundGame.level++
            AudioResourceController.getInstance().play(Path.Sounds.LEVEL_UP)
            GlobalRoundGame.updatePlayerInformation()
            information.showLevelUp = true
            //level up show
        }

        //地圖碰撞
        canNotMove()

        //showEnterImg
        if (!isVillage && !isBattle) {
            for (village in villages) {
                if (player.isCollision(village)) {
                    information.showEnterImg = true
                    break
                } else {
                    information.showEnterImg = false
                }
            }
            if (player.isCollision(bossCastle)) {
                information.showEnterImg = true
            }
        } else {
            information.showEnterImg = false
        }

        //GameOver事件更新
        if (isGameOver && information.changeGameOverScene()) {
            //Achievement
            GlobalAchievement.downgradeLessThanThree(downgradeCount)
            GlobalAchievement.moreThanThirtySeconds(information.time)

            SceneController.instance.change(GameOverScene(4))
        }

    }


    private fun paintJungle(g: Graphics) {
        g.drawImage(
            bgImg, 0, 0,
            4800, 4800, null
        )
        //隱形物件
        mapCollisionObj.forEach { a -> a.paint(g) }
        for (village in villages) {
            if (village.isCollision(camera)) {
                village.paint(g)
            }
        }
        eventOne.paint(g)
        eventTwo.paint(g)

        //非隱形物件
        for (monster in monsters) {
            if (monster.isCollision(camera)) {
                monster.paint(g)
            }
        }

        if (camera.isCollision(bossCastle)) {
            bossCastle.paint(g)
        }

        player.paint(g)

    }

    override fun paint(g: Graphics) {
        camera.start(g)
        if (isBattle) {
            camera.end(g)
            battleScene?.paint(g)
        } else if (isVillage) {
            camera.end(g)
            villageScene?.paint(g)
        } else {
            paintJungle(g)
        }
        camera.end(g)
        information.paint(g)
        for (gameObject in mapCollisionObj) {
            gameObject.painter.left
        }
        if (isGameOver) {
            information.gameOverTimeGoddessTalk(g)
        }

    }

    private fun startBattle(monsterEnum: GlobalPlayer.Monsters) {
        battleScene = BattleScene(monsterEnum)
        isBattle = true
    }

    private fun endBattle() {
        isBattle = false
        isBossBattle = false
        battleScene = null
        player.invincible()
    }

    private fun startVillage(village: Village) {
        villageScene = VillageScene(village.background)
        villageScene!!.sceneBegin()
        isVillage = true
    }

    private fun endVillage() {
        isVillage = false
        villageScene = null
        player.invincible()
    }

    private fun canNotMove() {

        mapCollisionObj.forEach { a ->
            if (player.isCollision(a)) {
                if (player.getDir() == Global.Direction.LEFT && player.collider.left < a.collider.right) {
                    player.setCenter(a.painter.centerX + Global.UNIT_X * 2 + 1, player.painter.centerY)
                } else if (player.getDir() == Global.Direction.UP && player.collider.top < a.collider.bottom) {
                    player.setCenter(player.painter.centerX, a.painter.centerY + Global.UNIT_Y * 2 + 1)
                } else if (player.getDir() == Global.Direction.RIGHT && player.collider.right > a.collider.left) {
                    player.setCenter(a.painter.centerX - Global.UNIT_X * 2 - 1, player.painter.centerY)
                } else if (player.getDir() == Global.Direction.DOWN && player.collider.bottom > a.collider.top) {
                    player.setCenter(player.painter.centerX, a.painter.centerY - Global.UNIT_Y * 2 - 1)
                }


            }

        }

    }

    private fun createMons() {
        //第一區
        monsters += Monster(1000, 4000, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER4)
        monsters += Monster(1400, 3540, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER4)
        monsters += Monster(1500, 4300, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER4)
        monsters += Monster(1760, 4500, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER4)
        monsters += Monster(1800, 3200, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER4)

        monsters += Monster(1800, 3600, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER3)
        monsters += Monster(2200, 4400, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER3)
        monsters += Monster(2340, 3840, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER3)
        monsters += Monster(2800, 4200, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER3)
        monsters += Monster(3200, 4400, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER3)

        monsters += Monster(2400, 3400, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER2)
        monsters += Monster(2710, 2740, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER2)
        monsters += Monster(2800, 3000, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER2)
        monsters += Monster(2870, 3340, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER2)
        monsters += Monster(3240, 3120, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER2)

        monsters += Monster(3700, 3900, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER1)
        monsters += Monster(4060, 4250, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER1)
        monsters += Monster(4200, 4000, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER1)
        monsters += Monster(4360, 3480, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER1)
        monsters += Monster(4400, 4200, 64, 64, GlobalPlayer.Monsters.AREA1_MONSTER1)
        //第二區
        monsters += Monster(650, 2850, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER1)
        monsters += Monster(1085, 2765, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER1)
        monsters += Monster(1160, 2460, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER1)
        monsters += Monster(1500, 2450, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER1)
        monsters += Monster(1575, 2160, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER1)

        monsters += Monster(740, 2240, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER2)
        monsters += Monster(385, 2265, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER2)
        monsters += Monster(550, 1970, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER2)
        monsters += Monster(405, 1770, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER2)
        monsters += Monster(920, 1805, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER2)

        monsters += Monster(980, 1405, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER3)
        monsters += Monster(340, 1255, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER3)
        monsters += Monster(705, 1210, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER3)
        monsters += Monster(1270, 1270, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER3)
        monsters += Monster(1390, 1475, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER3)

        monsters += Monster(1065, 800, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER4)
        monsters += Monster(970, 545, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER4)
        monsters += Monster(1315, 610, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER4)
        monsters += Monster(1645, 580, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER4)
        monsters += Monster(1555, 265, 64, 64, GlobalPlayer.Monsters.AREA2_MONSTER4)
        //第三區
        monsters += Monster(2155, 310, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER1)
        monsters += Monster(2335, 560, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER1)
        monsters += Monster(2400, 870, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER1)
        monsters += Monster(2600, 405, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER1)
        monsters += Monster(2630, 680, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER1)

        monsters += Monster(2980, 865, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER2)
        monsters += Monster(3325, 1075, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER2)
        monsters += Monster(3835, 1260, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER2)
        monsters += Monster(4255, 1260, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER2)
        monsters += Monster(4250, 700, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER2)

        monsters += Monster(2800, 1250, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER3)
        monsters += Monster(2615, 1500, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER3)
        monsters += Monster(3010, 1550, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER3)
        monsters += Monster(2970, 1865, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER3)
        monsters += Monster(3550, 1635, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER3)

        monsters += Monster(3630, 2050, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER4)
        monsters += Monster(3390, 2240, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER4)
        monsters += Monster(3700, 2465, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER4)
        monsters += Monster(4250, 2150, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER4)
        monsters += Monster(4410, 2380, 64, 64, GlobalPlayer.Monsters.AREA3_MONSTER4)
    }

    private fun createVillages() {
        villages += Village(4320, 3790, 300, 128, "village1")
        villages += Village(2690, 3790, 96, 96, "village2")
        villages += Village(1780, 3170, 200, 96, "village3")
        villages += Village(480, 4465, 100, 100, "village4")
        villages += Village(865, 2450, 96, 96, "village5")
        villages += Village(335, 1000, 300, 128, "village6")
        villages += Village(2255, 720, 100, 100, "village7")
        villages += Village(4370, 1060, 100, 100, "village8")
        villages += Village(3890, 2300, 200, 100, "village9")
    }

    private fun setMapLoader() {
        mapCollisionObj = mapLoader.creatObjectArray("canNotMove", 64,
            mapCollision, MapLoader.CompareClass { gameObject, name, mapInfo, MapObjectSize ->
                var tmp: GameObject? = null
                if (gameObject == name) {
                    tmp = MapCollision(
                        mapInfo.x * MapObjectSize + 32, mapInfo.y * MapObjectSize + 32,
                        mapInfo.sizeX * MapObjectSize, mapInfo.sizeY * MapObjectSize
                    )

                }
                return@CompareClass tmp;
            })
    }

    private fun eventOne() {
        //Achievement
        GlobalAchievement.allSpecialEvent(6)

        AudioResourceController.getInstance().play(Path.Sounds.PRAY_TIME)
        eventOne.isTriggered = true
        GlobalRoundGame.addNewText("Time Goddess:「Don't you have enough time?」「Let me help you !」")
        information.time = 99.99
    }

    private fun eventTwo() {
        //Achievement
        GlobalAchievement.allSpecialEvent(7)
        GlobalAchievement.threeTower(2)

        AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
        eventTwo.isTriggered = true
        GlobalRoundGame.addNewText("The SandTower looks lonely!")
    }


    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { e ->
            run {
                if (isBattle) {
                    when (e) {
                        is GameKernel.Input.Event.KeyPressed -> {
                            if (!battleScene!!.isFleeUsed && e.data.keyCode == KeyEvent.VK_C) {
                                battleScene!!.isFleeUsed = true
                                if (Global.random(0, 100) < GlobalRoundGame.FLEE_PROBABILITY) {
                                    endBattle()
                                    GlobalRoundGame.newText = mutableListOf()
                                    information.resetPrintText()
                                    GlobalRoundGame.addNewText(GlobalPlayer.playerName + " ran away ...")
                                } else {
                                    GlobalRoundGame.newText = mutableListOf()
                                    information.resetPrintText()
                                    GlobalRoundGame.addNewText(GlobalPlayer.playerName + " failed to escape ...")
                                }
                            }
                        }
                    }
                }
                if (isBattle || !isVillage) {
                    when (e) {
                        is GameKernel.Input.Event.KeyPressed -> {
                            if (GlobalRoundGame.havingHerb && e.data.keyCode == KeyEvent.VK_X) {
                                GlobalRoundGame.hp = GlobalRoundGame.hpLimit
                                AudioResourceController.getInstance().play(Path.Sounds.HERB)
                                GlobalRoundGame.setHerb()
                                GlobalRoundGame.addNewText("" + player + " used herb!!")
                                GlobalRoundGame.updatePlayerInformation()
                            }
                        }
                    }
                }
                if (!isBattle && !isVillage) {
                    player.input?.invoke(e)
                    when (e) {
                        is GameKernel.Input.Event.KeyKeepPressed -> {
                            for (village in villages) {
                                if (!player.getInvincible() && player.isCollision(village) && e.data.keyCode == KeyEvent.VK_C) {
                                    this.startVillage(village)
                                }
                            }
                            if (e.data.keyCode == KeyEvent.VK_Z) {
                                if (!informationPopUpShow) {
                                    AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                    informationPopUpShow = !informationPopUpShow
                                }
                                information.informationPopupWindow.show()
                            }

                        }
                        is GameKernel.Input.Event.KeyReleased -> {
                            if (e.data.keyCode == KeyEvent.VK_Z) {
                                informationPopUpShow = !informationPopUpShow
                                information.informationPopupWindow.hide()
                            }

                            if(e.data.keyCode == KeyEvent.VK_ESCAPE){
                                if(!escPopupShow){
                                    AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                    information.escPopupWindow.show()
                                    escPopupShow=!escPopupShow
                                }else{
                                    AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                    information.escPopupWindow.hide()
                                    escPopupShow=!escPopupShow
                                }
                            }
                            if(information.escPopupWindow.isShow&&e.data.keyCode==KeyEvent.VK_S){
                                SceneController.instance.change(StoryModeScene51())
                                GlobalRoundGame.reset()
                                AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)
                                AudioResourceController.getInstance().stop(Path.Sounds.STAGE_4)
                                AudioResourceController.getInstance().play(Path.Sounds.SELECTION)
                            }

                            if (!player.getInvincible() && player.isCollision(bossCastle) && e.data.keyCode == KeyEvent.VK_C) {
                                startBattle(GlobalPlayer.Monsters.STAGE4_BOSS)
                                isBossBattle = true
                            }

                            //彩蛋事件
                            when {
                                !eventOne.isTriggered && player.isCollision(eventOne) && e.data.keyCode == KeyEvent.VK_C
                                -> eventOne()

                                !eventTwo.isTriggered && player.isCollision(eventTwo) && e.data.keyCode == KeyEvent.VK_C
                                -> eventTwo()
                            }
                        }
                    }
                }
                if (villageScene != null) {
                    when (e) {
                        is GameKernel.Input.Event.KeyKeepPressed -> {
                            villageScene!!.input?.invoke(e)
                            if (villageScene!!.endVillage() && e.data.keyCode == KeyEvent.VK_C) {
                                this.endVillage()
                            }
                        }
                        else -> villageScene!!.input?.invoke(e)
                    }
                }
            }
        }
}