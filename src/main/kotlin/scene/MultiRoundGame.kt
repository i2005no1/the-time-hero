package scene

import camera.Camera
import camera.MapInformation
import client.ClientClass
import client.CommandReceiver
import core.GameKernel
import core.Scene
import core.controllers.ResController
import core.controllers.SceneController
import core.utils.Delay
import maploader.MapInfo
import maploader.MapLoader
import obj.*
import obj.utils.Animator
import obj.utils.GameObject
import utils.*
import java.awt.Graphics
import java.awt.Image
import java.awt.event.KeyEvent

class MultiRoundGame : Scene() {
    class Multiplayer(x: Int, y: Int, width: Int, height: Int, val id: Int, val playerName: String, val stylingName: String) : Player(x, y, width, height) {

        var multiInVillage: Boolean = false
        var multiInBattle: Boolean = false
        var level: Int = 1
        var hp: Int = 0
        var hpLimit: Int = 0

        private val head: Image = ResController.instance.image(Path.Imgs.Actors.addPng(stylingName))

        init {
            animator = Animator(0, Animator.State.STOP, stylingName, true)
        }

        fun commandMove(x: String, y: String, walkDir: String) {
            setCenter(x.toInt(), y.toInt())
            setWalkDir(walkDir)
        }

        fun paintComponentPlayer(g: Graphics) {
            if (!multiInVillage && !multiInBattle) {
                super.paintComponent(g)
            } else {
                g.drawImage(
                        head, painter.centerX, painter.centerY, painter.centerX + 96, painter.centerY + 96,
                        0, 0, Global.UNIT_X, 19, null
                )
            }
        }

    }


    class Command {
        val ADD: Int = 1
        val MOVE = 2
        val BATTLE = 3
        val VILLAGE = 4
        val BACK_TO_JUNGLE = 5
        val LEVEL_HP_HPLIMIT = 6
        val INVINCIBLE = 7
        val NO_INVINCIBLE = 8
        val PRAY_TIME = 9
        val MONSTER_POSITION = 12
        val MONSTER_DIR = 13
        val BATTLE_MONSTER = 14
        val SOMEONE_ENTER_BOSS_ENTER = 15
        val UPDATE_GLOBAL_WHO_SEND_BOSS_ONE = 16
        val UPDATE_GLOBAL_WHO_SEND_BOSS_TWO = 17
        val UPDATE_GLOBAL_WHO_SEND_BOSS_THREE = 18
        val WIN = 20
        val BOSS_DEFEATED = 21
    }

    //Scene
    private var battleScene: BattleScene? = null
    private val information: Information = Information()
    private var isBattle: Boolean = false
    private var villageScene: VillageScene? = null
    private var isVillage: Boolean = false
    private var cantMoveHint: Boolean = false
    private var isGameOver = false
    private var informationPopUpShow: Boolean = false
    private var escPopupShow: Boolean = false
    private var multiBossBattleScene: MultiBossBattleScene? = null


    //GameObject
    private val startPositionX: Int = 4300
    private val startPositionY: Int = 3850
    private var myselfPlayer: Multiplayer = Multiplayer(startPositionX, startPositionY, 64, 64, ClientClass.getInstance().id, GlobalPlayer.playerName, GlobalPlayer.usingStyling.stylingName)
    private val multiPlayers: MutableList<Multiplayer> = mutableListOf()
    private val monsters: MutableList<Monster> = mutableListOf()
    private val villages: MutableList<Village> = mutableListOf()
    private val invincibleDelay: Delay = Delay(110).apply { loop() }
    private val bossCastle1 = BossCastle(1975, 4020, 192, 192)
    private val bossCastle2 = BossCastle(1700, 1325, 192, 192)
    private val bossCastle3 = BossCastle(4100, 2655, 192, 192)
    private val defeatedBoss: MutableList<Boolean> = mutableListOf()
    private var isBossBattle: Boolean = false //最終魔王

    //Image
    private val bgImg = ResController.instance.image(Path.Imgs.Backgrounds.MAP) //要改

    //camera
    private var camera: Camera = Camera.Builder(Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT - Global.WINDOW_HEIGHT / 6)
            .setChaseObj(myselfPlayer, 10.0, 10.0)
            .setCameraWindowLocation(0, 0)
            .gen()

    //map
    private var mapCollision: ArrayList<MapInfo> = MapLoader("/maps/genMap.bmp", "/maps/genMap.txt").combineInfo()
    private var mapCollisionObj: ArrayList<GameObject> = ArrayList()
    private var mapLoader: MapLoader = MapLoader("/maps/genMap.bmp", "/maps/genMap.txt")

    //internet
    private val sendClient: ArrayList<String> = arrayListOf()
    private val command: Command = Command()
    private var controlMonster: Boolean = false
    private val bossNumber: MutableList<Int> = mutableListOf()
    private var hero: Multiplayer? = null

    private var gameOverSound: Boolean = false

    override fun sceneBegin() {
        GlobalRoundGame.startTime = System.currentTimeMillis()

        AudioResourceController.getInstance().stop(Path.Sounds.SELECTION)
        AudioResourceController.getInstance().loop(Path.Sounds.STAGE_4, 20)

        bossNumber += 0
        bossNumber += 0
        bossNumber += 0
        defeatedBoss += false
        defeatedBoss += false

        GlobalRoundGame.startTime = System.currentTimeMillis()
        setMapLoader()
        createMons()
        createVillages()
        myselfPlayer.hp = GlobalRoundGame.hp
        myselfPlayer.hpLimit = GlobalRoundGame.hpLimit
        myselfPlayer.level = GlobalRoundGame.level
        MapInformation.setMapInfo(0, 0, 4800, 4800)

        multiPlayers += myselfPlayer
        //送自己位置給其他人
        sendClient += myselfPlayer.collider.centerX.toString()
        sendClient += myselfPlayer.collider.centerY.toString()
        sendClient += myselfPlayer.id.toString()
        sendClient += GlobalPlayer.playerName
        sendClient += myselfPlayer.stylingName
        ClientClass.getInstance().sent(1, sendClient)
        sendClient.clear()
        information.addTeamMember(800, 0, 180, 400, multiPlayers)
        information.teamMemberInformation!!.addHead(myselfPlayer.stylingName)

        sendClient += GlobalRoundGame.level.toString()
        sendClient += GlobalRoundGame.hp.toString()
        sendClient += GlobalRoundGame.hpLimit.toString()
        ClientClass.getInstance().sent(command.LEVEL_HP_HPLIMIT, sendClient);
        sendClient.clear()
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    private fun timeUpDowngrade() {
        information.resetTime()
        myselfPlayer.setCenter(startPositionX, startPositionY)//玩家死亡重生點
        GlobalRoundGame.level -= 4
        if (GlobalRoundGame.level <= 0) {
            GlobalRoundGame.level = 1
        }
        GlobalRoundGame.updatePlayerInformation()
        if(GlobalRoundGame.hp>GlobalRoundGame.hpLimit){
            GlobalRoundGame.hp=GlobalRoundGame.hpLimit
            GlobalRoundGame.updatePlayerInformation()
        }
    }

    override fun update(timePassed: Long) {
        //multiplayer
        ClientClass.getInstance().consume(object : CommandReceiver {
            override fun receive(serialNum: Int, commandCode: Int, strs: ArrayList<String>?) {
                //println("serialNum${serialNum} commandCode: ${commandCode}")
                when (commandCode) {
                    command.ADD -> {
                        //println("battlePlayer's Size:${multiPlayers.size}")
                        for (existPlayer in multiPlayers) {
                            println("existPlayerID:${existPlayer.id} serialNum:${serialNum}")
                            if (existPlayer.id == serialNum) {
                                return
                            }
                        }
                        //接收新進來的人的信息
                        multiPlayers += Multiplayer(
                                strs!![0].toInt(),
                                strs!![1].toInt(),
                                64,
                                64,
                                strs!![2].toInt(),
                                strs!![3],
                                strs!![4]
                        )
                        information.teamMemberInformation!!.addHead(strs!![4])
                        //告訴新進來的人，我在這裡！
                        sendClient += myselfPlayer.collider.centerX.toString()
                        sendClient += myselfPlayer.collider.centerY.toString()
                        sendClient += myselfPlayer.id.toString()
                        sendClient += GlobalPlayer.playerName
                        sendClient += myselfPlayer.stylingName
                        ClientClass.getInstance().sent(1, sendClient)
                        sendClient.clear()
                    }
                    command.MOVE -> {
                        if (myselfPlayer.id == serialNum) {
                            return
                        }
                        for (multiplayer in multiPlayers) {
                            if (multiplayer.id == serialNum) {
                                multiplayer.commandMove(strs!![0], strs!![1], strs!![2])
                                return
                            }
                        }
                    }
                    command.BATTLE -> {
                        if (myselfPlayer.id == serialNum) {
                            return
                        }
                        for (multiplayer in multiPlayers) {
                            if (multiplayer.id == serialNum) {
                                multiplayer.multiInBattle = true
                                return
                            }
                        }
                    }
                    command.VILLAGE -> {
                        if (myselfPlayer.id == serialNum) {
                            return
                        }
                        for (multiplayer in multiPlayers) {
                            if (multiplayer.id == serialNum) {
                                multiplayer.multiInVillage = true
                                return
                            }
                        }
                    }
                    command.BACK_TO_JUNGLE -> {
                        if (myselfPlayer.id == serialNum) {
                            return
                        }
                        for (multiplayer in multiPlayers) {
                            if (multiplayer.id == serialNum) {
                                multiplayer.multiInVillage = false
                                multiplayer.multiInBattle = false
                                return
                            }
                        }
                    }
                    command.LEVEL_HP_HPLIMIT -> {
                        if (myselfPlayer.id == serialNum) {
                            return
                        }
                        for (multiplayer in multiPlayers) {
                            if (multiplayer.id == serialNum) {
                                multiplayer.level = strs!![0].toInt()
                                multiplayer.hp = strs!![1].toInt()
                                multiplayer.hpLimit = strs!![2].toInt()
                                information.teamMemberInformation!!.updateMultiplayer(multiPlayers)
                                return
                            }
                        }
                    }
                    command.INVINCIBLE -> {
                        if (myselfPlayer.id == serialNum) {
                            return
                        }
                        for (multiplayer in multiPlayers) {
                            if (multiplayer.id == serialNum) {
                                multiplayer.invincible()
                                return
                            }
                        }
                    }
                    command.NO_INVINCIBLE -> {
                        if (myselfPlayer.id == serialNum) {
                            return
                        }
                        for (multiplayer in multiPlayers) {
                            if (multiplayer.id == serialNum) {
                                multiplayer.noInvincible()
                                multiplayer.isShow()
                                return
                            }
                        }
                    }
                    command.PRAY_TIME -> {
                        if (myselfPlayer.id == serialNum) {
                            return
                        }
                        information.resetTime()
                        GlobalRoundGame.prayPrice += 20
                    }
                    command.MONSTER_POSITION -> {
                        if (myselfPlayer.id == serialNum) {
                            return
                        }
                        var index: Int = 0
                        for (monster in monsters) {
                            monster.setCenter(strs!![index++].toInt(), strs!![index++].toInt())
                            if (Monster.State.ALIVE.name == strs!![index++]) {
                                monster.state = Monster.State.ALIVE
                            } else {
                                monster.state = Monster.State.DEAD
                            }
                        }
                    }
                    command.MONSTER_DIR -> {
                        if (myselfPlayer.id == serialNum) {
                            return
                        }
                        var index: Int = 0
                        for (monster in monsters) {
                            monster.setDir(strs!![index++])
                        }
                    }
                    command.BATTLE_MONSTER -> {
                        if (myselfPlayer.id == serialNum) {
                            return
                        }
                        for (monster in monsters) {
                            if (monsters.indexOf(monster) == strs!![0].toInt()) {
                                monster.state = Monster.State.DEAD
                            }
                        }
                    }
                    command.SOMEONE_ENTER_BOSS_ENTER -> {
                        if (myselfPlayer.id == serialNum) {
                            return
                        }
                        bossNumber[strs!![0].toInt()] = strs!![1].toInt()
                    }
                    command.UPDATE_GLOBAL_WHO_SEND_BOSS_ONE -> {
                        GlobalRoundGame.whoSendBossInformation[0] = bossNumber[0]
                    }
                    command.UPDATE_GLOBAL_WHO_SEND_BOSS_TWO -> {
                        GlobalRoundGame.whoSendBossInformation[1] = bossNumber[1]
                    }
                    command.UPDATE_GLOBAL_WHO_SEND_BOSS_THREE -> {
                        GlobalRoundGame.whoSendBossInformation[2] = bossNumber[2]
                    }
                    command.WIN -> {
                        for (player in multiPlayers) {
                            if (player.id == strs!![0].toInt()) {
                                hero = player
                            }
                        }
                        GlobalRoundGame.endTime = System.currentTimeMillis()
                        isGameOver = true
                    }
                    command.BOSS_DEFEATED -> {
                        defeatedBoss[strs!![0].toInt()] = true
                        if((strs!![0].toInt()+1) == 1) {
                            information.showBossOneDefeated = true
                        }else if((strs!![0].toInt()+1) == 2) {
                            information.showBossTwoDefeated = true
                        }
                    }

                    else -> multiBossBattleScene?.bossBattleReceive(serialNum, commandCode, strs)

                }
            }
        })
        //倒數階段
        if (!information.isStart()) {
            myselfPlayer.setCenter(startPositionX, startPositionY)
        }
        //鏡頭移動
        camera.update(timePassed)

        //BattleScene update
        if (isBattle) {
            if (!isGameOver) {
                if (battleScene != null) {
                    battleScene?.update(timePassed)
                    //multiplayer
                    if (GlobalRoundGame.attacked) {
                        myselfPlayer.hp = GlobalRoundGame.hp
                        sendClient += GlobalRoundGame.level.toString()
                        sendClient += GlobalRoundGame.hp.toString()
                        sendClient += GlobalRoundGame.hpLimit.toString()
                        ClientClass.getInstance().sent(command.LEVEL_HP_HPLIMIT, sendClient);
                        sendClient.clear()
                        GlobalRoundGame.attacked = false
                    }
                    if (battleScene!!.monsterHp() == 0) {
                        GlobalRoundGame.newText = mutableListOf()
                        information.resetPrintText()
                        GlobalRoundGame.exp = GlobalRoundGame.exp + battleScene!!.monsterExp()
                        var addMoney = battleScene!!.monsterMoney()
                        GlobalRoundGame.roundMoney = GlobalRoundGame.roundMoney + addMoney
                        GlobalRoundGame.addNewText("Defeated " + battleScene!!.monsterName() + "! Gained " + battleScene!!.monsterExp() + " EXP and " + addMoney + " G!")
                        GlobalRoundGame.monsterKilled++
                        endBattle()
                    }
                }
                if (multiBossBattleScene != null) {
                    multiBossBattleScene?.update(timePassed)
                    //multiplayer
                    if (GlobalRoundGame.attacked) {
                        myselfPlayer.hp = GlobalRoundGame.hp
                        sendClient += GlobalRoundGame.level.toString()
                        sendClient += GlobalRoundGame.hp.toString()
                        sendClient += GlobalRoundGame.hpLimit.toString()
                        ClientClass.getInstance().sent(command.LEVEL_HP_HPLIMIT, sendClient);
                        sendClient.clear()
                        GlobalRoundGame.attacked = false
                    }
                    if (multiBossBattleScene!!.monsterHp() == 0) {
                        GlobalRoundGame.newText = mutableListOf()
                        information.resetPrintText()
                        GlobalRoundGame.endTime = System.currentTimeMillis()
                        //第三隻GameOver
                        if (multiBossBattleScene!!.monsterName() == GlobalPlayer.Monsters.MULTI_BOSS3.monsterName) {
                            isGameOver = true
                            sendClient += multiBossBattleScene!!.whoIsTrueHero()!!.id.toString()
                            ClientClass.getInstance().sent(command.WIN, sendClient)
                            sendClient.clear()
                        } else {
                            if (multiBossBattleScene!!.monsterName() == GlobalPlayer.Monsters.MULTI_BOSS1.monsterName) {
                                sendClient.clear()
                                sendClient += "0"
                                ClientClass.getInstance().sent(command.BOSS_DEFEATED, sendClient)
                                sendClient.clear()
                                endBossBattle()
                            } else if (multiBossBattleScene!!.monsterName() == GlobalPlayer.Monsters.MULTI_BOSS2.monsterName) {
                                sendClient.clear()
                                sendClient += "1"
                                ClientClass.getInstance().sent(command.BOSS_DEFEATED, sendClient)
                                sendClient.clear()
                                endBossBattle()
                            }
                        }
                    }
                    if (multiBossBattleScene != null) {
                        if (multiBossBattleScene!!.getRemoveBossScene() && multiBossBattleScene!!.monsterHp() != 0) {
                            if (multiBossBattleScene!!.theLastOneRemove()) {
                                if (multiBossBattleScene!!.monsterName() == GlobalPlayer.Monsters.MULTI_BOSS1.monsterName) {
                                    ClientClass.getInstance().sent(command.UPDATE_GLOBAL_WHO_SEND_BOSS_ONE, sendClient)
                                } else if (multiBossBattleScene!!.monsterName() == GlobalPlayer.Monsters.MULTI_BOSS2.monsterName) {
                                    ClientClass.getInstance().sent(command.UPDATE_GLOBAL_WHO_SEND_BOSS_TWO, sendClient)
                                } else if (multiBossBattleScene!!.monsterName() == GlobalPlayer.Monsters.MULTI_BOSS3.monsterName) {
                                    ClientClass.getInstance().sent(command.UPDATE_GLOBAL_WHO_SEND_BOSS_THREE, sendClient)

                                }
                            }
                            sendClient += multiBossBattleScene!!.playerNumber.toString()
                            ClientClass.getInstance().sent(55, sendClient)
                            sendClient.clear()
                            ClientClass.getInstance().sent(54, arrayListOf())
                            endBossBattle()
                        }
                    }
                }

                if (GlobalRoundGame.hp == 0) {
                    GlobalRoundGame.newText = mutableListOf()
                    information.resetPrintText()
                    GlobalRoundGame.addNewText(GlobalPlayer.playerName + " was defeated...")
                    if (isBossBattle) {
                        endBossBattle()
                    } else {
                        endBattle()
                    }
                    isBossBattle = false
                    GlobalRoundGame.hp = 10
                    myselfPlayer.setCenter(startPositionX, startPositionY) //玩家死亡重生點
                }
                if (information.time <= 0) {
                    GlobalRoundGame.newText = mutableListOf()
                    information.resetPrintText()
                    timeUpDowngrade()
                    if (isBossBattle) {
                        endBossBattle()
                    } else {
                        endBattle()
                    }
                }
            }


            // VillageScene
        } else if (isVillage && !isGameOver) {

            villageScene?.update(timePassed)
            if (GlobalRoundGame.ateRecoveryFood) {
                sendClient += GlobalRoundGame.level.toString()
                sendClient += GlobalRoundGame.hp.toString()
                sendClient += GlobalRoundGame.hpLimit.toString()
                ClientClass.getInstance().sent(command.LEVEL_HP_HPLIMIT, sendClient);
                sendClient.clear()
                GlobalRoundGame.ateRecoveryFood = false
            }
            if (villageScene!!.getIsResetTime()) {
                information.resetTime()
                villageScene!!.noResetTime()
                //multiplayer
                ClientClass.getInstance().sent(9, sendClient)
            }
            if (information.time <= 0) {
                timeUpDowngrade()
                endVillage()
            }
        } else { //JungleScene update
            if (!isGameOver) {

                for (player in multiPlayers) {
                    player.update(timePassed)
                }
                if (!myselfPlayer.getInvincible()) {
                    myselfPlayer.isShow()


                } else { //Player invincible
                    if (invincibleDelay.count()) {
                        myselfPlayer.noInvincible()
                        ClientClass.getInstance().sent(8, sendClient)
                    }
                }
                for (monster in monsters) {
                    if (myselfPlayer.isCollision(monster) && monster.state == Monster.State.ALIVE) {
                        //multiplayer
                        sendClient += monsters.indexOf(monster).toString()
                        ClientClass.getInstance().sent(14, sendClient)
                        sendClient.clear()

                        startBattle(monster.monsterEnum)
                        monster.state = Monster.State.DEAD
                        break
                    } else {
                        if (controlMonster) {
                            monster.update(timePassed)
                        } else {
                            monster.animatorUpdate()
                        }
                    }
                }
                if (information.time <= 0) {
                    timeUpDowngrade()
                }
            }
        }
        //informationScene update
        if (!isGameOver) {
            information.update(timePassed)
        }

        //level up
        if (GlobalRoundGame.exp >= GlobalRoundGame.expLimit) {
            GlobalRoundGame.exp = GlobalRoundGame.exp - GlobalRoundGame.expLimit
            GlobalRoundGame.level++
            myselfPlayer.level = GlobalRoundGame.level
            AudioResourceController.getInstance().play(Path.Sounds.LEVEL_UP)
            GlobalRoundGame.updatePlayerInformation()
            myselfPlayer.hp = GlobalRoundGame.hp
            myselfPlayer.hpLimit = GlobalRoundGame.hpLimit
            information.showLevelUp = true

            sendClient += GlobalRoundGame.level.toString()
            sendClient += GlobalRoundGame.hp.toString()
            sendClient += GlobalRoundGame.hpLimit.toString()
            ClientClass.getInstance().sent(command.LEVEL_HP_HPLIMIT, sendClient);
            sendClient.clear()
        }

        //地圖碰撞
        canNotMove()

        //showEnterImg
        if (!isVillage && !isBattle) {
            for (village in villages) {
                if (myselfPlayer.isCollision(village)) {
                    information.showEnterImg = true
                    break
                } else {
                    information.showEnterImg = false
                }
            }
            if (myselfPlayer.isCollision(bossCastle1) || myselfPlayer.isCollision(bossCastle2) || myselfPlayer.isCollision(
                            bossCastle3
                    )
            ) {
                information.showEnterImg = true
            }
        } else {
            information.showEnterImg = false
        }

        //GameOver事件更新
        if (isGameOver && information.changeGameOverScene()) {
            SceneController.instance.change(MultiGameOverScene(hero!!, multiPlayers))
        }

        //multiplayer
        whoSendMonsterPosition()
    }

    private fun paintJungle(g: Graphics) {
        g.drawImage(
                bgImg, 0, 0,
                4800, 4800, null
        )
        //隱形物件
        mapCollisionObj.forEach { a -> a.paint(g) }
        for (village in villages) {
            if (village.isCollision(camera)) {
                village.paint(g)
            }
        }

        //非隱形物件
        for (monster in monsters) {
            if (monster.isCollision(camera)) {
                monster.paint(g)
            }
        }

        if (camera.isCollision(bossCastle1)) {
            bossCastle1.paint(g)
        }
        if (camera.isCollision(bossCastle2)) {
            bossCastle2.paint(g)
        }
        if (camera.isCollision(bossCastle3)) {
            bossCastle3.paint(g)
        }

        for (player in multiPlayers) {
            player.paintComponentPlayer(g)
        }

    }

    override fun paint(g: Graphics) {
        camera.start(g)
        if (isBattle) {
            camera.end(g)
            if (isBossBattle) {
                multiBossBattleScene?.paint(g)
            } else {
                battleScene?.paint(g)
            }
        } else if (isVillage) {
            camera.end(g)
            villageScene?.paint(g)
        } else {
            paintJungle(g)
        }
        camera.end(g)
        information.paint(g)
        for (gameObject in mapCollisionObj) {
            gameObject.painter.left
        }
        if (isGameOver) {
            if (!gameOverSound) {
                AudioResourceController.getInstance().stop(Path.Sounds.STAGE_4)
                AudioResourceController.getInstance().loop(Path.Sounds.GAME_OVER, 5)
                gameOverSound = true
            }
            information.gameOverTimeGoddessTalk(g)
        }

    }

    private fun startBattle(monsterEnum: GlobalPlayer.Monsters) {
        //告訴所有人我進入Battle
        ClientClass.getInstance().sent(3, sendClient);

        battleScene = BattleScene(monsterEnum)
        isBattle = true
    }

    private fun endBattle() {
        //告訴所有人我回來Jungle
        ClientClass.getInstance().sent(5, sendClient);
        isBattle = false
        battleScene = null
        myselfPlayer.invincible()
        ClientClass.getInstance().sent(7, sendClient);
    }

    private fun startBossBattle(monsterEnum: GlobalPlayer.Monsters, bossIndex: Int) {
        //告訴所有人我進入Battle
        ClientClass.getInstance().sent(3, sendClient)

        multiBossBattleScene = MultiBossBattleScene(monsterEnum, bossNumber[bossIndex]++, bossIndex).apply { start() }
        sendClient += bossIndex.toString()
        sendClient += bossNumber[bossIndex].toString()
        ClientClass.getInstance().sent(command.SOMEONE_ENTER_BOSS_ENTER, sendClient)
        sendClient.clear()

        isBattle = true
    }

    private fun endBossBattle() {
        //告訴所有人我回來Jungle
        ClientClass.getInstance().sent(5, sendClient);
        isBattle = false
        isBossBattle = false
        multiBossBattleScene = null
        myselfPlayer.invincible()
        ClientClass.getInstance().sent(7, sendClient);
    }

    private fun startVillage(village: Village) {
        //告訴所有人我進入Village
        ClientClass.getInstance().sent(4, sendClient);
        villageScene = VillageScene(village.background)
        villageScene!!.sceneBegin()
        isVillage = true
    }

    private fun endVillage() {
        //告訴所有人我回來Jungle
        ClientClass.getInstance().sent(5, sendClient);
        isVillage = false
        villageScene = null
        myselfPlayer.invincible()
        ClientClass.getInstance().sent(7, sendClient);
    }

    private fun canNotMove() {
        mapCollisionObj.forEach { a ->
            if (myselfPlayer.isCollision(a)) {
                if (myselfPlayer.getDir() == Global.Direction.LEFT && myselfPlayer.collider.left < a.collider.right) {
                    myselfPlayer.setCenter(a.painter.centerX + Global.UNIT_X * 2 + 1, myselfPlayer.painter.centerY)
                } else if (myselfPlayer.getDir() == Global.Direction.UP && myselfPlayer.collider.top < a.collider.bottom) {
                    myselfPlayer.setCenter(myselfPlayer.painter.centerX, a.painter.centerY + Global.UNIT_Y * 2 + 1)
                } else if (myselfPlayer.getDir() == Global.Direction.RIGHT && myselfPlayer.collider.right > a.collider.left) {
                    myselfPlayer.setCenter(a.painter.centerX - Global.UNIT_X * 2 - 1, myselfPlayer.painter.centerY)
                } else if (myselfPlayer.getDir() == Global.Direction.DOWN && myselfPlayer.collider.bottom > a.collider.top) {
                    myselfPlayer.setCenter(myselfPlayer.painter.centerX, a.painter.centerY - Global.UNIT_Y * 2 - 1)
                }
            }
        }
    }

    private fun createMons() {
        //第一區
        monsters += Monster(1000, 4000, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER4)
        monsters += Monster(1400, 3540, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER4)
        monsters += Monster(1500, 4300, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER4)
        monsters += Monster(1760, 4500, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER4)
        monsters += Monster(1800, 3200, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER4)

        monsters += Monster(1800, 3600, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER3)
        monsters += Monster(2200, 4400, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER3)
        monsters += Monster(2340, 3840, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER3)
        monsters += Monster(2800, 4200, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER3)
        monsters += Monster(3200, 4400, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER3)

        monsters += Monster(2400, 3400, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER2)
        monsters += Monster(2710, 2740, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER2)
        monsters += Monster(2800, 3000, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER2)
        monsters += Monster(2870, 3340, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER2)
        monsters += Monster(3240, 3120, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER2)

        monsters += Monster(3700, 3900, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER1)
        monsters += Monster(4060, 4250, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER1)
        monsters += Monster(4200, 4000, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER1)
        monsters += Monster(4360, 3480, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER1)
        monsters += Monster(4400, 4200, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER1)
        //第二區
        monsters += Monster(650, 2850, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER5)
        monsters += Monster(1085, 2765, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER5)
        monsters += Monster(1160, 2460, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER5)
        monsters += Monster(1500, 2450, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER5)
        monsters += Monster(1575, 2160, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER5)

        monsters += Monster(740, 2240, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER6)
        monsters += Monster(385, 2265, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER6)
        monsters += Monster(550, 1970, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER6)
        monsters += Monster(405, 1770, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER6)
        monsters += Monster(920, 1805, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER6)

        monsters += Monster(980, 1405, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER7)
        monsters += Monster(340, 1255, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER7)
        monsters += Monster(705, 1210, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER7)
        monsters += Monster(1270, 1270, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER7)
        monsters += Monster(1390, 1475, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER7)

        monsters += Monster(1065, 800, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER8)
        monsters += Monster(970, 545, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER8)
        monsters += Monster(1315, 610, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER8)
        monsters += Monster(1645, 580, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER8)
        monsters += Monster(1555, 265, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER8)
        //第三區
        monsters += Monster(2155, 310, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER9)
        monsters += Monster(2335, 560, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER9)
        monsters += Monster(2400, 870, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER9)
        monsters += Monster(2600, 405, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER9)
        monsters += Monster(2630, 680, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER9)

        monsters += Monster(2980, 865, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER10)
        monsters += Monster(3325, 1075, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER10)
        monsters += Monster(3835, 1260, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER10)
        monsters += Monster(4255, 1260, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER10)
        monsters += Monster(4250, 700, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER10)

        monsters += Monster(2800, 1250, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER11)
        monsters += Monster(2615, 1500, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER11)
        monsters += Monster(3010, 1550, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER11)
        monsters += Monster(2970, 1865, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER11)
        monsters += Monster(3550, 1635, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER11)

        monsters += Monster(3630, 2050, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER12)
        monsters += Monster(3390, 2240, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER12)
        monsters += Monster(3700, 2465, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER12)
        monsters += Monster(4250, 2150, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER12)
        monsters += Monster(4410, 2380, 64, 64, GlobalPlayer.Monsters.MULTI_MONSTER12)
    }

    private fun createVillages() {
        villages += Village(4320, 3790, 300, 128, "village1")
        villages += Village(2690, 3790, 96, 96, "village2")
        villages += Village(1780, 3170, 200, 96, "village3")
        villages += Village(480, 4465, 100, 100, "village4")
        villages += Village(865, 2450, 96, 96, "village5")
        villages += Village(335, 1000, 300, 128, "village6")
        villages += Village(2255, 720, 100, 100, "village7")
        villages += Village(4370, 1060, 100, 100, "village8")
        villages += Village(3890, 2300, 200, 100, "village9")
    }

    private fun setMapLoader() {
        mapCollisionObj = mapLoader.creatObjectArray("canNotMove", 64,
                mapCollision, MapLoader.CompareClass { gameObject, name, mapInfo, MapObjectSize ->
            var tmp: GameObject? = null
            if (gameObject == name) {
                tmp = MapCollision(
                        mapInfo.x * MapObjectSize + 32, mapInfo.y * MapObjectSize + 32,
                        mapInfo.sizeX * MapObjectSize, mapInfo.sizeY * MapObjectSize
                )

            }
            return@CompareClass tmp;
        })
    }

    private fun whoSendMonsterPosition() {
        var id: Int = 100
        var sent: Boolean = false
        for (y in 0 until multiPlayers.size) {
            if (myselfPlayer.id == id && !isBattle && !isVillage) {
                for (monster in monsters) {
                    sendClient += monster.painter.centerX.toString()
                    sendClient += monster.painter.centerY.toString()
                    sendClient += monster.state.toString()
                }
                ClientClass.getInstance().sent(12, sendClient)
                sendClient.clear()
                for (monster in monsters) {
                    sendClient += monster.getDirString()
                }
                ClientClass.getInstance().sent(13, sendClient)
                sendClient.clear()
                controlMonster = true
                return
            } else {
                for (i in 1 until multiPlayers.size) {
                    if (multiPlayers[i].id == id && !multiPlayers[i].multiInBattle && !multiPlayers[i].multiInBattle) {
                        sent = true
                        controlMonster = false
                        break
                    }
                }
            }
            if (sent) {
                return
            } else {
                id++
            }
        }
    }



    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { e ->
            run {
                if (isBattle) {
                    when (e) {
                        is GameKernel.Input.Event.KeyPressed -> {
                            if (!isBossBattle) {
                                if (!battleScene!!.isFleeUsed && e.data.keyCode == KeyEvent.VK_C) {
                                    battleScene!!.isFleeUsed = true
                                    if (Global.random(0, 100) < GlobalRoundGame.FLEE_PROBABILITY) {
                                        GlobalRoundGame.newText = mutableListOf()
                                        information.resetPrintText()
                                        GlobalRoundGame.addNewText(GlobalPlayer.playerName + " ran away ...")
                                        endBattle()
                                    } else {
                                        GlobalRoundGame.newText = mutableListOf()
                                        information.resetPrintText()
                                        GlobalRoundGame.addNewText(GlobalPlayer.playerName + " failed to escape ...")
                                    }
                                }
                            } else {
                                if (!multiBossBattleScene!!.isFleeUsed && e.data.keyCode == KeyEvent.VK_C) {
                                    multiBossBattleScene!!.isFleeUsed = true
                                    if (Global.random(0, 100) < GlobalRoundGame.FLEE_PROBABILITY) {
                                        GlobalRoundGame.newText = mutableListOf()
                                        information.resetPrintText()
                                        GlobalRoundGame.addNewText(GlobalPlayer.playerName + " ran away ...")
                                        multiBossBattleScene!!.removeBossScene()
                                    } else {
                                        GlobalRoundGame.newText = mutableListOf()
                                        information.resetPrintText()
                                        GlobalRoundGame.addNewText(GlobalPlayer.playerName + " failed to escape ...")
                                    }
                                }
                            }
                        }
                    }
                }
                if (isBattle || !isVillage) {
                    when (e) {
                        is GameKernel.Input.Event.KeyPressed -> {
                            if (GlobalRoundGame.havingHerb && e.data.keyCode == KeyEvent.VK_X) {
                                AudioResourceController.getInstance().play(Path.Sounds.HERB)
                                GlobalRoundGame.hp = GlobalRoundGame.hpLimit
                                myselfPlayer.hp = GlobalRoundGame.hp
                                sendClient += GlobalRoundGame.level.toString()
                                sendClient += GlobalRoundGame.hp.toString()
                                sendClient += GlobalRoundGame.hpLimit.toString()
                                ClientClass.getInstance().sent(command.LEVEL_HP_HPLIMIT, sendClient);
                                sendClient.clear()
                                GlobalRoundGame.setHerb()
                                GlobalRoundGame.addNewText("" + myselfPlayer + " used herb!!")
                                GlobalRoundGame.updatePlayerInformation()
                            }
                        }
                    }
                }
                if (!isBattle && !isVillage) {
                    myselfPlayer.input?.invoke(e)

                    sendClient += myselfPlayer.collider.centerX.toString()
                    sendClient += myselfPlayer.collider.centerY.toString()
                    sendClient += myselfPlayer.getWalkDirString()
                    ClientClass.getInstance().sent(2, sendClient)
                    sendClient.clear()

                    when (e) {
                        is GameKernel.Input.Event.KeyKeepPressed -> {
                            for (village in villages) {
                                if (!myselfPlayer.getInvincible() && myselfPlayer.isCollision(village) && e.data.keyCode == KeyEvent.VK_C) {
                                    this.startVillage(village)
                                }
                            }
                            if (e.data.keyCode == KeyEvent.VK_Z) {
                                if (!informationPopUpShow) {
                                    AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                    informationPopUpShow = !informationPopUpShow
                                }
                                information.informationPopupWindow.show()
                            }

                        }
                        is GameKernel.Input.Event.KeyReleased -> {
                            if (e.data.keyCode == KeyEvent.VK_Z) {
                                informationPopUpShow = !informationPopUpShow
                                information.informationPopupWindow.hide()
                            }
                            if (e.data.keyCode == KeyEvent.VK_ESCAPE) {
                                if (!escPopupShow) {
                                    AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                    information.escPopupWindow.show()
                                    escPopupShow = !escPopupShow
                                } else {
                                    AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                    information.escPopupWindow.hide()
                                    escPopupShow = !escPopupShow
                                }
                            }
                            if (information.escPopupWindow.isShow && e.data.keyCode == KeyEvent.VK_S) {
                                SceneController.instance.change(StoryModeScene51())
                                GlobalRoundGame.reset()
                                AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)
                                AudioResourceController.getInstance().stop(Path.Sounds.STAGE_4)
                                AudioResourceController.getInstance().play(Path.Sounds.SELECTION)
                            }
                            if (!myselfPlayer.getInvincible() && myselfPlayer.isCollision(bossCastle1) && e.data.keyCode == KeyEvent.VK_C) {
                                if (!defeatedBoss[0]) {
                                    startBossBattle(GlobalPlayer.Monsters.MULTI_BOSS1, 0)
                                    isBossBattle = true
                                } else {
                                    GlobalRoundGame.addNewText("The boss has been defeated!")
                                }
                            }
                            if (!myselfPlayer.getInvincible() && myselfPlayer.isCollision(bossCastle2) && e.data.keyCode == KeyEvent.VK_C) {
                                if (defeatedBoss[1]) {
                                    GlobalRoundGame.addNewText("The boss has been defeated!")
                                } else if (!defeatedBoss[0]) {
                                    GlobalRoundGame.addNewText("You have to defeat boss1.")
                                } else if (!defeatedBoss[1] && defeatedBoss[0]) {
                                    startBossBattle(GlobalPlayer.Monsters.MULTI_BOSS2, 1)
                                    isBossBattle = true
                                }
                            }
                            if (!myselfPlayer.getInvincible() && myselfPlayer.isCollision(bossCastle3) && e.data.keyCode == KeyEvent.VK_C) {
                                if (defeatedBoss[0] && defeatedBoss[1]) {
                                    startBossBattle(GlobalPlayer.Monsters.MULTI_BOSS3, 2)
                                    isBossBattle = true
                                } else {
                                    GlobalRoundGame.addNewText("You have to defeat boss1 and boss2!")
                                }
                            }
                        }
                    }
                }
                if (villageScene != null) {
                    when (e) {
                        is GameKernel.Input.Event.KeyKeepPressed -> {
                            villageScene!!.input?.invoke(e)
                            if (villageScene!!.endVillage() && e.data.keyCode == KeyEvent.VK_C) {
                                myselfPlayer.hp = GlobalRoundGame.hp
                                sendClient += GlobalRoundGame.level.toString()
                                sendClient += GlobalRoundGame.hp.toString()
                                sendClient += GlobalRoundGame.hpLimit.toString()
                                ClientClass.getInstance().sent(command.LEVEL_HP_HPLIMIT, sendClient);
                                sendClient.clear()
                                this.endVillage()
                            }
                        }
                        else -> villageScene!!.input?.invoke(e)
                    }
                }
            }
        }
}