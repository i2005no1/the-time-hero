package scene

import core.GameKernel
import core.Scene
import core.controllers.ResController
import core.controllers.SceneController
import menu.EditText
import utils.Global
import utils.GlobalPlayer
import utils.Path
import java.awt.Graphics
import java.awt.event.KeyEvent

class MultiConnectInput : Scene() {
    enum class State {
        IP, PORT,
    }

    private val ipBgImg = ResController.instance.image(Path.Imgs.Backgrounds.MULTI_INPUT_IP)
    private val portBgImg = ResController.instance.image(Path.Imgs.Backgrounds.MULTI_INPUT_PORT)
    private val editTextIP: EditText = EditText(462, 214, 200, 80, "    Enter IP   ")
    private val editTextPort: EditText = EditText(495, 354, 200, 80, "  Enter Port   ")
    private var state: State = State.IP
    private var ip: String = "192.168.1.31"
    private var port: Int = 12345

    override fun sceneBegin() {
        editTextIP.setEditText("$ip")
        editTextPort.setEditText("$port")
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    override fun update(timePassed: Long) {
        //ip
        //port
        //name
    }

    override fun paint(g: Graphics) {
        if (state == State.IP) {
            g.drawImage(ipBgImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        } else if (state == State.PORT) {
            g.drawImage(portBgImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        }
        editTextIP.paint(g)
        editTextPort.paint(g)

    }

    private fun changeScene(change: Scene) {
        SceneController.instance.change(change)
    }

    private fun keyCodeRange(e: Int): Boolean {
        if (e in 48..57) {
            return true
        }
        if (e in 96..105) {
            return true
        }
        if (e == 110) {
            return true
        }
        return false
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { e ->
            run {
                when (e) {
                    is GameKernel.Input.Event.KeyPressed -> {
                        if (e.data.keyCode == KeyEvent.VK_ENTER) {
                            if (state == State.IP) {
                                if (!editTextIP.getFocus()) {
                                    editTextIP.isFocus()
                                    AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                                } else {
                                    editTextIP.unFocus()
                                    AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                                    ip = editTextIP.getEditText()
                                }
                            } else if (state == State.PORT) {
                                if (!editTextPort.getFocus()) {
                                    editTextPort.isFocus()
                                    AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                                } else {
                                    editTextPort.unFocus()
                                    AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                                    if (editTextPort.getEditText() == "") {
                                        port = 0
                                    } else {
                                        port = editTextPort.getEditText().toInt()
                                    }
                                }
                            }
                        }
                        if (editTextIP.getFocus() && e.data.keyCode != KeyEvent.VK_ENTER && keyCodeRange(e.data.keyCode)) {
                            editTextIP.input!!.invoke(e)
                        }
                        if (editTextIP.getFocus() && e.data.keyCode != KeyEvent.VK_ENTER && e.data.keyCode == 46) {
                            editTextIP.input!!.invoke(e)
                        }
                        if (editTextIP.getFocus() && e.data.keyCode == KeyEvent.VK_BACK_SPACE) {
                            editTextIP.input!!.invoke(e)
                        }
                        if (editTextPort.getFocus() && e.data.keyCode != KeyEvent.VK_ENTER && keyCodeRange(e.data.keyCode)) {
                            editTextPort.input!!.invoke(e)
                        }
                        if (editTextPort.getFocus() && e.data.keyCode == KeyEvent.VK_BACK_SPACE) {
                            editTextPort.input!!.invoke(e)
                        }
                        if (e.data.keyCode == KeyEvent.VK_Z && !editTextPort.getFocus() && !editTextIP.getFocus()) {
                            if (ip != "" && port != 0) {
                                AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)
                                changeScene(
                                    MultiWaitingHome(
                                        ip,
                                        port,
                                        GlobalPlayer.playerName,
                                        GlobalPlayer.usingStyling
                                    )
                                )
                                //192.168.1.20

//                                changeScene(MultiWaitingHome("192.168.1.31", 12345, GlobalPlayer.playerName, GlobalPlayer.usingStyling))


                            } else {
                                AudioResourceController.getInstance().play(Path.Sounds.CANNOT_MOVE)
                            }
                        }
                        if (e.data.keyCode == KeyEvent.VK_UP && !editTextPort.getFocus() && !editTextIP.getFocus()) {
                            state = State.IP
                        }
                        if (e.data.keyCode == KeyEvent.VK_DOWN && !editTextPort.getFocus() && !editTextIP.getFocus()) {
                            state = State.PORT
                        }
                        if (e.data.keyCode == KeyEvent.VK_X) {
                            changeScene(SelectionScene4())
                        }
                    }
                }

            }
        }

}