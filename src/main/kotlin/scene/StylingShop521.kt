package scene

import core.GameKernel
import core.Scene
import core.controllers.ResController
import core.controllers.SceneController
import core.utils.Delay
import obj.utils.Animator
import utils.Global
import utils.GlobalPlayer
import utils.Path
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.event.KeyEvent

class StylingShop521: core.Scene() {
    private val bgImg = ResController.instance.image(Path.Imgs.Backgrounds.STYLING_SHOP)
    private var dontHaveEnoughMoney: Boolean = false
    private val delay: Delay = Delay(90).apply { loop() }

    class ChoiceItem(val stylingShow: GlobalPlayer.Styling, private val stylingPaint: StylingPaint, private val pricePaint: PricePaint) {
        private var animator: Animator = Animator(0, Animator.State.WALK, stylingShow.stylingName, true)
        private var isFocused: Boolean = false
        private var dir: Global.Direction = Global.Direction.DOWN

        fun isFocused() {
            isFocused = true
        }

        fun unFocused() {
            isFocused = false
        }

        fun update() {
            if (isFocused) {
                animator.setSta(Animator.State.WALK)
                animator.update()
            } else {
                dir = Global.Direction.DOWN
                animator.setSta(Animator.State.STOP)
            }
        }

        fun paintComponent(g: Graphics) {
            animator.paint(dir, stylingPaint.x, stylingPaint.y, stylingPaint.x + stylingPaint.width, stylingPaint.y + stylingPaint.height, g)
            g.color = Color.WHITE
            g.font = Font("", Font.BOLD, 20)
            g.drawString("$  " + stylingShow?.price, pricePaint.x, pricePaint.y)
        }
    }

    class NowFocusedChoice(private var rectPosition: ChoiceRectLight, var item: ChoiceItem?) {
        private val rectImg = ResController.instance.image(Path.Imgs.Objs.RECT)
        private val leftImg = ResController.instance.image(Path.Imgs.Objs.LEFT_ANG)
        private val rightImg = ResController.instance.image(Path.Imgs.Objs.RIGHT_ANG)

        fun paintComponent(g: Graphics) {
            if (item != null) {
                g.drawImage(rectImg, rectPosition.x, rectPosition.y, null)
            } else {
                if (rectPosition == ChoiceRectLight.LAST_PAGE) {
                    g.drawImage(leftImg, rectPosition.x, rectPosition.y, null)
                } else {
                    g.drawImage(rightImg, rectPosition.x, rectPosition.y, null)
                }
            }
        }

        fun nowFocusedChange(rectPosition: ChoiceRectLight, item: ChoiceItem?) {
            this.item?.unFocused()
            this.rectPosition = rectPosition
            this.item = item
            item?.isFocused()
        }
    }

    private var nowFocusedChoice: NowFocusedChoice = NowFocusedChoice(ChoiceRectLight.values()[0], null)
    private var choiceItemOneToSix: MutableList<ChoiceItem> = mutableListOf()

    enum class ChoiceRectLight(val x: Int, val y: Int) {
        ONE(95, 105),
        TWO(290, 105),
        THREE(487, 105),
        FOUR(95, 363),
        FIVE(290, 363),
        SIX(487, 363),
        LAST_PAGE(10, 320),
        NEXT_PAGE(680, 320)
    }

    enum class StylingPaint(val x: Int, val y: Int, val width: Int, val height: Int) {
        ONE(140, 157, 96, 96),
        TWO(338, 157, 96, 96),
        THREE(535, 157, 96, 96),
        FOUR(140, 415, 96, 96),
        FIVE(338, 415, 96, 96),
        SIX(535, 415, 96, 96)
    }

    enum class PricePaint(val x: Int, val y: Int) {
        ONE(145, 295),
        TWO(343, 295),
        THREE(540, 295),
        FOUR(145, 553),
        FIVE(343, 553),
        SIX(540, 553),
    }

    private var nowChoiceIndex: Int = 0
    private var nowPage: Int = 1

    override fun sceneBegin() {
        changePage(nowPage)
        nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], choiceItemOneToSix[nowChoiceIndex])
    }

    private fun changePage(page: Int) {
        choiceItemOneToSix.clear()
        var start = 0 + (page - 1) * 6
        var end = 5 + (page - 1) * 6
        var i = 0
        var y = 0
        for (styling in GlobalPlayer.Styling.values()) {
            if (styling.isBought) {
                continue
            }
            if (i < start && !styling.isBought) {
                i++
                continue
            }
            if (!styling.isBought) {
                choiceItemOneToSix.add(ChoiceItem(styling, StylingPaint.values()[y], PricePaint.values()[y]))
                i++
                y++
            }
            if (i > end) {
                break
            }
        }
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    override fun update(timePassed: Long) {
        for (choiceItem in choiceItemOneToSix) {
            choiceItem.update()
        }
    }




    override fun paint(g: Graphics) {
        g.drawImage(bgImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        nowFocusedChoice.paintComponent(g)
        for (choiceItem in choiceItemOneToSix) {
            choiceItem.paintComponent(g)
        }
        g.color = Color.BLACK
        g.font = Font("", Font.BOLD, 30)
        g.drawString("" + GlobalPlayer.playerMoney, 850, 550)
        if(dontHaveEnoughMoney){
            g.drawString("You don't have enough money.", 150,Global.SCREEN_Y/2+25)
            if(delay.count()){
                dontHaveEnoughMoney = false
            }
        }

    }

    private fun changeScene(change: Scene) {
        SceneController.instance.change(change)
    }

    private fun filterTheLastOneStyle(): GlobalPlayer.Styling {
        var s: GlobalPlayer.Styling = GlobalPlayer.Styling.values()[0]
        for (styling in GlobalPlayer.Styling.values()) {
            if (!styling.isBought) {
                s = styling
            }
        }
        return s
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { e ->
            run {
                when (e) {
                    is GameKernel.Input.Event.KeyPressed -> {
                        if (e.data.keyCode == KeyEvent.VK_Z) {
                            if (nowChoiceIndex == 6 && nowPage != 1) {
                                AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                nowPage--
                                changePage(nowPage)
                            } else if (nowChoiceIndex == 7 && choiceItemOneToSix[choiceItemOneToSix.size - 1].stylingShow != filterTheLastOneStyle()) {
                                AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                nowPage++
                                changePage(nowPage)
                            } else if (nowChoiceIndex != 6 && nowChoiceIndex != 7) {
                                if (GlobalPlayer.playerMoney - nowFocusedChoice.item!!.stylingShow.price >= 0) {//popupWindow
                                    AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)
                                    GlobalPlayer.playerMoney -= nowFocusedChoice.item!!.stylingShow.price
                                    nowFocusedChoice.item!!.stylingShow.isBought = true
                                    if (nowFocusedChoice.item!!.stylingShow == filterTheLastOneStyle()) {
                                        nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex - 1], choiceItemOneToSix[nowChoiceIndex - 1])
                                    } else {
                                        nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], choiceItemOneToSix[nowChoiceIndex])
                                    }
                                    changePage(nowPage)
                                    GlobalPlayer.saveFile()
                                }else{
                                    AudioResourceController.getInstance().play(Path.Sounds.CANNOT_MOVE)
                                    dontHaveEnoughMoney = true
                                }
                            }
                        } else if (e.data.keyCode == KeyEvent.VK_X) {
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                            changeScene(GoddessRoomScene52())
                        } else if (e.data.keyCode == KeyEvent.VK_LEFT && nowChoiceIndex != 0 && nowChoiceIndex != 3 && nowChoiceIndex != 6 && nowChoiceIndex != 7) {
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            nowChoiceIndex--
                            nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], choiceItemOneToSix[nowChoiceIndex])
                        } else if (e.data.keyCode == KeyEvent.VK_RIGHT && nowChoiceIndex != 2 && nowChoiceIndex != choiceItemOneToSix.size - 1 && nowChoiceIndex != 6 && nowChoiceIndex != 7) {
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            nowChoiceIndex++
                            nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], choiceItemOneToSix[nowChoiceIndex])
                        } else if (e.data.keyCode == KeyEvent.VK_UP && nowChoiceIndex > 2 && nowChoiceIndex != 6 && nowChoiceIndex != 7) {
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            nowChoiceIndex -= 3
                            nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], choiceItemOneToSix[nowChoiceIndex])
                        } else if (e.data.keyCode == KeyEvent.VK_DOWN && nowChoiceIndex < 3 && nowChoiceIndex != 6 && nowChoiceIndex != 7) {
                            if (choiceItemOneToSix.size - 1 >= nowChoiceIndex + 3) {
                                nowChoiceIndex += 3
                                AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            } else {
                                if (choiceItemOneToSix.size > 3) {
                                    AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                    nowChoiceIndex = choiceItemOneToSix.size - 1
                                }
                            }
                            nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], choiceItemOneToSix[nowChoiceIndex])
                        } else if (e.data.keyCode == KeyEvent.VK_LEFT && nowChoiceIndex == 0 || nowChoiceIndex == 3) {
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            nowChoiceIndex = 6
                            nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], null)
                        } else if (e.data.keyCode == KeyEvent.VK_RIGHT && nowChoiceIndex == 2 || nowChoiceIndex == choiceItemOneToSix.size - 1) {
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            nowChoiceIndex = 7
                            nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], null)
                        } else if (e.data.keyCode == KeyEvent.VK_LEFT && nowChoiceIndex == 7) {
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            nowChoiceIndex = choiceItemOneToSix.size - 1
                            nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], choiceItemOneToSix[nowChoiceIndex])
                        } else if (e.data.keyCode == KeyEvent.VK_RIGHT && nowChoiceIndex == 6) {
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            nowChoiceIndex = 0
                            nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], choiceItemOneToSix[nowChoiceIndex])
                        }
                    }
                }
            }
        }

}