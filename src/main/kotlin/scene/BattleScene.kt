package scene

import core.GameKernel
import core.controllers.ResController
import obj.Monster
import obj.Player
import obj.utils.Animator
import utils.Global
import utils.GlobalPlayer
import utils.GlobalRoundGame
import utils.Path
import java.awt.Font
import java.awt.Graphics
import java.awt.Image

open class BattleScene(monsterJungle: GlobalPlayer.Monsters) : core.Scene() {
    protected var bgImg = ResController.instance.image(Path.Imgs.Backgrounds.Battle.addPng(monsterJungle.background))
    private val player: Player = Player(64, 420, 128, 128).apply { inBattleScene() }.apply { setAnimatorState(Animator.State.WALK) }//設好paint的東西
    protected val monster: Monster = Monster(1016, 356 + (256 - monsterJungle.size) / 2, monsterJungle.size, monsterJungle.size, monsterJungle).apply { inBattleScene() }
    protected val fleeKeyImg = ResController.instance.image(Path.Imgs.Objs.Information.FLEE_KEY)
    protected val fleeBgImg: Image = ResController.instance.image(Path.Imgs.Backgrounds.OBJ_BLUEBG)
    private val monHpLimit=monsterJungle.hpLimit

    var isFleeUsed: Boolean = false
    protected val damageDate: MutableList<DamageData> = mutableListOf()
    protected val strikeLight: MutableList<StrikeLight> = mutableListOf()
    protected var damageDateLimit: Int = 6
    protected var attackToMonsDamage: Int = 0
    protected var attackToPlayerDamage: Int = 0

    class DamageData(var x: Int, var y: Int, var data: Int, private val isPlayer: Boolean) {
        private var riseFastly: Int = 0
        private var riseSlowly: Int = 0
        private var downSlowly: Int = 0
        var remove: Boolean = false

        fun paint(g: Graphics) {
            g.font = Font("", Font.BOLD, 60)
            if (isPlayer) {
                g.color = java.awt.Color.ORANGE
            } else {
                g.color = java.awt.Color.RED
            }
            g.drawString("" + data, x, y)
        }

        fun update(timePassed: Long) {
            if (isPlayer) {
                if (riseFastly < 20) {
                    x -= 4
                    y -= 8
                    riseFastly++
                } else if (riseSlowly < 10) {
                    x -= 6
                    y -= 4
                    riseSlowly++
                } else if (downSlowly < 10) {
                    x -= 8
                    y += 4
                    downSlowly++
                } else {
                    remove = true
                }
            } else {
                if (riseFastly < 20) {
                    x += 4
                    y -= 8
                    riseFastly++
                } else if (riseSlowly < 10) {
                    x += 6
                    y -= 4
                    riseSlowly++
                } else if (downSlowly < 10) {
                    x += 8
                    y += 4
                    downSlowly++
                } else {
                    remove = true
                }

            }
        }
    }

    class StrikeLight(var x: Int, var y: Int) {
        private var battleStrikeSize = 1
        private val battleStrikeImg = ResController.instance.image(Path.Imgs.Objs.BATTLESTRIKE)
        var remove: Boolean = false

        fun paint(g: Graphics) {
            g.drawImage(
                    battleStrikeImg, x - 32 / 2 * battleStrikeSize, y - 32 / 2 * battleStrikeSize, 32 * battleStrikeSize, 32 * battleStrikeSize, null
            )
        }

        fun update(timePassed: Long) {
            battleStrikeSize++
            if (battleStrikeSize > 8) {
                remove = true
            }
        }
    }

    override fun sceneBegin() {

    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    protected fun damageToMons(): Int {
        var damage = GlobalRoundGame.attack - monster.defence
        if (damage > 20) {
            return ((damage * 0.8).toInt()..(damage * 1.2).toInt()).random()
        }
        return Global.random(10,20)
    }

    protected fun damageToPlayer(): Int {
        var damage = monster.attack - GlobalRoundGame.defence
        if (damage > 20) {
            return ((damage * 0.8).toInt()..(damage * 1.2).toInt()).random()
        }
        return Global.random(10,20)
    }

    protected fun playerStrength():Int{
        var strength = GlobalRoundGame.attack - monster.defence
        if (strength > 20) {
            return strength
        }
        return 20
    }

    protected fun monStrength():Int{
        var strength = monster.attack - GlobalRoundGame.defence
        if (strength > 20) {
            return strength
        }
        return 20
    }

    protected fun attackToMons() {  //玩家打怪物
        attackToMonsDamage = damageToMons()
        if (monster.hp - attackToMonsDamage > 0) {
            monster.hp = monster.hp - attackToMonsDamage
        } else {
            monster.hp = 0
        }
        if (damageDate.size <= damageDateLimit) {
            damageDate += DamageData(monster.painter.centerX + 60, monster.painter.centerY, attackToMonsDamage, false)
        }
        GlobalRoundGame.addNewText(monster.name + " received $attackToMonsDamage damage!")  //!!!!!!
    }

    protected fun attackToPlayer() { //怪物打玩家
        attackToPlayerDamage = damageToPlayer()
        GlobalRoundGame.attacked = true
        if (GlobalRoundGame.hp - attackToPlayerDamage > 0) {
            GlobalRoundGame.hp = GlobalRoundGame.hp - attackToPlayerDamage
        } else {
            GlobalRoundGame.hp = 0
        }
        if (damageDate.size <= damageDateLimit) {
            damageDate += DamageData(player.painter.centerX - 60, player.painter.centerY, attackToPlayerDamage, true)
        }
        GlobalRoundGame.addNewText(GlobalRoundGame.playerName + " took $attackToPlayerDamage damage!") //!!!!!!
    }

    private fun rebound() {
        var stronger = (playerStrength().toDouble()/monHpLimit) -
                (monStrength().toDouble()/GlobalRoundGame.hpLimit) //計算優勢(對怪物傷害占怪物最大血量百分比-對玩家傷害占玩家最大血量百分比)
//        println(stronger)
        if (stronger >= 0.1) {
            player.setdx(-22.0) //玩家有優勢
            monster.setdx(37.0)
        } else if (stronger <= -0.1) {
            player.setdx(-37.0) //怪物有優勢
            monster.setdx(22.0)
        } else {
            player.setdx(-22.0) //正常反彈距離
            monster.setdx(22.0)
        }
    }

    override fun update(timePassed: Long) {

        player.update(timePassed)
        monster.update(timePassed)

        if (player.isCollision(monster)) {
            AudioResourceController.getInstance().play(Path.Sounds.BATTLE)

            rebound()
            attackToMons()
            attackToPlayer()



            strikeLight += StrikeLight(player.painter.right, player.painter.centerY)
        }
        for (damage in damageDate) {
            damage.update(timePassed)
        }
        damageDate.removeIf { damage -> damage.remove }

        for (sl in strikeLight) {
            sl.update(timePassed)
        }
        strikeLight.removeIf { sl -> sl.remove }


    }

    fun monsterHp(): Int = monster.hp
    fun monsterName(): String = monster.name
    fun monsterExp(): Int = monster.exp
    fun monsterMoney(): Int = monster.money

    override fun paint(g: Graphics) {
        g.drawImage(bgImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT / 6 * 5, null)

        //flee
        g.drawImage(fleeBgImg, 0, 155, 270, 55, null)
        g.drawImage(fleeKeyImg, 70, 159, 40, 40, null)
        g.color = java.awt.Color.BLACK
        g.font = Font("", Font.BOLD, 20)
        g.drawString("Flee!!", 122, 186)
        player.paint(g)
        monster.paint(g)
        for (damage in damageDate) {
            damage.paint(g)
        }

        for (sl in strikeLight) {
            sl.paint(g)
        }


    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { }


}