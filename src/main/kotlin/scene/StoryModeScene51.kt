package scene

import core.GameKernel
import core.Scene
import core.controllers.ResController
import core.controllers.SceneController
import utils.Global
import utils.GlobalPlayer
import utils.GlobalRoundGame
import utils.Path
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.event.KeyEvent

class StoryModeScene51 : core.Scene() {
    enum class State{
        STAGE_ONE,STAGE_TWO,STAGE_THREE,STAGE_FOUR
    }

    private val cannotPlay = ResController.instance.image(Path.Imgs.Backgrounds.CANNOT_PLAY)
    private val stageOneImg = ResController.instance.image(Path.Imgs.Backgrounds.STORY_MODE_STAGE_ONE)
    private val stageTwoImg = ResController.instance.image(Path.Imgs.Backgrounds.STORY_MODE_STAGE_TWO)
    private val stageThreeImg = ResController.instance.image(Path.Imgs.Backgrounds.STORY_MODE_STAGE_THREE)
    private val stageFourImg = ResController.instance.image(Path.Imgs.Backgrounds.STORY_MODE_STAGE_FOUR)
    private val loading=ResController.instance.image(Path.Imgs.Objs.LOADING)
    private var isLoading=false
    private var enterDelay=0
    private var state: State = State.STAGE_ONE
    private var stateIndex: Int = 0
    private val start = ResController.instance.image(Path.Imgs.Objs.START)



    override fun sceneBegin() {
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    override fun update(timePassed: Long) {
    }

    override fun paint(g: Graphics) {
        if(state == State.STAGE_ONE){
            g.drawImage(stageOneImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        }else if(state == State.STAGE_TWO){
            g.drawImage(stageTwoImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        }else if(state == State.STAGE_THREE){
            g.drawImage(stageThreeImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        }else if(state == State.STAGE_FOUR){
            g.drawImage(stageFourImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        }
        if(GlobalPlayer.GameRecord.values()[stateIndex].canPlay){
            g.font = Font("", Font.BOLD, 25)
            g.color = Color.WHITE
            for(i in 0 until GlobalPlayer.GameRecord.values()[stateIndex].grade){
                g.drawImage(start,205 + i*50,497,null)
            }
            g.drawString(Global.timeFormat(GlobalPlayer.GameRecord.values()[stateIndex].time),490,525)
            g.drawString(""+GlobalPlayer.GameRecord.values()[stateIndex].moneySpent,280,568)
            g.drawString(""+GlobalPlayer.GameRecord.values()[stateIndex].playerLevel,560,568)
            g.drawString(""+GlobalPlayer.GameRecord.values()[stateIndex].monsterKilled,860,568)
        }else{
            g.drawImage(cannotPlay, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        }
        if(isLoading){
            g.drawImage(loading,390,330,200,50,null)
            enterStage()
        }
    }

    private fun changeScene(change: Scene) {
        SceneController.instance.change(change)
    }

    private fun enterStage(){
        if(enterDelay<2){
            enterDelay++
        }else{
            enterDelay=0
            isLoading=false
            if (state == State.STAGE_ONE) {
                changeScene(StageOneScene())
            } else if (state == State.STAGE_TWO && GlobalPlayer.GameRecord.STAGE_TWO.canPlay) {
                changeScene(StageTwoScene())
            } else if (state == State.STAGE_THREE && GlobalPlayer.GameRecord.STAGE_THREE.canPlay) {
                changeScene(StageThreeScene())
            } else if (state == State.STAGE_FOUR && GlobalPlayer.GameRecord.STAGE_FOUR.canPlay) {
                changeScene(StageFourScene())
            }
        }

    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = {e->
            run{
                when(e){
                    is GameKernel.Input.Event.KeyPressed ->{
                        if(e.data.keyCode == KeyEvent.VK_RIGHT && state != State.STAGE_FOUR){
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            state = State.values()[++stateIndex]
                        }else if(e.data.keyCode == KeyEvent.VK_LEFT && state != State.STAGE_ONE){
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            state = State.values()[--stateIndex]
                        }else if(e.data.keyCode == KeyEvent.VK_X){
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                            changeScene(SelectionScene4())
                        }else if(e.data.keyCode == KeyEvent.VK_Z){
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)
                            isLoading=true

                        }
                        //enter the stage map
                    }
                }
            }
        }
}