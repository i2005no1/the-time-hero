package scene

import core.GameKernel
import core.Scene
import core.controllers.ResController
import core.controllers.SceneController
import utils.Global
import utils.GlobalPlayer
import utils.Path
import java.awt.Graphics
import java.awt.event.KeyEvent

class SelectionScene4 : core.Scene() {
    enum class State {
        STORY_MODE, GODDESS_ROOM, MULTIPLAYER
    }

    private val storyModeImg = ResController.instance.image(Path.Imgs.Backgrounds.SELECTION_STORY_MODE)
    private val goddessRoomImg = ResController.instance.image(Path.Imgs.Backgrounds.SELECTION_GODDESS_ROOM)
    private val multiplayerImg = ResController.instance.image(Path.Imgs.Backgrounds.SELECTION_MULTI)
    private var state: State = State.STORY_MODE

    override fun sceneBegin() {
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    override fun update(timePassed: Long) {
    }

    override fun paint(g: Graphics) {
        if (state == State.STORY_MODE) {
            g.drawImage(storyModeImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        } else if (state == State.GODDESS_ROOM) {
            g.drawImage(goddessRoomImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        }else if(state == State.MULTIPLAYER) {
            g.drawImage(multiplayerImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        }
    }

    private fun changeScene(change: Scene) {
        SceneController.instance.change(change)
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { e ->
            run {
                when (e) {
                    is GameKernel.Input.Event.KeyPressed -> {
                        if (e.data.keyCode == KeyEvent.VK_LEFT) {
                            if(state == State.GODDESS_ROOM) {
                                AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                state = State.STORY_MODE
                            }else if(state == State.MULTIPLAYER) {
                                AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                state = State.GODDESS_ROOM
                            }
                        } else if (e.data.keyCode == KeyEvent.VK_RIGHT) {
                            if(state == State.GODDESS_ROOM) {
                                AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                state = State.MULTIPLAYER
                            }else if(state == State.STORY_MODE) {
                                AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                state = State.GODDESS_ROOM
                            }
                        }
                        if(e.data.keyCode == KeyEvent.VK_Z){
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)
                            if(state == State.STORY_MODE){
                                changeScene(StoryModeScene51())
                            }
                            if(state == State.GODDESS_ROOM){
                                changeScene(GoddessRoomScene52())
                            }
                            if(state == State.MULTIPLAYER){
                                changeScene(MultiConnectInput())
                            }
                        }
                        if(e.data.keyCode == KeyEvent.VK_X){ // 加圖
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                            AudioResourceController.getInstance().stop(Path.Sounds.SELECTION)
                            AudioResourceController.getInstance().loop(Path.Sounds.HOME,10)
                            changeScene(HomeScene1())
                        }
                    }
                }
            }
        }
}