package scene

import client.ClientClass
import client.CommandReceiver
import core.GameKernel
import core.Scene
import core.controllers.ResController
import core.controllers.SceneController
import obj.Player
import utils.Global
import utils.GlobalPlayer
import utils.GlobalRoundGame
import utils.Path
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.event.KeyEvent

class MultiStatisticScene(private val hero: MultiRoundGame.Multiplayer, private val multiPlayers: MutableList<MultiRoundGame.Multiplayer>,private val information: MutableList<MultiGameOverScene.InformationPlayer>) : Scene() {

    private val bgImg = ResController.instance.image(Path.Imgs.Backgrounds.MULTI_STATISTIC_BG)
    private val start = ResController.instance.image(Path.Imgs.Objs.START)
    private var playTime = GlobalRoundGame.endTime - GlobalRoundGame.startTime
    private var grade: Int =
            if (playTime <= GlobalPlayer.GradeLevel.values()[4].threeStart) {
                3
            } else if (playTime <= GlobalPlayer.GradeLevel.values()[4].twoStart) {
                2
            } else {
                1
            }

    override fun sceneBegin() {
        hero.setCenter(200, 315)
        hero.setSize(64, 64)
        var i: Int = 0
        for (player in multiPlayers) {
            if (player.id == hero.id) {
                continue
            }
            player.setSize(64, 64)
            player.setCenter(140, 390 + i * 100)

            i++
        }
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    override fun update(timePassed: Long) {

    }

    override fun paint(g: Graphics) {
        g.drawImage(bgImg, 0, 0, null)
        g.font = Font("", Font.BOLD, 25)
        //Grade
        for (i in 0 until grade) {
            g.drawImage(start, 270 + i * 50, 40, null)
        }
        g.color = Color.WHITE
        g.drawString(Global.timeFormat(playTime), 650, 80)
        var a: Int = 0
        for (i in information) {
            if (i.id == hero.id) {
                hero.paint(g)
                g.color = Color.WHITE
                var stringWidth= g.getFontMetrics().stringWidth(i.name);
                g.drawString(i.name, 260-stringWidth/2, 265)
                g.drawString(i.level, 355, 265)
                g.drawString(i.attack, 470, 265)
                g.drawString(i.defence, 585, 265)
                g.drawString(i.speed, 700, 265)
                g.drawString(i.moneySpent, 810, 265)
                g.drawString(i.monsterKilled, 980, 265)
            }else{
                for (m in multiPlayers) {
                    if (i.id == m.id) {
                        m.paint(g)
                        break
                    }
                }
                g.color = Color.WHITE
                var stringWidth = g.getFontMetrics().stringWidth(i.name)
                g.drawString(i.name, 260 - stringWidth / 2, 265 + a * 100 + 140)
                g.drawString(i.level, 355, 265 + a * 100 + 140)
                g.drawString(i.attack, 470, 265 + a * 100 + 140)
                g.drawString(i.defence, 585, 265 + a * 100 + 140)
                g.drawString(i.speed, 700, 265 + a * 100 + 140)
                g.drawString(i.moneySpent, 810, 265 + a * 100 + 140)
                g.drawString(i.monsterKilled, 980, 265 + a * 100 + 140)
                a++
            }
        }
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { e ->
            run {
                when (e) {
                    is GameKernel.Input.Event.KeyPressed -> {
                        if (e.data.keyCode == KeyEvent.VK_Z ) {
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)
                            AudioResourceController.getInstance().stop(Path.Sounds.GAME_OVER)
                            AudioResourceController.getInstance().loop(Path.Sounds.SELECTION, 5)

                            GlobalRoundGame.reset()
                            SceneController.instance.change(StoryModeScene51())
                        }
                    }
                }
            }
        }
}