package scene

import core.GameKernel
import core.Scene
import core.controllers.ResController
import core.controllers.SceneController
import obj.utils.Animator
import utils.Global
import utils.GlobalPlayer
import utils.GlobalRoundGame
import utils.Path
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.event.KeyEvent

class GameOverScene(private val stageGame: Int) : Scene() {

    private val bgImg = ResController.instance.image(Path.Imgs.Backgrounds.GAME_OVER_SCENE)
    private val text = ResController.instance.image(Path.Imgs.Backgrounds.GAME_OVER_TEXT)
    private val start =ResController.instance.image(Path.Imgs.Objs.START)
    private var showDelay: Int = 0
    private var player: obj.Player = obj.Player(Global.SCREEN_X / 2, Global.SCREEN_Y / 2, 288, 288).apply { setAnimatorState(Animator.State.SLOW) }
    private var showPlay: Boolean = false
    private var playTime = GlobalRoundGame.endTime - GlobalRoundGame.startTime
    private var grade: Int =
            if (playTime <= GlobalPlayer.GradeLevel.values()[stageGame-1].threeStart) {
                3
            } else if (playTime <= GlobalPlayer.GradeLevel.values()[stageGame-1].twoStart) {
                2
            } else {
                1
            }

    //time goddess talk
    //goddess talk
    private val gameOverOne = ResController.instance.image(Path.Imgs.Objs.Information.GAME_OVER_TWO)
    private val largeLabel = ResController.instance.image(Path.Imgs.Objs.Information.LARGE_LABEL_90)
    private var gameOverOneY: Int = 90
    private var largeLabelY: Int = 50
    private var timeGoddessIndex: Int = 1

    //talk
    enum class TimeGoddess(val string: String) {
        NAME("Time Goddess"),
        ONE_1("Congratulations!"),
        ONE_2("You are the true hero!!"),
        TWO_1("I always believed that " + GlobalPlayer.playerName),
        TWO_2("would become the true hero."),
        THREE_1("Well then, that's it for now."),
        THREE_2("Please come again, true hero.")
    }

    private var canConfirm: Boolean = false

    override fun sceneBegin() {}

    override fun sceneEnd() {
        ResController.instance.clear()
        AudioResourceController.getInstance().stop(Path.Sounds.GAME_OVER)
        AudioResourceController.getInstance().loop(Path.Sounds.SELECTION,10)
    }

    override fun paint(g: Graphics) {
        g.drawImage(bgImg, 0, 0, null)
        if (showPlay) {
            player.paint(g)
        }
        if (showDelay <= 10) {
        } else if (showDelay <= 130) {
            g.font = Font("", java.awt.Font.BOLD, 90)
            g.color = Color.RED
            g.drawString("The true hero is... ", 150, Global.WINDOW_HEIGHT - 150)
        } else if (showDelay <= 250) {
            g.font = Font("", java.awt.Font.BOLD, 90)
            g.color = Color.RED
            var stringWidth= g.getFontMetrics().stringWidth(GlobalPlayer.playerName)
            g.drawString(GlobalPlayer.playerName + "!", player.painter.centerX-stringWidth/2, Global.WINDOW_HEIGHT - 150)
            showPlay = true
        } else if (showDelay <= 750) {
            gameOverTimeGoddessTalk(g)
            if (showDelay >= 390) {
                g.font = Font("", java.awt.Font.BOLD, 20)
                g.color = Color.WHITE
                g.drawString(TimeGoddess.NAME.string, 150, Global.WINDOW_HEIGHT / 2 + 60)
                g.drawString(TimeGoddess.values()[timeGoddessIndex].string, 190, Global.WINDOW_HEIGHT / 2 + 100)
                g.drawString(TimeGoddess.values()[timeGoddessIndex + 1].string, 190, Global.WINDOW_HEIGHT / 2 + 140)
                if (showDelay == 390 + 120) {
                    timeGoddessIndex += 2
                } else if (showDelay == 390 + 120 + 120) {
                    timeGoddessIndex += 2
                } else if (showDelay == 390 + 120 + 120 + 120) {
                    timeGoddessIndex += 2
                }
            }
        } else if (showDelay > 755) {
            g.drawImage(text, 0, 0, null)
            g.font = Font("", java.awt.Font.BOLD, 50)
            g.color = Color.DARK_GRAY
            //Grade
            for(i in 0 until grade){
                g.drawImage(start,120 + i*50,250,null)
            }
            g.drawString(Global.timeFormat(playTime), 120, 420)
            g.drawString("" + GlobalRoundGame.moneySpent, 800, 220)
            g.drawString("" + GlobalRoundGame.level, 800, 350)
            g.drawString("" + GlobalRoundGame.monsterKilled, 800, 480)
            canConfirm = true
        }
        showDelay++
    }

    private fun gameOverTimeGoddessTalk(g: Graphics) {
        if (gameOverOneY > 0) {
            gameOverOneY--
        } else if (largeLabelY > 0) {
            largeLabelY--
        }
        g.drawImage(gameOverOne, 50, 0 - gameOverOneY, null)
        if (gameOverOneY <= 0) {
            g.drawImage(largeLabel, 50, Global.WINDOW_HEIGHT / 2 + largeLabelY, null)
        }
    }

    override fun update(timePassed: Long) {
        player.update(timePassed)
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { e ->
            run {
                when (e) {
                    is GameKernel.Input.Event.KeyPressed -> {
                        if (e.data.keyCode == KeyEvent.VK_Z && canConfirm) {
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)

                            GlobalPlayer.setRecord(stageGame - 1, grade, playTime)
                            if(stageGame<4) {
                                GlobalPlayer.GameRecord.values()[stageGame].canPlay = true
                            }
                            GlobalPlayer.saveFile()
                            GlobalRoundGame.reset()

                            SceneController.instance.change(StoryModeScene51())
                        }else if(e.data.keyCode == KeyEvent.VK_Z && !canConfirm){
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)
                            showDelay = 755
                            showPlay = true
                        }
                    }
                }
            }
        }

}