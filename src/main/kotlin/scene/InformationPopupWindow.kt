package scene

import core.GameKernel
import core.controllers.ResController
import utils.Global
import utils.GlobalPlayer
import utils.GlobalRoundGame
import utils.Path
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.Image

class InformationPopupWindow(x: Int, y: Int, width: Int, height: Int) : menu.PopupWindow(x, y, width, height) {
    private val head: Image = ResController.instance.image(Path.Imgs.Actors.addPng(GlobalPlayer.usingStyling.stylingName))
    private val headBackground: Image = ResController.instance.image(Path.Imgs.Objs.Information.HEAD_BACKGROUND)
    private val bgImg = ResController.instance.image(Path.Imgs.Objs.ATTACK_DEFENCE_SPEED)

    override fun paintWindow(g: Graphics) {
        if (isShow) {
            g.drawImage(bgImg, x, y, null)
            g.drawImage(headBackground, x + width / 2 +5, y + 15, 70, 55, null)
            g.drawImage(
                    head, x + width / 2 + 7, y + 15 + 8, x + width / 2 + 7 + Global.UNIT_X * 2, y + 15 + 8 + 19 * 2,
                    0, 0, Global.UNIT_X, 19, null
            )
            g.color = Color.WHITE
            g.font = Font("", Font.BOLD, 20)
            g.drawString("Attack: "+ GlobalRoundGame.attack,x+25,y+105)
            g.drawString("Defence: "+ GlobalRoundGame.defence,x+25,y+135)
            g.drawString("Speed: "+ GlobalRoundGame.speed,x+25,y+165)
        }
    }


    override fun update(timePassed: Long) {

    }

    override fun sceneBegin() {
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { }
}