package scene

import core.GameKernel
import core.Scene
import core.controllers.ResController
import core.controllers.SceneController
import utils.Global
import utils.GlobalAchievement
import utils.Path
import java.awt.Font
import java.awt.Graphics
import java.awt.event.KeyEvent

class Achievement523 : core.Scene() {
    private val bgImg = ResController.instance.image(Path.Imgs.Backgrounds.ACHIEVEMENT)
    private val arrows = ResController.instance.image(Path.Imgs.Backgrounds.ACHIEVEMENT_ARROWS)
    private val completed = ResController.instance.image(Path.Imgs.Objs.COMPLETED)

    private var addIndex: Int = 0
    private var addY: Int = 0

    override fun sceneBegin() {
        //Achievement
        GlobalAchievement.allThreeStart()
        GlobalAchievement.allStyling()
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    override fun update(timePassed: Long) {
    }

    override fun paint(g: Graphics) {
        g.drawImage(bgImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        g.drawImage(arrows, 0, 120 + addY*98,null)
        g.font = Font("", Font.BOLD, 18)
        for(i in 0..4){
            if(!GlobalAchievement.Achievement.values()[i+addIndex].finish){
                g.color = java.awt.Color.DARK_GRAY
            }else{
                g.color = java.awt.Color.WHITE
                g.drawImage(completed,500,130+ i*98 ,null)
            }
            g.drawString(GlobalAchievement.Achievement.values()[i+addIndex].content,200,170+ i*98)
            g.drawString(""+GlobalAchievement.Achievement.values()[i+addIndex].finishedCount + "/" +GlobalAchievement.Achievement.values()[i+addIndex].goal
                    ,800,170+ i*98)
        }
    }

    private fun changeScene(change: Scene) {
        SceneController.instance.change(change)
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { e->
            run{
                when(e){
                    is GameKernel.Input.Event.KeyPressed ->{
                        if(e.data.keyCode == KeyEvent.VK_X){
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                            changeScene(GoddessRoomScene52())
                        }else if(e.data.keyCode == KeyEvent.VK_DOWN){
                            if(addIndex < 5 && addY == 4){
                                AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                addIndex++
                            }
                            if(addY < 4){
                                AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                addY++
                            }
                        }else if(e.data.keyCode == KeyEvent.VK_UP){
                            if(addIndex > 0 && addY == 0){
                                AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                addIndex--
                            }
                            if(addY > 0){
                                AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                addY--
                            }
                        }
                    }
                }
            }
        }
}