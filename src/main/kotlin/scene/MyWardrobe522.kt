package scene

import core.GameKernel
import core.Scene
import core.controllers.ResController
import core.controllers.SceneController
import obj.utils.Animator
import utils.Global
import utils.GlobalPlayer
import utils.Path
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.event.KeyEvent

class MyWardrobe522 : core.Scene() {
    private val bgImg = ResController.instance.image(Path.Imgs.Backgrounds.MY_WARDROBE)

    class ChoiceItem(val stylingShow: GlobalPlayer.Styling, private val stylingPaint: StylingPaint, private val pricePaint: PricePaint) {
        private var animator: Animator = Animator(0, Animator.State.WALK, stylingShow.stylingName, true)
        private var isFocused: Boolean = false
        private var dir: Global.Direction = Global.Direction.DOWN

        fun isFocused() {
            isFocused = true
        }

        fun unFocused() {
            isFocused = false
        }

        fun update() {
            if (isFocused) {
                animator.setSta(Animator.State.WALK)
                animator.update()
            } else {
                dir = Global.Direction.DOWN
                animator.setSta(Animator.State.STOP)
            }
        }

        fun paintComponent(g: Graphics) {
            animator.paint(dir, stylingPaint.x,
                    stylingPaint.y,
                    stylingPaint.x + stylingPaint.width,
                    stylingPaint.y + stylingPaint.height, g)
            g.color = Color.WHITE
            g.font = Font("", Font.BOLD, 20)
            g.drawString("" + stylingShow.stylingName, pricePaint.x, pricePaint.y)
        }
    }

    class NowFocusedChoice(var rectPosition: ChoiceRectLight, var item: ChoiceItem?) {
        private val rectImg = ResController.instance.image(Path.Imgs.Objs.RECT)
        private val leftImg = ResController.instance.image(Path.Imgs.Objs.LEFT_ANG)
        private val rightImg = ResController.instance.image(Path.Imgs.Objs.RIGHT_ANG)


        fun paintComponent(g: Graphics) {
            if (item != null) {
                g.drawImage(rectImg, rectPosition.x, rectPosition.y, null)
            } else {
                if (rectPosition == ChoiceRectLight.LAST_PAGE) {
                    g.drawImage(leftImg, rectPosition.x, rectPosition.y, null)
                } else {
                    g.drawImage(rightImg, rectPosition.x, rectPosition.y, null)
                }
            }
        }

        fun nowFocusedChange(rectPosition: ChoiceRectLight, item: ChoiceItem?) {
            this.item?.unFocused()
            this.rectPosition = rectPosition
            this.item = item
            item?.isFocused()
        }
    }

    private var nowFocusedChoice: NowFocusedChoice = NowFocusedChoice(ChoiceRectLight.values()[0], null)
    private var choiceItemOneToSix: MutableList<ChoiceItem> = mutableListOf()
    private var focus: Animator = Animator(0, Animator.State.WALK, GlobalPlayer.usingStyling.stylingName, true)

    enum class ChoiceRectLight(val x: Int, val y: Int) {
        ONE(410, 108),
        TWO(605, 108),
        THREE(805, 108),
        FOUR(410, 370),
        FIVE(605, 370),
        SIX(805, 370),
        LAST_PAGE(327, 320),
        NEXT_PAGE(995, 320)
    }

    enum class PricePaint(val x: Int, val y: Int) {
        ONE(470, 295),
        TWO(668, 295),
        THREE(865, 295),
        FOUR(470, 553),
        FIVE(668, 553),
        SIX(865, 553),
    }

    enum class StylingPaint(val x: Int, val y: Int, val width: Int, val height: Int) {
        ONE(460, 157, 96, 96),
        TWO(658, 157, 96, 96),
        THREE(855, 157, 96, 96),
        FOUR(460, 415, 96, 96),
        FIVE(658, 415, 96, 96),
        SIX(855, 415, 96, 96)
    }

    private var nowChoiceIndex: Int = 0
    private var nowPage: Int = 1

    override fun sceneBegin() {
        changePage(nowPage)
        nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], choiceItemOneToSix[nowChoiceIndex])
    }

    private fun changePage(page: Int) {
        choiceItemOneToSix.clear()
        var start = 0 + (page - 1) * 6
        var end = 5 + (page - 1) * 6
        var i = 0
        var y = 0
        for (styling in GlobalPlayer.Styling.values()) {
            if (!styling.isBought) {
                continue
            }
            if (i < start && styling.isBought) {
                i++
                continue
            }
            if (styling.isBought) {
                choiceItemOneToSix.add(ChoiceItem(styling, StylingPaint.values()[y], PricePaint.values()[y]))
                i++
                y++
            }
            if (i > end) {
                break
            }
        }
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    override fun update(timePassed: Long) {
        for (choiceItem in choiceItemOneToSix) {
            choiceItem.update()
        }
        focus.update()

    }

    override fun paint(g: Graphics) {
        g.drawImage(bgImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        focus.paint(Global.Direction.DOWN,80, Global.SCREEN_Y /2- Global.UNIT_Y *3 + 20,80+ Global.UNIT_Y *6, Global.SCREEN_Y /2+ Global.UNIT_Y *3+20,g)
        nowFocusedChoice.paintComponent(g)
        for (choiceItem in choiceItemOneToSix) {
            choiceItem.paintComponent(g)
        }
        g.color = Color.WHITE
        g.font = Font("", Font.BOLD, 40)
    }

    private fun changeScene(change: Scene) {
        SceneController.instance.change(change)
    }

    private fun filterTheLastOneStyle(): GlobalPlayer.Styling{
        var s: GlobalPlayer.Styling = GlobalPlayer.Styling.values()[0]
        for(styling in GlobalPlayer.Styling.values()){
            if(styling.isBought){
                s = styling
            }
        }
        return s
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { e ->
            run {
                when (e) {
                    is GameKernel.Input.Event.KeyPressed -> {
                        if (e.data.keyCode == KeyEvent.VK_Z) {
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)
                            if (nowChoiceIndex == 6 && nowPage != 1) {
                                nowPage--
                                changePage(nowPage)
                            } else if (nowChoiceIndex == 7 && choiceItemOneToSix[choiceItemOneToSix.size - 1].stylingShow != filterTheLastOneStyle()) {
                                nowPage++
                                changePage(nowPage)
                            } else if (nowChoiceIndex != 6 && nowChoiceIndex != 7) {
                                GlobalPlayer.usingStyling = nowFocusedChoice.item!!.stylingShow
                                focus = Animator(0, Animator.State.WALK, GlobalPlayer.usingStyling.stylingName, true)
                                GlobalPlayer.saveFile()
                            }
                        } else if (e.data.keyCode == KeyEvent.VK_X) {
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                            changeScene(GoddessRoomScene52())
                        } else if (e.data.keyCode == KeyEvent.VK_LEFT && nowChoiceIndex != 0 && nowChoiceIndex != 3 && nowChoiceIndex != 6 && nowChoiceIndex != 7) {
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            nowChoiceIndex--
                            nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], choiceItemOneToSix[nowChoiceIndex])
                        } else if (e.data.keyCode == KeyEvent.VK_RIGHT && nowChoiceIndex != 2 && nowChoiceIndex != choiceItemOneToSix.size - 1 && nowChoiceIndex != 6 && nowChoiceIndex != 7) {
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            nowChoiceIndex++
                            nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], choiceItemOneToSix[nowChoiceIndex])
                        } else if (e.data.keyCode == KeyEvent.VK_UP && nowChoiceIndex > 2 && nowChoiceIndex != 6 && nowChoiceIndex != 7) {
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            nowChoiceIndex -= 3
                            nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], choiceItemOneToSix[nowChoiceIndex])
                        } else if (e.data.keyCode == KeyEvent.VK_DOWN && nowChoiceIndex < 3 && nowChoiceIndex != 6 && nowChoiceIndex != 7) {
                            if (choiceItemOneToSix.size - 1 >= nowChoiceIndex + 3) {
                                AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                nowChoiceIndex += 3
                            } else {
                                if(choiceItemOneToSix.size > 3) {
                                    AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                                    nowChoiceIndex = choiceItemOneToSix.size - 1
                                }
                            }
                            nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], choiceItemOneToSix[nowChoiceIndex])
                        } else if (e.data.keyCode == KeyEvent.VK_LEFT && nowChoiceIndex == 0 || nowChoiceIndex == 3) {
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            nowChoiceIndex = 6
                            nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], null)
                        } else if (e.data.keyCode == KeyEvent.VK_RIGHT && nowChoiceIndex == 2 || nowChoiceIndex == choiceItemOneToSix.size - 1) {
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            nowChoiceIndex = 7
                            nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], null)
                        } else if (e.data.keyCode == KeyEvent.VK_LEFT && nowChoiceIndex == 7) {
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            nowChoiceIndex = choiceItemOneToSix.size - 1
                            nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], choiceItemOneToSix[nowChoiceIndex])
                        } else if (e.data.keyCode == KeyEvent.VK_RIGHT && nowChoiceIndex == 6) {
                            AudioResourceController.getInstance().play(Path.Sounds.SELECTION_MOVE)
                            nowChoiceIndex = 0
                            nowFocusedChoice.nowFocusedChange(ChoiceRectLight.values()[nowChoiceIndex], choiceItemOneToSix[nowChoiceIndex])
                        }
                    }
                }
            }
        }

}