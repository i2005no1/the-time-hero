package scene

import core.GameKernel
import core.Scene
import core.controllers.FileController
import core.controllers.ResController
import core.controllers.SceneController
import obj.utils.GameObject
import utils.Global
import utils.Path
import java.awt.Graphics
import java.awt.event.KeyEvent
import java.io.IOException

class HomeScene1 : Scene() {

    enum class Choice{
        NEW_GAME,LOAD_GAME,
    }

    class BallLight(x: Int, y: Int, width: Int, height: Int) : GameObject(x, y, width, height) {
        private val img = ResController.instance.image(Path.Imgs.Objs.LIGHT)

        override fun paintComponent(g: Graphics) {
            g.drawImage(img, painter.left, painter.top, null)
        }

        override fun update(timePassed: Long) {
            translate(x = -10, y = 10)
        }

    }

    private val bgImg = ResController.instance.image(Path.Imgs.Backgrounds.THE_TIME_HERO)
    private val ballLight: MutableList<BallLight> = mutableListOf()
    private val newGame = ResController.instance.image(Path.Imgs.Backgrounds.NEW_GAME)
    private val loadGame = ResController.instance.image(Path.Imgs.Backgrounds.LOAD_GAME)
    private var choice = Choice.NEW_GAME
    private var choiceIndex = 0

    override fun sceneBegin() {
        AudioResourceController.getInstance().loop(Path.Sounds.HOME,10)
    }

    override fun sceneEnd() {
        AudioResourceController.getInstance().stop(Path.Sounds.HOME)
        AudioResourceController.getInstance().loop(Path.Sounds.SELECTION,5)
        ResController.instance.clear()
    }

    override fun update(timePassed: Long) {
        ballLight()
        for (ball in ballLight) {
            ball.update(timePassed)
        }
        ballLight.removeIf{ a ->
            a.touchScreenBottom
        }

    }

    override fun paint(g: Graphics) {
        g.drawImage(bgImg, 0, 0, Global.WINDOW_WIDTH, Global.WINDOW_HEIGHT, null)
        for (ball in ballLight) {
            ball.paint(g)
        }
        if(choice == Choice.NEW_GAME){
            g.drawImage(newGame, 264, 398,null)
        }else if(choice == Choice.LOAD_GAME){
            g.drawImage(loadGame, 262, 473,null)
        }
    }

    private fun changeScene(change: Scene) {
        SceneController.instance.change(change)
    }

    private fun ballLight() {
        if (Global.random(0, 100) > 95) {
            ballLight += BallLight(Global.random(500, 1400), 0, 118, 113)
        }
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { e ->
            run {
                when (e) {
                    is GameKernel.Input.Event.KeyPressed -> {
                        if (e.data.keyCode == KeyEvent.VK_UP) {
                            if(choiceIndex != 0){
                                AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)
                                choiceIndex -= 1
                                choice = Choice.values()[choiceIndex]
                            }
                        } else if (e.data.keyCode == KeyEvent.VK_DOWN) {
                            if(choiceIndex != 1){
                                AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)
                                choiceIndex +=1
                                choice = Choice.values()[choiceIndex]
                            }
                        } else if (e.data.keyCode == KeyEvent.VK_Z) {
                            if(choice == Choice.NEW_GAME){
                                AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                                changeScene(GamePlotScene2())
                            }else if(choice == Choice.LOAD_GAME){
                                AudioResourceController.getInstance().play(Path.Sounds.CONFIRM_JAP)
                                try {
                                    FileController().read("TheTimeHero")
                                } catch (ex: IOException) {
                                    ex.printStackTrace()
                                }
                                changeScene(SelectionScene4())
                            }
                        }
                    }
                }
            }
        }


}