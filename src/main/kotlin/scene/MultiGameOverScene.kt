package scene

import client.ClientClass
import client.CommandReceiver
import core.GameKernel
import core.Scene
import core.controllers.ResController
import core.controllers.SceneController
import utils.Global
import utils.GlobalPlayer
import utils.GlobalRoundGame
import utils.Path
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.event.KeyEvent

class MultiGameOverScene(private val hero: MultiRoundGame.Multiplayer, private val multiPlayers: MutableList<MultiRoundGame.Multiplayer>) : Scene() {

    private val bgImg = ResController.instance.image(Path.Imgs.Backgrounds.MULTI_GAME_OVER_BG)
    private var showDelay: Int = 0
    private var showPlay: Boolean = false
    private var canConfirm: Boolean = false

    //time goddess talk
    //goddess talk
    private val trueHeroTimeGoddess = ResController.instance.image(Path.Imgs.Objs.Information.GAME_OVER_TWO)
    private val fakeHeroTimeGoddess = ResController.instance.image(Path.Imgs.Objs.Information.GAME_OVER_ONE)
    private val largeLabel = ResController.instance.image(Path.Imgs.Objs.Information.LARGE_LABEL_90)
    private var gameOverOneY: Int = 90
    private var largeLabelY: Int = 50
    private var timeGoddessIndex: Int = 1

    //talk
    enum class TrueHeroTimeGoddess(val string: String) {
        NAME("Time Goddess"),
        ONE_1("Congratulations!"),
        ONE_2("You are the true hero!!"),
        TWO_1("I always believed that " + GlobalPlayer.playerName),
        TWO_2("would become the true hero."),
        THREE_1("Well then, that's it for now."),
        THREE_2("Please come again, true hero.")
    }

    enum class FakeHeroTimeGoddess(val string: String) {
        NAME("Time Goddess"),
        ONE_1("Oh dear, that's too bad!"),
        ONE_2("Looks like you're just a fake hero."),
        TWO_1("But I'm sure that you, " + GlobalPlayer.playerName),
        TWO_2("... will be the true hero next time!"),
        THREE_1("Well then, that's it for now."),
        THREE_2("Please come again, fake hero.")
    }
    enum class Position(val x: Int, val y : Int){
        TWO_PLAYERS_ONE(510,470),
        THREE_PLAYERS_ONE(355,470),
        THREE_PLAYERS_TWO(665,470),
        FOUR_PLAYERS_ONE(200,470),
        FOUR_PLAYERS_TWO(510,470),
        FOUR_PLAYERS_THREE(820,470)
    }

    private val sendClient: ArrayList<String> = arrayListOf()
    class Command {
        val INFORMATION: Int = 1
    }
    private var command: Command = Command()
    open class InformationPlayer() {
        var id: Int = 0
        var name: String = ""
        var level: String = ""
        var attack: String = ""
        var defence: String = ""
        var speed: String = ""
        var moneySpent: String = ""
        var monsterKilled: String = ""

    }
    private val information: MutableList<InformationPlayer> = mutableListOf()

    override fun sceneBegin() {
        sendClient += ClientClass.getInstance().id.toString()
        sendClient += GlobalPlayer.playerName
        sendClient += GlobalRoundGame.level.toString()
        sendClient += GlobalRoundGame.attack.toString()
        sendClient += GlobalRoundGame.defence.toString()
        sendClient += GlobalRoundGame.speed.toString()
        sendClient += GlobalRoundGame.moneySpent.toString()
        sendClient += GlobalRoundGame.monsterKilled.toString()
        ClientClass.getInstance().sent(command.INFORMATION, sendClient)
        sendClient.clear()

        hero.noInvincible()
        hero.setCenter(480,160)
        hero.setSize(192,192)
        hero.setDir(Global.Direction.DOWN)
        var index :Int = 0
        if(multiPlayers.size == 3){
            index = 1
        }else if(multiPlayers.size == 4){
            index = 3
        }
        for( player in multiPlayers){
            if(player.id == hero.id){
                continue
            }
            player.setCenter(Position.values()[index].x,Position.values()[index].y)
            player.setSize(128,128)
            player.setDir(Global.Direction.DOWN)
            player.noInvincible()
            index++
        }
    }

    override fun sceneEnd() {
        ResController.instance.clear()
    }

    private fun gameOverTimeGoddessTalk(g: Graphics) {
        if (gameOverOneY > 0) {
            gameOverOneY--
        } else if (largeLabelY > 0) {
            largeLabelY--
        }
        if (multiPlayers[0].id == hero.id) {
            g.drawImage(trueHeroTimeGoddess, 50, 0 - gameOverOneY, null)
        } else {
            g.drawImage(fakeHeroTimeGoddess, 50, 0 - gameOverOneY, null)
        }
        if (gameOverOneY <= 0) {
            g.drawImage(largeLabel, 50, Global.WINDOW_HEIGHT / 2 + largeLabelY, null)
        }
    }

    override fun update(timePassed: Long) {
        hero.update(timePassed)

        ClientClass.getInstance().consume(object : CommandReceiver {
            override fun receive(serialNum: Int, commandCode: Int, strs: ArrayList<String>?) {
                when (commandCode) {
                    command.INFORMATION -> {
                        var index: Int = 0
                        var a: InformationPlayer = InformationPlayer()
                        do {
                            a.id = strs!![index++].toInt()
                            a.name = strs!![index++]
                            a.level = strs!![index++]
                            a.attack = strs!![index++]
                            a.defence = strs!![index++]
                            a.speed = strs!![index++]
                            a.moneySpent = strs!![index++]
                            a.monsterKilled = strs!![index++]
                            information += a
                        } while (index < strs!!.size)
                    }
                }
            }
        })

    }

    override fun paint(g: Graphics) {
        g.drawImage(bgImg, 0, 0, null)
        if (showPlay) {
            hero.paint(g)
            g.color = Color.WHITE
            g.font = Font("", Font.BOLD, 25)
            var stringWidth= g.getFontMetrics().stringWidth(hero.playerName)
            g.drawString(hero.playerName , hero.painter.centerX-stringWidth/2, hero.painter.centerY+130)
        }
        if (showDelay <= 10) {
        } else if (showDelay <= 130) {
            g.font = Font("", Font.BOLD, 90)
            g.color = Color.BLACK
            g.drawString("The true hero is... ", 150, Global.WINDOW_HEIGHT - 150)
        } else if (showDelay <= 250) {
            g.font = Font("", Font.BOLD, 90)
            g.color = Color.BLACK
            var stringWidth= g.getFontMetrics().stringWidth(hero.playerName)
            g.drawString(hero.playerName + "!", hero.painter.centerX-stringWidth/2, Global.WINDOW_HEIGHT - 150)
            showPlay = true
        } else if (showDelay <= 750) {
            gameOverTimeGoddessTalk(g)
            if (showDelay >= 390) {
                g.font = Font("", Font.BOLD, 20)
                g.color = Color.WHITE
                g.drawString(GameOverScene.TimeGoddess.NAME.string, 150, Global.WINDOW_HEIGHT / 2 + 60)
                g.drawString(GameOverScene.TimeGoddess.values()[timeGoddessIndex].string, 190, Global.WINDOW_HEIGHT / 2 + 100)
                g.drawString(GameOverScene.TimeGoddess.values()[timeGoddessIndex + 1].string, 190, Global.WINDOW_HEIGHT / 2 + 140)
                if (showDelay == 390 + 120) {
                    timeGoddessIndex += 2
                } else if (showDelay == 390 + 120 + 120) {
                    timeGoddessIndex += 2
                } else if (showDelay == 390 + 120 + 120 + 120) {
                    timeGoddessIndex += 2
                }
            }
        } else if (showDelay > 755) {
            for(player in multiPlayers){
                if(player!= hero){
                    player.paint(g)
                    var stringWidth= g.getFontMetrics().stringWidth(player.playerName)
                    g.drawString(player.playerName , player.painter.centerX-stringWidth/2, player.painter.centerY+100)
                }
            }
            canConfirm = true
        }
        showDelay++
    }

    override val input: ((GameKernel.Input.Event) -> Unit)?
        get() = { e ->
            run {
                when (e) {
                    is GameKernel.Input.Event.KeyPressed -> {
                        if (e.data.keyCode == KeyEvent.VK_Z && canConfirm) {
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)

                            SceneController.instance.change(MultiStatisticScene(hero,multiPlayers,information))
                        }else if(e.data.keyCode == KeyEvent.VK_Z && !canConfirm){
                            AudioResourceController.getInstance().play(Path.Sounds.CONFIRM)
                            showDelay = 755
                            showPlay = true
                        }
                    }
                }
            }
        }
}